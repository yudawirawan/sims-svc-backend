module.exports = {
  maxParamLength: 2000,
  logger: {
    level: process.env.NODE_ENV === 'development' ? 'info' : 'error',
    redact: ['req.headers.authorization'],
    serializers: {
      req(request) {
        return {
          method: request.method,
          url: request.url,
        }
      },
    },
    prettyPrint: {
      translateTime: true,
      ignore: 'pid,hostname,reqId,responseTime,req,res',
      messageFormat: '{msg} [id={reqId} {req.method} {req.url} statusCode={res.statusCode}]',
    }
  },
  schemaErrorFormatter: (errors) => {
    let message = ''
    for (const err of errors) {
      message += err.message
    }
    return new Error(message)
  }
}
