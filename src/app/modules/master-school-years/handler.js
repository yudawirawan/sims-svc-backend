const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMasterSchoolYear, updateMasterSchoolYear, destroyMasterSchoolYear
} = require('./service')

const postMasterSchoolYear = async(req, reply) => {
  const res = await createMasterSchoolYear(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMasterSchoolYear = async(req, reply) => {
  const schoolYear = await findPaging(req.query)
  return ok(reply, schoolYear, 'fetch data success')
}

const getMasterSchoolYear = async(req, reply) => {
  const id = req.params.id
  const schoolYear = await findById(id)
  if (!schoolYear) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, schoolYear, 'fetch data success')
}

const putMasterSchoolYear = async(req, reply) => {
  const id = req.params.id
  const schoolYear = await findById(id)
  if (!schoolYear) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMasterSchoolYear(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMasterSchoolYear = async(req, reply) => {
  const id = req.params.id
  const schoolYear = await findById(id)
  if (!schoolYear) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMasterSchoolYear(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMasterSchoolYear,
  getAllMasterSchoolYear,
  getMasterSchoolYear,
  putMasterSchoolYear,
  deleteMasterSchoolYear,
}
