const App = require('../../index')
const {Op} = require('sequelize')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MasterSchoolYear')

const findById = async(id) => {
  return await App.db.MasterSchoolYear.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMasterSchoolYear = async(payload) => {
  try {
    const res = await App.db.MasterSchoolYear.create(payload)
    await App.db.MasterSchoolYear.update({ status: 0 }, { where: { id: { [Op.ne]: res.id } } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateMasterSchoolYear = async(id, payload) => {
  try {
    const res = await App.db.MasterSchoolYear.update(payload, { where: { id } })
    await App.db.MasterSchoolYear.update({ status: 0 }, { where: { id: { [Op.ne]: id } } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyMasterSchoolYear = async(id) => {
  try {
    await App.db.MasterSchoolYear.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMasterSchoolYear,
  updateMasterSchoolYear,
  destroyMasterSchoolYear,
}
