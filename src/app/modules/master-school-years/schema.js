const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMasterSchoolYearSchema = {
  description: 'Create school year data',
  tags: ['master-school-years'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      schoolYear: { type: 'string' },
      startDate: { type: 'string' },
      endDate: { type: 'string' },
      description: { type: 'string' },
      status: { type: 'number' },
    },
    required: [
      'departmentId',
      'schoolYear',
      'startDate',
      'endDate',
      'status'
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            schoolYear: { type: 'string' },
            startDate: { type: 'string' },
            endDate: { type: 'string' },
            description: { type: 'string' },
            status: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMasterSchoolYearSchema = {
  description: 'List all school year data',
  tags: ['master-school-years'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              schoolYear: { type: 'string' },
              startDate: { type: 'string' },
              endDate: { type: 'string' },
              description: { type: 'string' },
              status: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMasterSchoolYearSchema = {
  description: 'Get level school year by id',
  tags: ['master-school-years'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        schoolYear: { type: 'string' },
        startDate: { type: 'string' },
        endDate: { type: 'string' },
        description: { type: 'string' },
        status: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMasterSchoolYearSchema = {
  description: 'Update school year data',
  summary: 'Update data',
  tags: ['master-school-years'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      schoolYear: { type: 'string' },
      startDate: { type: 'string' },
      endDate: { type: 'string' },
      description: { type: 'string' },
      status: { type: 'number' },
    },
    required: [
      'departmentId',
      'schoolYear',
      'startDate',
      'endDate',
      'status',
    ],
  },
  params: idParamsSchema,
}

const deleteMasterSchoolYearSchema = {
  description: 'Delete school year data',
  summary: 'Delete data',
  tags: ['master-school-years'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMasterSchoolYearSchema,
  getAllMasterSchoolYearSchema,
  getMasterSchoolYearSchema,
  putMasterSchoolYearSchema,
  deleteMasterSchoolYearSchema,
}
