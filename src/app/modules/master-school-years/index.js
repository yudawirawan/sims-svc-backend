const { postMasterSchoolYear, putMasterSchoolYear, deleteMasterSchoolYear, getAllMasterSchoolYear, getMasterSchoolYear } = require('./handler')
const {
  postMasterSchoolYearSchema, getAllMasterSchoolYearSchema, getMasterSchoolYearSchema, putMasterSchoolYearSchema, putStatusMasterSchoolYearSchema, deleteMasterSchoolYearSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMasterSchoolYearSchema }, postMasterSchoolYear)
  app.get('/', { schema: getAllMasterSchoolYearSchema }, getAllMasterSchoolYear)
  app.get('/:id', { schema: getMasterSchoolYearSchema }, getMasterSchoolYear)
  app.put('/:id', { schema: putMasterSchoolYearSchema }, putMasterSchoolYear)
  app.delete('/:id', { schema: deleteMasterSchoolYearSchema }, deleteMasterSchoolYear)
}
