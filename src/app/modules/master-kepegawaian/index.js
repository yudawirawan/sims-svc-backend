const { postKepegawaian, putKepegawaian, deleteKepegawaian, getAllKepegawaian, getKepegawaian } = require('./handler')
const {
  postKepegawaianSchema, getAllKepegawaianSchema, getKepegawaianSchema, putKepegawaianSchema, deleteKepegawaianSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postKepegawaianSchema }, postKepegawaian)
  app.get('/', { schema: getAllKepegawaianSchema }, getAllKepegawaian)
  app.get('/:id', { schema: getKepegawaianSchema }, getKepegawaian)
  app.put('/:id', { schema: putKepegawaianSchema }, putKepegawaian)
  app.delete('/:id', { schema: deleteKepegawaianSchema }, deleteKepegawaian)
}
