const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MasterKepegawaian')

const findById = async(id) => {
  return await App.db.MasterKepegawaian.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createKepegawaian = async(payload) => {
  try {
    return await App.db.MasterKepegawaian.create(payload)
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateKepegawaian = async(id, payload) => {
  try {
    return await App.db.MasterKepegawaian.update(payload, { where: { id } })
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyKepegawaian = async(id) => {
  try {
    await App.db.MasterKepegawaian.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createKepegawaian,
  updateKepegawaian,
  destroyKepegawaian,
}
