const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createKepegawaian, updateKepegawaian, destroyKepegawaian
} = require('./service')

const postKepegawaian = async(req, reply) => {
  const res = await createKepegawaian(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllKepegawaian = async(req, reply) => {
  const pegawai = await findPaging(req.query)
  return ok(reply, pegawai, 'fetch data success')
}

const getKepegawaian = async(req, reply) => {
  const id = req.params.id
  const kepegawaian = await findById(id)
  if (!kepegawaian) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, kepegawaian, 'fetch data success')
}

const putKepegawaian = async(req, reply) => {
  const id = req.params.id
  const kepegawaian = await findById(id)
  if (!kepegawaian) {
    return notFound(reply, 'data not found')
  }
  const res = await updateKepegawaian(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteKepegawaian = async(req, reply) => {
  const id = req.params.id
  const kepegawaian = await findById(id)
  if (!kepegawaian) {
    return notFound(reply, 'data not found')
  }
  const res = destroyKepegawaian(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postKepegawaian,
  getAllKepegawaian,
  getKepegawaian,
  putKepegawaian,
  deleteKepegawaian,
}
