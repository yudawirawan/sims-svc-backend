const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMasterClassSchema = {
  description: 'Create class data',
  tags: ['master-classes'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      schoolYearId: { type: 'number' },
      name: { type: 'string' },
      kepegawaianId: { type: 'number' },
      mentorId: { type: 'number' },
      capacity: { type: 'number' },
      capacityFilled: { type: 'number' },
      description: { type: 'string' },
    },
    required: [
      'departmentId',
      'levelId',
      'schoolYearId',
      'name',
      'kepegawaianId'
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            levelId: { type: 'number' },
            schoolYearId: { type: 'number' },
            name: { type: 'string' },
            kepegawaianId: { type: 'number' },
            mentorId: { type: 'number' },
            capacity: { type: 'number' },
            capacityFilled: { type: 'number' },
            description: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMasterClassSchema = {
  description: 'List all classes data',
  tags: ['master-classes'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              levelId: { type: 'number' },
              schoolYearId: { type: 'number' },
              name: { type: 'string' },
              kepegawaianId: { type: 'number' },
              mentorId: { type: 'number' },
              capacity: { type: 'number' },
              capacityFilled: { type: 'number' },
              description: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMasterClassSchema = {
  description: 'Get class by id',
  tags: ['master-classes'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        levelId: { type: 'number' },
        schoolYearId: { type: 'number' },
        name: { type: 'string' },
        kepegawaianId: { type: 'number' },
        mentorId: { type: 'number' },
        capacity: { type: 'number' },
        capacityFilled: { type: 'number' },
        description: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMasterClassSchema = {
  description: 'Update class data',
  summary: 'Update data',
  tags: ['master-classes'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      schoolYearId: { type: 'number' },
      name: { type: 'string' },
      kepegawaianId: { type: 'number' },
      mentorId: { type: 'number' },
      capacity: { type: 'number' },
      capacityFilled: { type: 'number' },
      description: { type: 'string' },
    },
    required: [
      'departmentId',
      'levelId',
      'schoolYearId',
      'name',
      'kepegawaianId'
    ],
  },
  params: idParamsSchema,
}

const deleteMasterClassSchema = {
  description: 'Delete class data',
  summary: 'Delete data',
  tags: ['master-classes'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMasterClassSchema,
  getAllMasterClassSchema,
  getMasterClassSchema,
  putMasterClassSchema,
  deleteMasterClassSchema,
}
