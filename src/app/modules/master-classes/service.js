const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MasterClass')

const findById = async(id) => {
  return await App.db.MasterClass.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'level', attributes: ['level'] },
    { association: 'schoolYear', attributes: ['schoolYear'] },
    { association: 'kepegawaian', attributes: ['nama', 'nik'] },
    { association: 'mentor', attributes: ['nama', 'nik'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMasterClass = async(payload) => {
  try {
    const res = await App.db.MasterClass.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateMasterClass = async(id, payload) => {
  try {
    const res = await App.db.MasterClass.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyMasterClass = async(id) => {
  try {
    await App.db.MasterClass.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMasterClass,
  updateMasterClass,
  destroyMasterClass,
}
