const { postMasterClass, putMasterClass, deleteMasterClass, getAllMasterClass, getMasterClass } = require('./handler')
const {
  postMasterClassSchema, getAllMasterClassSchema, getMasterClassSchema, putMasterClassSchema, deleteMasterClassSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMasterClassSchema }, postMasterClass)
  app.get('/', { schema: getAllMasterClassSchema }, getAllMasterClass)
  app.get('/:id', { schema: getMasterClassSchema }, getMasterClass)
  app.put('/:id', { schema: putMasterClassSchema }, putMasterClass)
  app.delete('/:id', { schema: deleteMasterClassSchema }, deleteMasterClass)
}
