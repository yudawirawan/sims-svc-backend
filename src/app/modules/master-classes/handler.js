const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMasterClass, updateMasterClass, destroyMasterClass
} = require('./service')

const postMasterClass = async(req, reply) => {
  const res = await createMasterClass(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMasterClass = async(req, reply) => {
  const kelas = await findPaging(req.query)
  return ok(reply, kelas, 'fetch data success')
}

const getMasterClass = async(req, reply) => {
  const id = req.params.id
  const kelas = await findById(id)
  if (!kelas) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, kelas, 'fetch data success')
}

const putMasterClass = async(req, reply) => {
  const id = req.params.id
  const kelas = await findById(id)
  if (!kelas) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMasterClass(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMasterClass = async(req, reply) => {
  const id = req.params.id
  const kelas = await findById(id)
  if (!kelas) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMasterClass(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMasterClass,
  getAllMasterClass,
  getMasterClass,
  putMasterClass,
  deleteMasterClass,
}
