const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'ClassesPromote')

const findById = async(id) => {
  return await App.db.ClassesPromote.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'oldLevel', attributes: ['level'] },
    { association: 'oldClass', attributes: ['name'] },
    { association: 'student', attributes: ['nama'] },
    { association: 'newLevel', attributes: ['level'] },
    { association: 'newClass', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] },
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createClassesPromote = async(payload) => {
  const transaction = await App.db.sequelize.transaction()
  try {
    const res = await App.db.ClassesPromote.create(payload, { transaction })
    payload.studentReference.studentId = res.studentId
      if (!payload.studentReference.isDeleted) {
        await App.db.MasterStudentReference.update(payload.studentReference, {
          where: { studentId: res.studentId },
          transaction,
          plain: true,
          returning: true,
          raw: true,
        })
      } else if (studentReference.isDeleted && studentReference.studentId) {
        await App.db.MasterStudentReference.destroy({ where: { studentId: studentReference.studentId } })
      }
    res.studentReference = payload.studentReference
    await transaction.commit()
    return res
  } catch (err) {
    console.log(err)
    await transaction.rollback()
    return {
      error: err.message,
    }
  }
}

const updateClassesPromote = async(id, payload) => {
  const transaction = await App.db.sequelize.transaction()
  try {
    const res = await App.db.ClassesPromote.update(
      payload,
      { where: { id } },
      {
        returning: true,
        plain: true,
        raw: true,
        transaction,
      })
    payload.studentReference.studentId = res.studentId
    if (!payload.studentReference.isDeleted) {
      await App.db.MasterStudentReference.update(payload.studentReference, {
        where: { studentId: payload.studentId },
        transaction,
        plain: true,
        returning: true,
        raw: true,
      })
    } else if (studentReference.isDeleted && studentReference.studentId) {
      await App.db.MasterStudentReference.destroy({ where: { studentId: studentReference.studentId } })
    }
    res.studentReference = payload.studentReference
    await transaction.commit()
    return res
  } catch (err) {
    console.log(err)
    await transaction.rollback()
    return {
      error: err.message,
    }
  }
}

const destroyClassesPromote = async(id) => {
  try {
    await App.db.ClassesPromote.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const genRegisterId = async() => {
  try {
    const moment = require('moment-timezone')
    const period = moment().tz('Asia/Jakarta').format('YYYY')
    const [[result]] = await db.sequelize
      .query(`SELECT registerId FROM psb_candidates WHERE registerId LIKE '${period}-%' ORDER BY registerId DESC LIMIT 1`)
    let genRegId = ''
    if (result && result.registerId) {
      const regIdNum = result.registerId.substring(5, result.registerId.length)
      const regId = Number(regIdNum) + 1
      genRegId = period + '-' + String(regId).padStart(4, '0')
    } else {
      genRegId = moment().tz('Asia/Jakarta').format('YYYY') + '-0001'
    }
    return {
      genRegId
    }
  } catch (err) {
    console.log(err)
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createClassesPromote,
  updateClassesPromote,
  destroyClassesPromote,
  genRegisterId,
}
