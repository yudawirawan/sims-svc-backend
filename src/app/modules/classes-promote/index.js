const { postClassesPromote, putClassesPromote, deleteClassesPromote, getAllClassesPromote, getClassesPromote } = require('./handler')
const {
  postClassesPromoteSchema, getAllClassesPromoteSchema, getClassesPromoteSchema, putClassesPromoteSchema, deleteClassesPromoteSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postClassesPromoteSchema }, postClassesPromote)
  app.get('/', { schema: getAllClassesPromoteSchema }, getAllClassesPromote)
  app.get('/:id', { schema: getClassesPromoteSchema }, getClassesPromote)
  app.put('/:id', { schema: putClassesPromoteSchema }, putClassesPromote)
  app.delete('/:id', { schema: deleteClassesPromoteSchema }, deleteClassesPromote)
}
