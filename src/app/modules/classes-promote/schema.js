const { err401, err400, idParamsSchema, queryPaging, resp200 } = require('../../utils/http_schema')

const postClassesPromoteSchema = {
  description: 'Create classes changedata',
  tags: ['classes-promote'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      oldLevelId: { type: 'number' },
      oldClassId: { type: 'number' },
      studentId: { type: 'number' },
      newLevelId: { type: 'number' },
      newClassId: { type: 'number' },
      studentReference: {
        type: 'object',
        properties: {
          departmentId: { type: 'number' },
          levelId: { type: 'number' },
          classId: { type: 'number' },
        }
      },
    },
    required: [
      'oldLevelId',
      'oldClassId',
      'studentId',
      'newLevelId',
      'newClassId',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            oldLevelId: { type: 'number' },
            oldClassId: { type: 'number' },
            studentId: { type: 'number' },
            newLevelId: { type: 'number' },
            newClassId: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllClassesPromoteSchema = {
  description: 'List all classes changedata',
  tags: ['classes-promote'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              oldLevelId: { type: 'number' },
              oldClassId: { type: 'number' },
              studentId: { type: 'number' },
              newLevelId: { type: 'number' },
              newClassId: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getClassesPromoteSchema = {
  description: 'Get classes changedata by id',
  tags: ['classes-promote'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        oldLevelId: { type: 'number' },
        oldClassId: { type: 'number' },
        studentId: { type: 'number' },
        newLevelId: { type: 'number' },
        newClassId: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putClassesPromoteSchema = {
  description: 'Update classes changedata',
  summary: 'Update data',
  tags: ['classes-promote'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      oldLevelId: { type: 'number' },
      oldClassId: { type: 'number' },
      studentId: { type: 'number' },
      newLevelId: { type: 'number' },
      newClassId: { type: 'number' },
      studentReference: {
        type: 'object',
        properties: {
          departmentId: { type: 'number' },
          levelId: { type: 'number' },
          classId: { type: 'number' },
        }
      },
    },
    required: [
      'oldLevelId',
      'oldClassId',
      'studentId',
      'newLevelId',
      'newClassId',
    ],
  },
  params: idParamsSchema,
}

const deleteClassesPromoteSchema = {
  description: 'Delete batch psb process',
  summary: 'Delete psb process batch',
  tags: ['classes-promote'],
  body: {
    type: 'object',
    properties: {
      ids: { type: 'array' },
    },
    required: ['ids'],
  },
  response: {
    ...resp200({
      id: { type: 'number' },
      username: { type: 'string' },
    }),
    ...err401,
  },
}

module.exports = {
  postClassesPromoteSchema,
  getAllClassesPromoteSchema,
  getClassesPromoteSchema,
  putClassesPromoteSchema,
  deleteClassesPromoteSchema,
}
