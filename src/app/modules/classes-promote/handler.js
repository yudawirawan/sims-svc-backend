const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createClassesPromote, updateClassesPromote, destroyClassesPromote,
} = require('./service')

const postClassesPromote = async(req, reply) => {
  const result = await createClassesPromote(req.body)
  if (result.error) {
    return badRequest(reply, result.error)
  }
  return ok(reply, result, 'Create data success')
}

const getAllClassesPromote = async(req, reply) => {
  const result = await findPaging(req.query)
  return ok(reply, result, 'fetch data success')
}

const getClassesPromote = async(req, reply) => {
  const id = req.params.id
  const result = await findById(id)
  if (!result) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, result, 'fetch data success')
}

const putClassesPromote = async(req, reply) => {
  const id = req.params.id
  const result = await findById(id)
  if (!result) {
    return notFound(reply, 'data not found')
  }
  const res = await updateClassesPromote(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteClassesPromote = async(req, reply) => {
  const id = req.params.id
  const result = await findById(id)
  if (!result) {
    return notFound(reply, 'data not found')
  }
  const res = destroyClassesPromote(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deleteClassesPromoteBatch = async(req, reply) => {
  const { ids } = req.body
  const result = destroyClassesPromote(ids)
  if (result.error) {
    return badRequest(reply, result.error)
  }
  return ok(reply, null, 'Delete users success')
}

module.exports = {
  postClassesPromote,
  getAllClassesPromote,
  getClassesPromote,
  putClassesPromote,
  deleteClassesPromote,
  deleteClassesPromoteBatch,
}
