const { postPsbProcess, putPsbProcess, deletePsbProcess, deletePsbProcessBatch, getAllPsbProcess, getPsbProcess } = require('./handler')
const {
  postPsbProcessSchema, getAllPsbProcessSchema, getPsbProcessSchema, putPsbProcessSchema, deletePsbProcessSchema, deletePsbProcessBatchSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postPsbProcessSchema }, postPsbProcess)
  app.get('/', { schema: getAllPsbProcessSchema }, getAllPsbProcess)
  app.get('/:id', { schema: getPsbProcessSchema }, getPsbProcess)
  app.put('/:id', { schema: putPsbProcessSchema }, putPsbProcess)
  app.delete('/:id', { schema: deletePsbProcessSchema }, deletePsbProcess)
  app.post('/delete-batch', { schema: deletePsbProcessBatchSchema }, deletePsbProcessBatch)
}
