const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postPsbProcessSchema = {
  description: 'Create PSB Process data',
  tags: ['psb-processes'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      name: { type: 'string' },
      code: { type: 'string' },
      description: { type: 'string' },
      status: { type: 'string' },
    },
    required: [
      'departmentId',
      'name',
      'code',
      'description',
      'status',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            name: { type: 'string' },
            code: { type: 'string' },
            description: { type: 'string' },
            status: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllPsbProcessSchema = {
  description: 'List all Psb Process data',
  tags: ['psb-processes'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              name: { type: 'string' },
              code: { type: 'string' },
              description: { type: 'string' },
              status: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getPsbProcessSchema = {
  description: 'Get Psb Process data by id',
  tags: ['psb-processes'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        name: { type: 'string' },
        code: { type: 'string' },
        description: { type: 'string' },
        status: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putPsbProcessSchema = {
  description: 'Update Psb Process data',
  summary: 'Update data',
  tags: ['psb-processes'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      name: { type: 'string' },
      code: { type: 'string' },
      description: { type: 'string' },
      status: { type: 'string' },
    },
    required: [
      'departmentId',
      'name',
      'code',
      'description',
      'status',
    ],
  },
  params: idParamsSchema,
}

const deletePsbProcessSchema = {
  description: 'Delete Psb Process data',
  summary: 'Delete data',
  tags: ['psb-processes'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

const deletePsbProcessBatchSchema = {
  description: 'Delete batch psb process',
  summary: 'Delete psb process batch',
  tags: ['psb-processes'],
  body: {
    type: 'object',
    properties: {
      ids: { type: 'array' },
    },
    required: ['ids'],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        username: { type: 'string' },
      }
    },
    ...err401,
  },
}

module.exports = {
  postPsbProcessSchema,
  getAllPsbProcessSchema,
  getPsbProcessSchema,
  putPsbProcessSchema,
  deletePsbProcessSchema,
  deletePsbProcessBatchSchema,
}
