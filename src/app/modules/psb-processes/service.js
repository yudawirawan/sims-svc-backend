const App = require('./../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'PsbProcess')

const findById = async(id) => {
  return await App.db.PsbProcess.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] },
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createPsbProcess = async(payload) => {
  try {
    const res = await App.db.PsbProcess.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updatePsbProcess = async(id, payload) => {
  try {
    const res = await App.db.PsbProcess.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyPsbProcess = async(id) => {
  try {
    await App.db.PsbProcess.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createPsbProcess,
  updatePsbProcess,
  destroyPsbProcess,
}
