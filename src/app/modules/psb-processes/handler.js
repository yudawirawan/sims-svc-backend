const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createPsbProcess, updatePsbProcess, destroyPsbProcess,
} = require('./service')

const postPsbProcess = async(req, reply) => {
  const res = await createPsbProcess(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllPsbProcess = async(req, reply) => {
  const pegawai = await findPaging(req.query)
  return ok(reply, pegawai, 'fetch data success')
}

const getPsbProcess = async(req, reply) => {
  const id = req.params.id
  const PsbProcess = await findById(id)
  if (!PsbProcess) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, PsbProcess, 'fetch data success')
}

const putPsbProcess = async(req, reply) => {
  const id = req.params.id
  const PsbProcess = await findById(id)
  if (!PsbProcess) {
    return notFound(reply, 'data not found')
  }
  const res = await updatePsbProcess(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deletePsbProcess = async(req, reply) => {
  const id = req.params.id
  const PsbProcess = await findById(id)
  if (!PsbProcess) {
    return notFound(reply, 'data not found')
  }
  const res = destroyPsbProcess(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deletePsbProcessBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyPsbProcess(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete users success')
}

module.exports = {
  postPsbProcess,
  getAllPsbProcess,
  getPsbProcess,
  putPsbProcess,
  deletePsbProcess,
  deletePsbProcessBatch,
}
