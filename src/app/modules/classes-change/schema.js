const { err401, err400, idParamsSchema, queryPaging, resp200 } = require('../../utils/http_schema')

const postClassesChangeSchema = {
  description: 'Create classes changedata',
  tags: ['classes-change'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      oldLevelId: { type: 'number' },
      oldClassId: { type: 'number' },
      studentId: { type: 'number' },
      newLevelId: { type: 'number' },
      newClassId: { type: 'number' },
      studentReference: {
        type: 'object',
        properties: {
          studentId: { type: 'number' },
          departmentId: { type: 'number' },
          levelId: { type: 'number' },
          classId: { type: 'number' },
        }
      },
    },
    required: [
      'oldLevelId',
      'oldClassId',
      'studentId',
      'newLevelId',
      'newClassId',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            oldLevelId: { type: 'number' },
            oldClassId: { type: 'number' },
            studentId: { type: 'number' },
            newLevelId: { type: 'number' },
            newClassId: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllClassesChangeSchema = {
  description: 'List all classes changedata',
  tags: ['classes-change'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              oldLevelId: { type: 'number' },
              oldClassId: { type: 'number' },
              studentId: { type: 'number' },
              newLevelId: { type: 'number' },
              newClassId: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getClassesChangeSchema = {
  description: 'Get classes changedata by id',
  tags: ['classes-change'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        oldLevelId: { type: 'number' },
        oldClassId: { type: 'number' },
        studentId: { type: 'number' },
        newLevelId: { type: 'number' },
        newClassId: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putClassesChangeSchema = {
  description: 'Update classes changedata',
  summary: 'Update data',
  tags: ['classes-change'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      oldLevelId: { type: 'number' },
      oldClassId: { type: 'number' },
      studentId: { type: 'number' },
      newLevelId: { type: 'number' },
      newClassId: { type: 'number' },
      studentReference: {
        type: 'object',
        properties: {
          departmentId: { type: 'number' },
          levelId: { type: 'number' },
          classId: { type: 'number' },
        }
      },
    },
    required: [
      'oldLevelId',
      'oldClassId',
      'studentId',
      'newLevelId',
      'newClassId',
    ],
  },
  params: idParamsSchema,
}

const deleteClassesChangeSchema = {
  description: 'Delete batch psb process',
  summary: 'Delete psb process batch',
  tags: ['classes-change'],
  body: {
    type: 'object',
    properties: {
      ids: { type: 'array' },
    },
    required: ['ids'],
  },
  response: {
    ...resp200({
      id: { type: 'number' },
      username: { type: 'string' },
    }),
    ...err401,
  },
}

module.exports = {
  postClassesChangeSchema,
  getAllClassesChangeSchema,
  getClassesChangeSchema,
  putClassesChangeSchema,
  deleteClassesChangeSchema,
}
