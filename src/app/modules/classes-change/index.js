const { postClassesChange, putClassesChange, deleteClassesChange, getAllClassesChange, getClassesChange } = require('./handler')
const {
  postClassesChangeSchema, getAllClassesChangeSchema, getClassesChangeSchema, putClassesChangeSchema, deleteClassesChangeSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postClassesChangeSchema }, postClassesChange)
  app.get('/', { schema: getAllClassesChangeSchema }, getAllClassesChange)
  app.get('/:id', { schema: getClassesChangeSchema }, getClassesChange)
  app.put('/:id', { schema: putClassesChangeSchema }, putClassesChange)
  app.delete('/:id', { schema: deleteClassesChangeSchema }, deleteClassesChange)
}
