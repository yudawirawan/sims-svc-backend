const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createClassesChange, updateClassesChange, destroyClassesChange,
} = require('./service')

const postClassesChange = async(req, reply) => {
  const result = await createClassesChange(req.body)
  if (result.error) {
    return badRequest(reply, result.error)
  }
  return ok(reply, result, 'Create data success')
}

const getAllClassesChange = async(req, reply) => {
  const result = await findPaging(req.query)
  return ok(reply, result, 'fetch data success')
}

const getClassesChange = async(req, reply) => {
  const id = req.params.id
  const result = await findById(id)
  if (!result) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, result, 'fetch data success')
}

const putClassesChange = async(req, reply) => {
  const id = req.params.id
  const result = await findById(id)
  if (!result) {
    return notFound(reply, 'data not found')
  }
  const res = await updateClassesChange(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteClassesChange = async(req, reply) => {
  const id = req.params.id
  const result = await findById(id)
  if (!result) {
    return notFound(reply, 'data not found')
  }
  const res = destroyClassesChange(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deleteClassesChangeBatch = async(req, reply) => {
  const { ids } = req.body
  const result = destroyClassesChange(ids)
  if (result.error) {
    return badRequest(reply, result.error)
  }
  return ok(reply, null, 'Delete users success')
}

module.exports = {
  postClassesChange,
  getAllClassesChange,
  getClassesChange,
  putClassesChange,
  deleteClassesChange,
  deleteClassesChangeBatch,
}
