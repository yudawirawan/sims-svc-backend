const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'StudentExtracurricular')

const findById = async(id) => {
  return await App.db.StudentExtracurricular.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'level', attributes: ['level'] },
    { association: 'class', attributes: ['name'] },
    { association: 'student', attributes: ['nama'] },
    { association: 'subject1', attributes: ['name'] },
    { association: 'subject2', attributes: ['name'] },
    { association: 'subject3', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createStudentExtracurricular = async(payload) => {
  try {
    const res = await App.db.StudentExtracurricular.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateStudentExtracurricular = async(id, payload) => {
  try {
    const res = await App.db.StudentExtracurricular.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyStudentExtracurricular = async(id) => {
  try {
    await App.db.StudentExtracurricular.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createStudentExtracurricular,
  updateStudentExtracurricular,
  destroyStudentExtracurricular,
}
