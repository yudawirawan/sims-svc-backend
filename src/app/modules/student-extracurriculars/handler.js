const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createStudentExtracurricular, updateStudentExtracurricular, destroyStudentExtracurricular
} = require('./service')

const postStudentExtracurricular = async(req, reply) => {
  const extra = await createStudentExtracurricular(req.body)
  if (extra.error) {
    return badRequest(reply, extra.error)
  }
  return ok(reply, extra, 'Create data success')
}

const getAllStudentExtracurricular = async(req, reply) => {
  const extra = await findPaging(req.query)
  return ok(reply, extra, 'fetch data success')
}

const getStudentExtracurricular = async(req, reply) => {
  const id = req.params.id
  const extra = await findById(id)
  if (!extra) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, extra, 'fetch data success')
}

const putStudentExtracurricular = async(req, reply) => {
  const id = req.params.id
  const extra = await findById(id)
  if (!extra) {
    return notFound(reply, 'data not found')
  }
  const res = await updateStudentExtracurricular(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteStudentExtracurricular = async(req, reply) => {
  const id = req.params.id
  const extra = await findById(id)
  if (!extra) {
    return notFound(reply, 'data not found')
  }
  const res = destroyStudentExtracurricular(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postStudentExtracurricular,
  getAllStudentExtracurricular,
  getStudentExtracurricular,
  putStudentExtracurricular,
  deleteStudentExtracurricular,
}
