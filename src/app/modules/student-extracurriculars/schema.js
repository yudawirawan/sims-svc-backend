const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postStudentExtracurricularSchema = {
  description: 'Create lesson plan data',
  tags: ['student-extracurricular'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      classId: { type: 'number' },
      studentId: { type: 'number' },
      subjectId1: { type: 'number' },
      subjectId2: { type: 'number' },
      subjectId3: { type: 'number' },
    },
    required: [
      'departmentId',
      'levelId',
      'classId',
      'studentId',
      'subjectId1',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            levelId: { type: 'number' },
            classId: { type: 'number' },
            studentId: { type: 'number' },
            subjectId1: { type: 'number' },
            subjectId2: { type: 'number' },
            subjectId3: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllStudentExtracurricularSchema = {
  description: 'List all lesson plans data',
  tags: ['student-extracurricular'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              levelId: { type: 'number' },
              classId: { type: 'number' },
              studentId: { type: 'number' },
              subjectId1: { type: 'number' },
              subjectId2: { type: 'number' },
              subjectId3: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getStudentExtracurricularSchema = {
  description: 'Get lesson plan data by id',
  tags: ['student-extracurricular'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        levelId: { type: 'number' },
        classId: { type: 'number' },
        studentId: { type: 'number' },
        subjectId1: { type: 'number' },
        subjectId2: { type: 'number' },
        subjectId3: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putStudentExtracurricularSchema = {
  description: 'Update lesson plan data',
  summary: 'Update data',
  tags: ['student-extracurricular'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      classId: { type: 'number' },
      studentId: { type: 'number' },
      subjectId1: { type: 'number' },
      subjectId2: { type: 'number' },
      subjectId3: { type: 'number' },
    },
    required: [
      'departmentId',
      'levelId',
      'classId',
      'studentId',
      'subjectId1',
    ],
  },
  params: idParamsSchema,
}

const deleteStudentExtracurricularSchema = {
  description: 'Delete lesson plan data',
  summary: 'Delete data',
  tags: ['student-extracurricular'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postStudentExtracurricularSchema,
  getAllStudentExtracurricularSchema,
  getStudentExtracurricularSchema,
  putStudentExtracurricularSchema,
  deleteStudentExtracurricularSchema,
}
