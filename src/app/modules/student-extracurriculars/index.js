const { postStudentExtracurricular, putStudentExtracurricular, deleteStudentExtracurricular, getAllStudentExtracurricular, getStudentExtracurricular } = require('./handler')
const {
  postStudentExtracurricularSchema, getAllStudentExtracurricularSchema, getStudentExtracurricularSchema, putStudentExtracurricularSchema, deleteStudentExtracurricularSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postStudentExtracurricularSchema }, postStudentExtracurricular)
  app.get('/', { schema: getAllStudentExtracurricularSchema }, getAllStudentExtracurricular)
  app.get('/:id', { schema: getStudentExtracurricularSchema }, getStudentExtracurricular)
  app.put('/:id', { schema: putStudentExtracurricularSchema }, putStudentExtracurricular)
  app.delete('/:id', { schema: deleteStudentExtracurricularSchema }, deleteStudentExtracurricular)
}
