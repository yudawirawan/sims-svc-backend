const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postPsbPlaceSchema = {
  description: 'Create PSB Place data',
  tags: ['psb-places'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      processId: { type: 'number' },
      candidateId: { type: 'number' },
      groupId: { type: 'number' },
      generationId: { type: 'number' },
      levelId: { type: 'number' },
      classId: { type: 'number' },
      status: { type: 'number' },
    },
    required: [
      'departmentId',
      'processId',
      'candidateId',
      'groupId',
      'generationId',
      'levelId',
      'classId',
      'status',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            processId: { type: 'number' },
            groupId: { type: 'number' },
            generationId: { type: 'number' },
            levelId: { type: 'number' },
            classId: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllPsbPlaceSchema = {
  description: 'List all Psb Place data',
  tags: ['psb-places'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              processId: { type: 'number' },
              groupId: { type: 'number' },
              generationId: { type: 'number' },
              levelId: { type: 'number' },
              classId: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getPsbPlaceSchema = {
  description: 'Get Psb Place data by id',
  tags: ['psb-places'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        processId: { type: 'number' },
        groupId: { type: 'number' },
        generationId: { type: 'number' },
        levelId: { type: 'number' },
        classId: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putPsbPlaceSchema = {
  description: 'Update Psb Place data',
  summary: 'Update data',
  tags: ['psb-places'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      processId: { type: 'number' },
      candidateId: { type: 'number' },
      groupId: { type: 'number' },
      generationId: { type: 'number' },
      levelId: { type: 'number' },
      classId: { type: 'number' },
      status: { type: 'number' },
    },
    required: [
      'departmentId',
      'processId',
      'candidateId',
      'groupId',
      'generationId',
      'levelId',
      'classId',
      'status'
    ],
  },
  params: idParamsSchema,
}

const deletePsbPlaceSchema = {
  description: 'Delete Psb Place data',
  summary: 'Delete data',
  tags: ['psb-places'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

const deletePsbPlaceBatchSchema = {
  description: 'Delete batch psb place',
  summary: 'Delete psb place batch',
  tags: ['psb-places'],
  body: {
    type: 'object',
    properties: {
      ids: { type: 'array' },
    },
    required: ['ids'],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        username: { type: 'string' },
      }
    },
    ...err401,
  },
}

module.exports = {
  postPsbPlaceSchema,
  getAllPsbPlaceSchema,
  getPsbPlaceSchema,
  putPsbPlaceSchema,
  deletePsbPlaceSchema,
  deletePsbPlaceBatchSchema,
}
