const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createPsbPlace, updatePsbPlace, destroyPsbPlace
} = require('./service')

const postPsbPlace = async(req, reply) => {
  const res = await createPsbPlace(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllPsbPlace = async(req, reply) => {
  const student = await findPaging(req.query)
  return ok(reply, student, 'fetch data success')
}

const getPsbPlace = async(req, reply) => {
  const id = req.params.id
  const PsbPlace = await findById(id)
  if (!PsbPlace) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, PsbPlace, 'fetch data success')
}

const putPsbPlace = async(req, reply) => {
  const id = req.params.id
  const PsbPlace = await findById(id)
  if (!PsbPlace) {
    return notFound(reply, 'data not found')
  }
  const res = await updatePsbPlace(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const putPsbStudentToPlace = async(req, reply) => {
  const id = req.params.id
  const PsbCandidate = await findById(id)
  if (!PsbCandidate) {
    return notFound(reply, 'data not found')
  }
  const res = await updatePsbCandidate(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deletePsbPlace = async(req, reply) => {
  const id = req.params.id
  const PsbPlace = await findById(id)
  if (!PsbPlace) {
    return notFound(reply, 'data not found')
  }
  const res = destroyPsbPlace(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deletePsbPlaceBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyPsbPlace(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete users success')
}

module.exports = {
  postPsbPlace,
  getAllPsbPlace,
  getPsbPlace,
  putPsbPlace,
  deletePsbPlace,
  deletePsbPlaceBatch,
  putPsbStudentToPlace
}
