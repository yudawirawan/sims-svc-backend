const { postPsbPlace, putPsbPlace, deletePsbPlace, deletePsbPlaceBatch, getAllPsbPlace, getPsbPlace } = require('./handler')
const {
  postPsbPlaceSchema, getAllPsbPlaceSchema, getPsbPlaceSchema, putPsbPlaceSchema, deletePsbPlaceSchema, deletePsbPlaceBatchSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postPsbPlaceSchema }, postPsbPlace)
  app.get('/', { schema: getAllPsbPlaceSchema }, getAllPsbPlace)
  app.get('/:id', { schema: getPsbPlaceSchema }, getPsbPlace)
  app.put('/:id', { schema: putPsbPlaceSchema }, putPsbPlace)
  // app.put('/proses/:id', { schema: putPsbStudentToPlaceSchema}, putPsbStudentToPlace)
  app.delete('/:id', { schema: deletePsbPlaceSchema }, deletePsbPlace)
  app.post('/delete-batch', { schema: deletePsbPlaceBatchSchema }, deletePsbPlaceBatch)
}
