const app = require('./../../index')
const App = require('./../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'PsbPlace')

const findById = async(id) => {
  return await App.db.PsbPlace.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'psb_process', attributes: ['name'] },
    { association: 'psb_group', attributes: ['groupName'] },
    { association: 'generation', attributes: ['generation'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createPsbPlace = async(payload) => {
  try {
    const res = await App.db.PsbPlace.create(payload)
    const candidate = await App.db.PsbCandidate.findOne({ where: { id: payload.candidateId } })
    await App.db.PsbCandidate.update(
      { status: payload.status },
      { where: { candidateId: payload.candidateId } }
    )
    if (payload.status === 1) {
      await App.db.MasterStudent.create(candidate)
    }
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updatePsbPlace = async(id, payload) => {
  try {
    const res = await App.db.PsbPlace.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const putPsbStudentToPlace = async(id, payload) => {
  try {
    const res = await App.db.PsbCandidate.update(
      { status: payload.status },
      { where: { candidateId: payload.candidateId } }
    )
    if (payload.status === 1) {
      await App.db.MasterStudent.create(payload)
      await App.db.PsbPlace.update(payload, { where: { id } })
    } else if (payload.status === 0) {
      await App.db.MasterStudent.delete({ where: { registerId: payload.registerId } })
      await App.db.PsbPlace.delete({ where: { id } })
    }

    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyPsbPlace = async(id) => {
  try {
    await App.db.PsbPlace.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createPsbPlace,
  updatePsbPlace,
  destroyPsbPlace,
  putPsbStudentToPlace,
}
