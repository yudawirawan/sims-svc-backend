const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMasterGeneration, updateMasterGeneration, destroyMasterGeneration
} = require('./service')

const postMasterGeneration = async(req, reply) => {
  const res = await createMasterGeneration(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMasterGeneration = async(req, reply) => {
  const generation = await findPaging(req.query)
  return ok(reply, generation, 'fetch data success')
}

const getMasterGeneration = async(req, reply) => {
  const id = req.params.id
  const generation = await findById(id)
  if (!generation) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, generation, 'fetch data success')
}

const putMasterGeneration = async(req, reply) => {
  const id = req.params.id
  const generation = await findById(id)
  if (!generation) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMasterGeneration(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMasterGeneration = async(req, reply) => {
  const id = req.params.id
  const generation = await findById(id)
  if (!generation) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMasterGeneration(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMasterGeneration,
  getAllMasterGeneration,
  getMasterGeneration,
  putMasterGeneration,
  deleteMasterGeneration,
}
