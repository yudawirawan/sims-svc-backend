const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MasterGeneration')

const findById = async(id) => {
  return await App.db.MasterGeneration.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] },
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMasterGeneration = async(payload) => {
  try {
    const res = await App.db.MasterGeneration.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateMasterGeneration = async(id, payload) => {
  try {
    const res = await App.db.MasterGeneration.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyMasterGeneration = async(id) => {
  try {
    await App.db.MasterGeneration.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMasterGeneration,
  updateMasterGeneration,
  destroyMasterGeneration,
}
