const { postMasterGeneration, putMasterGeneration, deleteMasterGeneration, getAllMasterGeneration, getMasterGeneration } = require('./handler')
const {
  postMasterGenerationSchema, getAllMasterGenerationSchema, getMasterGenerationSchema, putMasterGenerationSchema, deleteMasterGenerationSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMasterGenerationSchema }, postMasterGeneration)
  app.get('/', { schema: getAllMasterGenerationSchema }, getAllMasterGeneration)
  app.get('/:id', { schema: getMasterGenerationSchema }, getMasterGeneration)
  app.put('/:id', { schema: putMasterGenerationSchema }, putMasterGeneration)
  app.delete('/:id', { schema: deleteMasterGenerationSchema }, deleteMasterGeneration)
}
