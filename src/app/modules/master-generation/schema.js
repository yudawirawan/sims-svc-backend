const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMasterGenerationSchema = {
  description: 'Create generation master data',
  tags: ['master-generation'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      generation: { type: 'string' },
      description: { type: 'string' },
      status: { type: 'number' },
    },
    required: [
      'departmentId',
      'generation',
      'status',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            generation: { type: 'string' },
            description: { type: 'string' },
            status: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMasterGenerationSchema = {
  description: 'List all generation master data',
  tags: ['master-generation'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              generation: { type: 'string' },
              description: { type: 'string' },
              status: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMasterGenerationSchema = {
  description: 'Get generation master data by id',
  tags: ['master-generation'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        generation: { type: 'string' },
        description: { type: 'string' },
        status: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMasterGenerationSchema = {
  description: 'Update User data',
  summary: 'Update data',
  tags: ['master-generation'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      generation: { type: 'string' },
      description: { type: 'string' },
      status: { type: 'number' },
    },
    required: [
      'departmentId',
      'generation',
      'status',
    ],
  },
  params: idParamsSchema,
}

const deleteMasterGenerationSchema = {
  description: 'Delete generation master data',
  summary: 'Delete data',
  tags: ['master-generation'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMasterGenerationSchema,
  getAllMasterGenerationSchema,
  getMasterGenerationSchema,
  putMasterGenerationSchema,
  deleteMasterGenerationSchema,
}
