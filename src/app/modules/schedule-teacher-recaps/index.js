const { postScheduleTeacherRecaps, putScheduleTeacherRecaps, deleteScheduleTeacherRecaps, getAllScheduleTeacherRecaps, getScheduleTeacherRecaps } = require('./handler')
const {
  postScheduleTeacherRecapsSchema, getAllScheduleTeacherRecapsSchema, getScheduleTeacherRecapsSchema, putScheduleTeacherRecapsSchema, deleteScheduleTeacherRecapsSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postScheduleTeacherRecapsSchema }, postScheduleTeacherRecaps)
  app.get('/', { schema: getAllScheduleTeacherRecapsSchema }, getAllScheduleTeacherRecaps)
  app.get('/:id', { schema: getScheduleTeacherRecapsSchema }, getScheduleTeacherRecaps)
  app.put('/:id', { schema: putScheduleTeacherRecapsSchema }, putScheduleTeacherRecaps)
  app.delete('/:id', { schema: deleteScheduleTeacherRecapsSchema }, deleteScheduleTeacherRecaps)
}
