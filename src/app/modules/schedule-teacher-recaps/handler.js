const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createScheduleTeacherRecaps, updateScheduleTeacherRecaps, destroyScheduleTeacherRecaps,
} = require('./service')

const postScheduleTeacherRecaps = async(req, reply) => {
  const res = await createScheduleTeacherRecaps(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllScheduleTeacherRecaps = async (req, reply) => {
  const schedule = await findPaging(req.query)
  return ok(reply, schedule, 'fetch data success')
}

const getScheduleTeacherRecaps = async(req, reply) => {
  const id = req.params.id
  const schedule = await findById(id)
  if (!schedule) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, schedule, 'fetch data success')
}

const putScheduleTeacherRecaps = async(req, reply) => {
  const id = req.params.id
  const schedule = await findById(id)
  if (!schedule) {
    return notFound(reply, 'data not found')
  }
  const res = await updateScheduleTeacherRecaps(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteScheduleTeacherRecaps = async(req, reply) => {
  const id = req.params.id
  const schedule = await findById(id)
  if (!schedule) {
    return notFound(reply, 'data not found')
  }
  const res = destroyScheduleTeacherRecaps(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deleteScheduleTeacherRecapsBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyScheduleTeacherRecaps(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete users success')
}

module.exports = {
  postScheduleTeacherRecaps,
  getAllScheduleTeacherRecaps,
  getScheduleTeacherRecaps,
  putScheduleTeacherRecaps,
  deleteScheduleTeacherRecaps,
  deleteScheduleTeacherRecapsBatch,
}
