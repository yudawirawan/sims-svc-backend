const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postScheduleTeacherRecapsSchema = {
  description: 'Create Schedule Teachers recap data',
  tags: ['schedule-teacher-recaps'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      schoolYearId: { type: 'number' },
      teacherId: { type: 'number' },
    },
    required: [
      'departmentId',
      'schoolYearId',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            schoolYearId: { type: 'number' },
            teacherId: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllScheduleTeacherRecapsSchema = {
  description: 'List all Schedule Teachers Process data',
  tags: ['schedule-teacher-recaps'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              schoolYearId: { type: 'number' },
              teacherId: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getScheduleTeacherRecapsSchema = {
  description: 'Get Schedule Teachers Process data by id',
  tags: ['schedule-teacher-recaps'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        schoolYearId: { type: 'number' },
        teacherId: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putScheduleTeacherRecapsSchema = {
  description: 'Update Schedule Teachers Process data',
  summary: 'Update data',
  tags: ['schedule-teacher-recaps'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      schoolYearId: { type: 'number' },
      teacherId: { type: 'number' },
    },
    required: [
      'departmentId',
      'schoolYearId',
    ],
  },
  params: idParamsSchema,
}

const deleteScheduleTeacherRecapsSchema = {
  description: 'Delete Schedule Teachers Process data',
  summary: 'Delete data',
  tags: ['schedule-teacher-recaps'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postScheduleTeacherRecapsSchema,
  getAllScheduleTeacherRecapsSchema,
  getScheduleTeacherRecapsSchema,
  putScheduleTeacherRecapsSchema,
  deleteScheduleTeacherRecapsSchema,
}
