const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'ScheduleTeacherRecap')

const findById = async(id) => {
  return await App.db.ScheduleTeacherRecaps.findOne({ where: { id } })
}

const findPaging = async (query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'schoolYear', attributes: ['schoolYear'] },
    { association: 'teacher', attributes: ['id'], 
      include: 
        { association: 'kepegawaian', attributes: ['nama'] },
    },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createScheduleTeacherRecaps = async(payload) => {
  try {
    const res = await App.db.ScheduleTeacherRecaps.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateScheduleTeacherRecaps = async(id, payload) => {
  try {
    const res = await App.db.ScheduleTeacherRecaps.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyScheduleTeacherRecaps = async(id) => {
  try {
    await App.db.ScheduleTeacherRecaps.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createScheduleTeacherRecaps,
  updateScheduleTeacherRecaps,
  destroyScheduleTeacherRecaps,
}
