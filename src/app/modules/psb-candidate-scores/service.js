const App = require('../../index')
const {Op} = require('sequelize')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'PsbCandidateScore')

const findById = async(id) => {
  return await App.db.PsbCandidateScore.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'candidate', attributes: ['nama'] },
    { association: 'candidateExam', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createPsbCandidateScore = async(payload) => {
  try {
    const res = await App.db.PsbCandidateScore.create(payload)
    await App.db.PsbCandidateScore.update( { status: 0 } , { where: { id: {[Op.ne]: res.id} } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updatePsbCandidateScore = async(id, payload) => {
  try {
    const res = await App.db.PsbCandidateScore.update(payload, { where: { id } })
    await App.db.PsbCandidateScore.update( { status: 0 } , { where: { id: {[Op.ne]: id} } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyPsbCandidateScore = async(id) => {
  try {
    await App.db.PsbCandidateScore.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createPsbCandidateScore,
  updatePsbCandidateScore,
  destroyPsbCandidateScore,
}
