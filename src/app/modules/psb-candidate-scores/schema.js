const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postPsbCandidateScoreSchema = {
  description: 'Create PSB score data',
  tags: ['psb-candidate-scores'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      candidateId: { type: 'number' },
      examId: { type: 'number' },
      score: { type: 'number' },
      status: { type: 'number' },
    },
    required: [
      'candidateId',
      'examId',
      'score',
      'status',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'string' },
            candidateId: { type: 'number' },
            examId: { type: 'number' },
            score: { type: 'number' },
            status: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllPsbCandidateScoreSchema = {
  description: 'List all PSB score data',
  tags: ['psb-candidate-scores'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'string' },
              candidateId: { type: 'number' },
              examId: { type: 'number' },
              score: { type: 'number' },
              status: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getPsbCandidateScoreSchema = {
  description: 'Get PSB score data by id',
  tags: ['psb-candidate-scores'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'string' },
        candidateId: { type: 'number' },
        examId: { type: 'number' },
        score: { type: 'number' },
        status: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putPsbCandidateScoreSchema = {
  description: 'Update PSB score data',
  summary: 'Update data',
  tags: ['psb-candidate-scores'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      candidateId: { type: 'number' },
      examId: { type: 'number' },
      score: { type: 'number' },
      status: { type: 'number' },
    },
    required: [
      'candidateId',
      'examId',
      'score',
      'status',
    ],
  },
  params: idParamsSchema,
}

const deletePsbCandidateScoreSchema = {
  description: 'Delete PSB score data',
  summary: 'Delete data',
  tags: ['psb-candidate-scores'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'string' }
      }
    },
    ...err401,
  },
}

const deletePsbCandidateScoreBatchSchema = {
  description: 'Delete batch PSB score',
  summary: 'Delete PSB score batch',
  tags: ['psb-candidate-scores'],
  body: {
    type: 'object',
    properties: {
      ids: { type: 'array' },
    },
    required: ['ids'],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'string' },
        username: { type: 'string' },
      }
    },
    ...err401,
  },
}

module.exports = {
  postPsbCandidateScoreSchema,
  getAllPsbCandidateScoreSchema,
  getPsbCandidateScoreSchema,
  putPsbCandidateScoreSchema,
  deletePsbCandidateScoreSchema,
  deletePsbCandidateScoreBatchSchema,
}
