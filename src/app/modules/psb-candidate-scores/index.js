const { postPsbCandidateScore, putPsbCandidateScore, deletePsbCandidateScore, getAllPsbCandidateScore, getPsbCandidateScore } = require('./handler')
const {
  postPsbCandidateScoreSchema, getAllPsbCandidateScoreSchema, getPsbCandidateScoreSchema, putPsbCandidateScoreSchema, putStatusPsbCandidateScoreSchema, deletePsbCandidateScoreSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postPsbCandidateScoreSchema }, postPsbCandidateScore)
  app.get('/', { schema: getAllPsbCandidateScoreSchema }, getAllPsbCandidateScore)
  app.get('/:id', { schema: getPsbCandidateScoreSchema }, getPsbCandidateScore)
  app.put('/:id', { schema: putPsbCandidateScoreSchema }, putPsbCandidateScore)
  app.delete('/:id', { schema: deletePsbCandidateScoreSchema }, deletePsbCandidateScore)
}
