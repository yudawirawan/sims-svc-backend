const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createPsbCandidateScore, updatePsbCandidateScore, destroyPsbCandidateScore
} = require('./service')

const postPsbCandidateScore = async(req, reply) => {
  const res = await createPsbCandidateScore(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllPsbCandidateScore = async(req, reply) => {
  const score = await findPaging(req.query)
  return ok(reply, score, 'fetch data success')
}

const getPsbCandidateScore = async(req, reply) => {
  const id = req.params.id
  const score = await findById(id)
  if (!score) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, score, 'fetch data success')
}

const putPsbCandidateScore = async(req, reply) => {
  const id = req.params.id
  const score = await findById(id)
  if (!score) {
    return notFound(reply, 'data not found')
  }
  const res = await updatePsbCandidateScore(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deletePsbCandidateScore = async(req, reply) => {
  const id = req.params.id
  const score = await findById(id)
  if (!score) {
    return notFound(reply, 'data not found')
  }
  const res = destroyPsbCandidateScore(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postPsbCandidateScore,
  getAllPsbCandidateScore,
  getPsbCandidateScore,
  putPsbCandidateScore,
  deletePsbCandidateScore,
}
