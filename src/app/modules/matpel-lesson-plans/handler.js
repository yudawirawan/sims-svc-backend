const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMatpelLessonPlan, updateMatpelLessonPlan, destroyMatpelLessonPlan
} = require('./service')

const postMatpelLessonPlan = async(req, reply) => {
  const res = await createMatpelLessonPlan(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMatpelLessonPlan = async(req, reply) => {
  const rpp = await findPaging(req.query)
  return ok(reply, rpp, 'fetch data success')
}

const getMatpelLessonPlan = async(req, reply) => {
  const id = req.params.id
  const rpp = await findById(id)
  if (!rpp) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, rpp, 'fetch data success')
}

const putMatpelLessonPlan = async(req, reply) => {
  const id = req.params.id
  const rpp = await findById(id)
  if (!rpp) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMatpelLessonPlan(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMatpelLessonPlan = async(req, reply) => {
  const id = req.params.id
  const rpp = await findById(id)
  if (!rpp) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMatpelLessonPlan(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMatpelLessonPlan,
  getAllMatpelLessonPlan,
  getMatpelLessonPlan,
  putMatpelLessonPlan,
  deleteMatpelLessonPlan,
}
