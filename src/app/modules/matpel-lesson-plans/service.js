const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MatpelLessonPlan')

const findById = async(id) => {
  return await App.db.MatpelLessonPlan.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'level', attributes: ['level'] },
    { association: 'semester', attributes: ['semester'] },
    { association: 'subject', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMatpelLessonPlan = async(payload) => {
  try {
    const res = await App.db.MatpelLessonPlan.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateMatpelLessonPlan = async(id, payload) => {
  try {
    const res = await App.db.MatpelLessonPlan.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyMatpelLessonPlan = async(id) => {
  try {
    await App.db.MatpelLessonPlan.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMatpelLessonPlan,
  updateMatpelLessonPlan,
  destroyMatpelLessonPlan,
}
