const { postMatpelLessonPlan, putMatpelLessonPlan, deleteMatpelLessonPlan, getAllMatpelLessonPlan, getMatpelLessonPlan } = require('./handler')
const {
  postMatpelLessonPlanSchema, getAllMatpelLessonPlanSchema, getMatpelLessonPlanSchema, putMatpelLessonPlanSchema, deleteMatpelLessonPlanSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMatpelLessonPlanSchema }, postMatpelLessonPlan)
  app.get('/', { schema: getAllMatpelLessonPlanSchema }, getAllMatpelLessonPlan)
  app.get('/:id', { schema: getMatpelLessonPlanSchema }, getMatpelLessonPlan)
  app.put('/:id', { schema: putMatpelLessonPlanSchema }, putMatpelLessonPlan)
  app.delete('/:id', { schema: deleteMatpelLessonPlanSchema }, deleteMatpelLessonPlan)
}
