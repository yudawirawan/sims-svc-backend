const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMatpelLessonPlanSchema = {
  description: 'Create lesson plan data',
  tags: ['matpel-lesson-plans'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      semesterId: { type: 'number' },
      subjectId: { type: 'number' },
      code: { type: 'string' },
      material: { type: 'string' },
      description: { type: 'string' },
      status: { type: 'number' },
    },
    required: [
      'departmentId',
      'levelId',
      'semesterId',
      'subjectId',
      'code',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            levelId: { type: 'number' },
            semesterId: { type: 'number' },
            subjectId: { type: 'number' },
            code: { type: 'string' },
            material: { type: 'string' },
            description: { type: 'string' },
            status: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMatpelLessonPlanSchema = {
  description: 'List all lesson plans data',
  tags: ['matpel-lesson-plans'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              levelId: { type: 'number' },
              semesterId: { type: 'number' },
              subjectId: { type: 'number' },
              code: { type: 'string' },
              material: { type: 'string' },
              description: { type: 'string' },
              status: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMatpelLessonPlanSchema = {
  description: 'Get lesson plan data by id',
  tags: ['matpel-lesson-plans'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        levelId: { type: 'number' },
        semesterId: { type: 'number' },
        subjectId: { type: 'number' },
        code: { type: 'string' },
        material: { type: 'string' },
        description: { type: 'string' },
        status: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMatpelLessonPlanSchema = {
  description: 'Update lesson plan data',
  summary: 'Update data',
  tags: ['matpel-lesson-plans'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      semesterId: { type: 'number' },
      subjectId: { type: 'number' },
      code: { type: 'string' },
      material: { type: 'string' },
      description: { type: 'string' },
      status: { type: 'number' },
    },
    required: [
      'departmentId',
      'levelId',
      'semesterId',
      'subjectId',
      'code',
    ],
  },
  params: idParamsSchema,
}

const deleteMatpelLessonPlanSchema = {
  description: 'Delete lesson plan data',
  summary: 'Delete data',
  tags: ['matpel-lesson-plans'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMatpelLessonPlanSchema,
  getAllMatpelLessonPlanSchema,
  getMatpelLessonPlanSchema,
  putMatpelLessonPlanSchema,
  deleteMatpelLessonPlanSchema,
}
