const { postMasterSemester, putMasterSemester, deleteMasterSemester, getAllMasterSemester, getMasterSemester } = require('./handler')
const {
  postMasterSemesterSchema, getAllMasterSemesterSchema, getMasterSemesterSchema, putMasterSemesterSchema, deleteMasterSemesterSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMasterSemesterSchema }, postMasterSemester)
  app.get('/', { schema: getAllMasterSemesterSchema }, getAllMasterSemester)
  app.get('/:id', { schema: getMasterSemesterSchema }, getMasterSemester)
  app.put('/:id', { schema: putMasterSemesterSchema }, putMasterSemester)
  app.delete('/:id', { schema: deleteMasterSemesterSchema }, deleteMasterSemester)
}
