const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMasterSemesterSchema = {
  description: 'Create semester data',
  tags: ['master-semesters'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      semester: { type: 'string' },
      description: { type: 'string' },
      status: { type: 'number' },
    },
    required: [
      'departmentId',
      'semester',
      'status'
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            semester: { type: 'string' },
            description: { type: 'string' },
            status: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMasterSemesterSchema = {
  description: 'List all school year data',
  tags: ['master-semesters'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              semester: { type: 'string' },
              description: { type: 'string' },
              status: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMasterSemesterSchema = {
  description: 'Get semester by id',
  tags: ['master-semesters'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        semester: { type: 'string' },
        description: { type: 'string' },
        status: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMasterSemesterSchema = {
  description: 'Update semster data',
  summary: 'Update data',
  tags: ['master-semesters'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      semester: { type: 'string' },
      description: { type: 'string' },
      status: { type: 'number' },
    },
    required: [
      'departmentId',
      'semester',
      'status',
    ],
  },
  params: idParamsSchema,
}

const deleteMasterSemesterSchema = {
  description: 'Delete school year data',
  summary: 'Delete data',
  tags: ['master-semesters'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMasterSemesterSchema,
  getAllMasterSemesterSchema,
  getMasterSemesterSchema,
  putMasterSemesterSchema,
  deleteMasterSemesterSchema,
}
