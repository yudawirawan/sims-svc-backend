const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMasterSemester, updateMasterSemester, destroyMasterSemester
} = require('./service')

const postMasterSemester = async(req, reply) => {
  const res = await createMasterSemester(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMasterSemester = async(req, reply) => {
  const semester = await findPaging(req.query)
  return ok(reply, semester, 'fetch data success')
}

const getMasterSemester = async(req, reply) => {
  const id = req.params.id
  const semester = await findById(id)
  if (!semester) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, semester, 'fetch data success')
}

const putMasterSemester = async(req, reply) => {
  const id = req.params.id
  const semester = await findById(id)
  if (!semester) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMasterSemester(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMasterSemester = async(req, reply) => {
  const id = req.params.id
  const semester = await findById(id)
  if (!semester) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMasterSemester(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMasterSemester,
  getAllMasterSemester,
  getMasterSemester,
  putMasterSemester,
  deleteMasterSemester,
}
