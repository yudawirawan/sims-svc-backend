const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MasterSemester')

const findById = async(id) => {
  return await App.db.MasterSemester.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMasterSemester = async(payload) => {
  try {
    const res = await App.db.MasterSemester.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateMasterSemester = async(id, payload) => {
  try {
    const res = await App.db.MasterSemester.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyMasterSemester = async(id) => {
  try {
    await App.db.MasterSemester.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMasterSemester,
  updateMasterSemester,
  destroyMasterSemester,
}
