const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createPsbCandidate, updatePsbCandidate, destroyPsbCandidate, genRegisterId
} = require('./service')

const postPsbCandidate = async(req, reply) => {
  const res = await createPsbCandidate(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllPsbCandidate = async(req, reply) => {
  const student = await findPaging(req.query)
  return ok(reply, student, 'fetch data success')
}

const getPsbCandidate = async(req, reply) => {
  const id = req.params.id
  const PsbCandidate = await findById(id)
  if (!PsbCandidate) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, PsbCandidate, 'fetch data success')
}

const putPsbCandidate = async(req, reply) => {
  const id = req.params.id
  const PsbCandidate = await findById(id)
  if (!PsbCandidate) {
    return notFound(reply, 'data not found')
  }
  const res = await updatePsbCandidate(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deletePsbCandidate = async(req, reply) => {
  const id = req.params.id
  const PsbCandidate = await findById(id)
  if (!PsbCandidate) {
    return notFound(reply, 'data not found')
  }
  const res = destroyPsbCandidate(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete psb candidate success')
}

const deletePsbCandidateBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyPsbCandidate(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete psb candidate batch success')
}

const postGenRegisterId = async(req, reply) => {
  const res = await genRegisterId()
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Fetch register id success')
}

module.exports = {
  postPsbCandidate,
  getAllPsbCandidate,
  getPsbCandidate,
  putPsbCandidate,
  deletePsbCandidate,
  deletePsbCandidateBatch,
  postGenRegisterId,
}
