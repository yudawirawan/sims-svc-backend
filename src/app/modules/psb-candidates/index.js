const {
  postPsbCandidate,
  putPsbCandidate,
  deletePsbCandidate,
  getAllPsbCandidate,
  getPsbCandidate,
  postGenRegisterId
} = require('./handler')
const {
  postPsbCandidateSchema,
  getAllPsbCandidateSchema,
  getPsbCandidateSchema,
  putPsbCandidateSchema,
  deletePsbCandidateSchema,
  genRegisterIdSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postPsbCandidateSchema }, postPsbCandidate)
  app.get('/', { schema: getAllPsbCandidateSchema }, getAllPsbCandidate)
  app.get('/:id', { schema: getPsbCandidateSchema }, getPsbCandidate)
  app.put('/:id', { schema: putPsbCandidateSchema }, putPsbCandidate)
  app.delete('/:id', { schema: deletePsbCandidateSchema }, deletePsbCandidate)
  app.get('/gen-reg-id', { schema: genRegisterIdSchema }, postGenRegisterId)
}
