const { putConfigSchool, getConfigSchool } = require('./handler')
const {
  getConfigSchoolSchema, putConfigSchoolSchema
} = require('./schema')

module.exports = async(app) => {
  app.get('/', { schema: getConfigSchoolSchema }, getConfigSchool)
  app.put('/', { schema: putConfigSchoolSchema }, putConfigSchool)
}
