const { err401 } = require('../../utils/http_schema')

const getConfigSchoolSchema = {
  description: 'Get Config School data by id',
  tags: ['config-school'],
  summary: 'Get data by id',
  public: true,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        logo: { type: 'string' },
        name: { type: 'string' },
        address: { type: 'string' },
        telp1: { type: 'string' },
        telp2: { type: 'string' },
        hp1: { type: 'string' },
        hp2: { type: 'string' },
        website: { type: 'string' },
        email: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putConfigSchoolSchema = {
  description: 'Update Config School data',
  summary: 'Update data',
  tags: ['config-school'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      logo: { type: 'string' },
      name: { type: 'string' },
      address: { type: 'string' },
      telp1: { type: 'string' },
      telp2: { type: 'string' },
      hp1: { type: 'string' },
      hp2: { type: 'string' },
      website: { type: 'string' },
      email: { type: 'string' },
    },
    required: [
      'logo',
      'name',
      'address',
      'telp1',
      'hp1',
      'website',
      'email',
    ],
  },
}

module.exports = {
  getConfigSchoolSchema,
  putConfigSchoolSchema,
}
