const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, updateConfigSchool,
} = require('./service')

const getConfigSchool = async(req, reply) => {
  const configSchool = await findById(1)
  if (!configSchool) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, configSchool, 'fetch data success')
}

const putConfigSchool = async(req, reply) => {
  const configSchool = await findById(1)
  if (!configSchool) {
    return notFound(reply, 'data not found')
  }
  const res = await updateConfigSchool(configSchool, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

module.exports = {
  getConfigSchool,
  putConfigSchool,
}
