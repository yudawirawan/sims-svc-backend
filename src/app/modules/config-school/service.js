const App = require('./../../index')

const findById = async(id) => {
  const include = [
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  return await App.db.ConfigSchool.findOne({ where: { id }, include, raw: false })
}

const createConfigSchool = async(payload) => {
  try {
    return await App.db.ConfigSchool.create(payload)
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateConfigSchool = async(configSchool, payload) => {
  try {
    return await App.db.ConfigSchool.update(payload, { where: { id: configSchool.id } })
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  createConfigSchool,
  updateConfigSchool,
}
