const App = require('../../index')
const { queryPaging } = require('../../utils/http_schema')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MasterSubject')


const findById = async(id) => {
  return await App.db.MasterSubject.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMatpelMaster = async(payload) => {
  try {
    const res = await App.db.MasterSubject.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateMatpelMaster = async(id, payload) => {
  try {
    const res = await App.db.MasterSubject.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyMatpelMaster = async(id) => {
  try {
    await App.db.MasterSubject.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMatpelMaster,
  updateMatpelMaster,
  destroyMatpelMaster,
}
