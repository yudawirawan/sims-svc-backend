const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMatpelMasterSchema = {
  description: 'Create subject data',
  tags: ['master-subjects'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      code: { type: 'string' },
      name: { type: 'string' },
      lessonHours: { type: 'number' },
      curriculumStatus: { type: 'string' },
      status: { type: 'number' },
      description: { type: 'string' },
    },
    required: [
      'departmentId',
      'code',
      'status',
      'name'
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            code: { type: 'string' },
            name: { type: 'string' },
            lessonHours: { type: 'number' },
            curriculumStatus: { type: 'string' },
            status: { type: 'number' },
            description: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMatpelMasterSchema = {
  description: 'List all subjects data',
  tags: ['master-subjects'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              code: { type: 'string' },
              name: { type: 'string' },
              lessonHours: { type: 'number' },
              curriculumStatus: { type: 'string' },
              status: { type: 'number' },
              description: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMatpelMasterSchema = {
  description: 'Get subject data by id',
  tags: ['master-subjects'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        code: { type: 'string' },
        name: { type: 'string' },
        lessonHours: { type: 'number' },
        curriculumStatus: { type: 'string' },
        status: { type: 'number' },
        description: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMatpelMasterSchema = {
  description: 'Update subject data',
  summary: 'Update data',
  tags: ['master-subjects'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      code: { type: 'string' },
      name: { type: 'string' },
      lessonHours: { type: 'number' },
      curriculumStatus: { type: 'string' },
      status: { type: 'number' },
      description: { type: 'string' },
    },
    required: [
      'departmentId',
      'code',
      'status',
      'name'
    ],
  },
  params: idParamsSchema,
}

const deleteMatpelMasterSchema = {
  description: 'Delete subject data',
  summary: 'Delete data',
  tags: ['master-subjects'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMatpelMasterSchema,
  getAllMatpelMasterSchema,
  getMatpelMasterSchema,
  putMatpelMasterSchema,
  deleteMatpelMasterSchema,
}
