const { postMatpelMaster, putMatpelMaster, deleteMatpelMaster, getAllMatpelMaster, getMatpelMaster } = require('./handler')
const {
  postMatpelMasterSchema, getAllMatpelMasterSchema, getMatpelMasterSchema, putMatpelMasterSchema, deleteMatpelMasterSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMatpelMasterSchema }, postMatpelMaster)
  app.get('/', { schema: getAllMatpelMasterSchema }, getAllMatpelMaster)
  app.get('/:id', { schema: getMatpelMasterSchema }, getMatpelMaster)
  app.put('/:id', { schema: putMatpelMasterSchema }, putMatpelMaster)
  app.delete('/:id', { schema: deleteMatpelMasterSchema }, deleteMatpelMaster)
}
