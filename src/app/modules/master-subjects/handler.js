const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMatpelMaster, updateMatpelMaster, destroyMatpelMaster
} = require('./service')

const postMatpelMaster = async(req, reply) => {
  const res = await createMatpelMaster(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMatpelMaster = async(req, reply) => {
  const subject = await findPaging(req.query)
  return ok(reply, subject, 'fetch data success')
}

const getMatpelMaster = async(req, reply) => {
  const id = req.params.id
  const subject = await findById(id)
  if (!subject) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, subject, 'fetch data success')
}

const putMatpelMaster = async(req, reply) => {
  const id = req.params.id
  const subject = await findById(id)
  if (!subject) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMatpelMaster(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMatpelMaster = async(req, reply) => {
  const id = req.params.id
  const subject = await findById(id)
  if (!subject) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMatpelMaster(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMatpelMaster,
  getAllMatpelMaster,
  getMatpelMaster,
  putMatpelMaster,
  deleteMatpelMaster,
}
