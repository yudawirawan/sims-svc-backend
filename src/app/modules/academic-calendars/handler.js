const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createAcademicCalendar, updateAcademicCalendar, destroyAcademicCalendar
} = require('./service')

const postAcademicCalendar = async(req, reply) => {
  const res = await createAcademicCalendar(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllAcademicCalendar = async(req, reply) => {
  const acad = await findPaging(req.query)
  return ok(reply, acad, 'fetch data success')
}

const getAcademicCalendar = async(req, reply) => {
  const id = req.params.id
  const acad = await findById(id)
  if (!acad) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, acad, 'fetch data success')
}

const putAcademicCalendar = async(req, reply) => {
  const id = req.params.id
  const acad = await findById(id)
  if (!acad) {
    return notFound(reply, 'data not found')
  }
  const res = await updateAcademicCalendar(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteAcademicCalendar = async(req, reply) => {
  const id = req.params.id
  const acad = await findById(id)
  if (!acad) {
    return notFound(reply, 'data not found')
  }
  const res = destroyAcademicCalendar(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postAcademicCalendar,
  getAllAcademicCalendar,
  getAcademicCalendar,
  putAcademicCalendar,
  deleteAcademicCalendar,
}
