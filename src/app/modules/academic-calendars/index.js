const { postAcademicCalendar, putAcademicCalendar, deleteAcademicCalendar, getAllAcademicCalendar, getAcademicCalendar } = require('./handler')
const {
  postAcademicCalendarSchema, getAllAcademicCalendarSchema, getAcademicCalendarSchema, putAcademicCalendarSchema, deleteAcademicCalendarSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postAcademicCalendarSchema }, postAcademicCalendar)
  app.get('/', { schema: getAllAcademicCalendarSchema }, getAllAcademicCalendar)
  app.get('/:id', { schema: getAcademicCalendarSchema }, getAcademicCalendar)
  app.put('/:id', { schema: putAcademicCalendarSchema }, putAcademicCalendar)
  app.delete('/:id', { schema: deleteAcademicCalendarSchema }, deleteAcademicCalendar)
}
