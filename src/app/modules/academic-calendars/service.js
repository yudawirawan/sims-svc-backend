const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'AcademicCalendar')

const findById = async(id) => {
  return await App.db.AcademicCalendar.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] },
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createAcademicCalendar = async(payload) => {
  try {
    const res = await App.db.AcademicCalendar.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateAcademicCalendar = async(id, payload) => {
  try {
    const res = await App.db.AcademicCalendar.update(payload, { where: { id } })
    return res
  } catch (err) {
    console.log(err)
    return {
      error: err.message,
    }
  }
}

const destroyAcademicCalendar = async(id) => {
  try {
    await App.db.AcademicCalendar.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createAcademicCalendar,
  updateAcademicCalendar,
  destroyAcademicCalendar,
}
