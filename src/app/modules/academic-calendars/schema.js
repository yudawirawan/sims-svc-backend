const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postAcademicCalendarSchema = {
  description: 'Create academic calendars data',
  tags: ['academic-calendars'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      color: { type: 'string' },
      name: { type: 'string' },
      startDate: { type: 'string' },
      endDate: { type: 'string' },
    },
    required: [
      'color',
      'name',
      'startDate',
      'endDate',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            color: { type: 'string' },
            name: { type: 'string' },
            startDate: { type: 'string' },
            endDate: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllAcademicCalendarSchema = {
  description: 'List all academic calendars data',
  tags: ['academic-calendars'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              color: { type: 'string' },
              name: { type: 'string' },
              startDate: { type: 'string' },
              endDate: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getAcademicCalendarSchema = {
  description: 'Get academic calendars data by id',
  tags: ['academic-calendars'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        color: { type: 'string' },
        name: { type: 'string' },
        startDate: { type: 'string' },
        endDate: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putAcademicCalendarSchema = {
  description: 'Update data',
  summary: 'Update data',
  tags: ['academic-calendars'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      color: { type: 'string' },
      name: { type: 'string' },
      startDate: { type: 'string' },
      endDate: { type: 'string' },
    },
    required: [
      'color',
      'name',
      'startDate',
      'endDate',
    ],
  },
  params: idParamsSchema,
}

const deleteAcademicCalendarSchema = {
  description: 'Delete academic calendars data',
  summary: 'Delete data',
  tags: ['academic-calendars'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postAcademicCalendarSchema,
  getAllAcademicCalendarSchema,
  getAcademicCalendarSchema,
  putAcademicCalendarSchema,
  deleteAcademicCalendarSchema,
}
