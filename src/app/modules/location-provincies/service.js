const App = require('./../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'LocationProvincies')

const findById = async(id) => {
  return await App.db.LocationProvincies.findOne({ where: { id } })
}

const findPaging = async (query) => {
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createLocationProvincies = async(payload) => {
  try {
    const res = await App.db.LocationProvincies.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateLocationProvincies = async(id, payload) => {
  try {
    const res = await App.db.LocationProvincies.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyLocationProvincies = async(id) => {
  try {
    await App.db.LocationProvincies.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createLocationProvincies,
  updateLocationProvincies,
  destroyLocationProvincies,
}
