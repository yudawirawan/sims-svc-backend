const { postLocationProvincies, putLocationProvincies, deleteLocationProvincies, deleteLocationProvinciesBatch, getAllLocationProvincies, getLocationProvincies } = require('./handler')
const {
  postLocationProvinciesSchema, getAllLocationProvinciesSchema, getLocationProvinciesSchema, putLocationProvinciesSchema, deleteLocationProvinciesSchema, deleteLocationProvinciesBatchSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postLocationProvinciesSchema }, postLocationProvincies)
  app.get('/', { schema: getAllLocationProvinciesSchema }, getAllLocationProvincies)
  app.get('/:id', { schema: getLocationProvinciesSchema }, getLocationProvincies)
  app.put('/:id', { schema: putLocationProvinciesSchema }, putLocationProvincies)
  app.delete('/:id', { schema: deleteLocationProvinciesSchema }, deleteLocationProvincies)
  app.post('/delete-batch', { schema: deleteLocationProvinciesBatchSchema }, deleteLocationProvinciesBatch)
}
