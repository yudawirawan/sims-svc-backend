const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postLocationProvinciesSchema = {
  description: 'Create Local Provincies data',
  tags: ['location-provincies'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      code: { type: 'string' },
      name: { type: 'string' },
      latitude: { type: 'string' },
      longitude: { type: 'string' },
    },
    required: [
      'code',
      'name',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            code: { type: 'string' },
            name: { type: 'string' },
            latitude: { type: 'string' },
            longitude: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllLocationProvinciesSchema = {
  description: 'List all Local Provincies data',
  tags: ['location-provincies'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              code: { type: 'string' },
              name: { type: 'string' },
              latitude: { type: 'string' },
              longitude: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getLocationProvinciesSchema = {
  description: 'Get Local Provincies data by id',
  tags: ['location-provincies'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        code: { type: 'string' },
        name: { type: 'string' },
        latitude: { type: 'string' },
        longitude: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putLocationProvinciesSchema = {
  description: 'Update Local Provincies data',
  summary: 'Update data',
  tags: ['location-provincies'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      code: { type: 'string' },
      name: { type: 'string' },
      latitude: { type: 'string' },
      longitude: { type: 'string' },
    },
    required: [
      'code',
      'name',
    ],
  },
  params: idParamsSchema,
}

const deleteLocationProvinciesSchema = {
  description: 'Delete Local Provincies data',
  summary: 'Delete data',
  tags: ['location-provincies'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

const deleteLocationProvinciesBatchSchema = {
  description: 'Delete provincies',
  summary: 'Delete provincies batch',
  tags: ['location-provincies'],
  body: {
    type: 'object',
    properties: {
      ids: { type: 'array' },
    },
    required: ['ids'],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        username: { type: 'string' },
      }
    },
    ...err401,
  },
}

module.exports = {
  postLocationProvinciesSchema,
  getAllLocationProvinciesSchema,
  getLocationProvinciesSchema,
  putLocationProvinciesSchema,
  deleteLocationProvinciesSchema,
  deleteLocationProvinciesBatchSchema,
}
