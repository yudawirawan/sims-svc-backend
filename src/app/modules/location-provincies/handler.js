const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createLocationProvincies, updateLocationProvincies, destroyLocationProvincies,
} = require('./service')

const postLocationProvincies = async(req, reply) => {
  const res = await createLocationProvincies(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllLocationProvincies = async (req, reply) => {
  const candidate = await findPaging(req.query)
  return ok(reply, candidate, 'fetch data success')
}

const getLocationProvincies = async(req, reply) => {
  const id = req.params.id
  const LocationProvincies = await findById(id)
  if (!LocationProvincies) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, LocationProvincies, 'fetch data success')
}

const putLocationProvincies = async(req, reply) => {
  const id = req.params.id
  const LocationProvincies = await findById(id)
  if (!LocationProvincies) {
    return notFound(reply, 'data not found')
  }
  const res = await updateLocationProvincies(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteLocationProvincies = async(req, reply) => {
  const id = req.params.id
  const LocationProvincies = await findById(id)
  if (!LocationProvincies) {
    return notFound(reply, 'data not found')
  }
  const res = destroyLocationProvincies(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deleteLocationProvinciesBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyLocationProvincies(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete provincies success')
}

module.exports = {
  postLocationProvincies,
  getAllLocationProvincies,
  getLocationProvincies,
  putLocationProvincies,
  deleteLocationProvincies,
  deleteLocationProvinciesBatch,
}
