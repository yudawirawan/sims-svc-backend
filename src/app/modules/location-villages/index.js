const { postLocationVillages, putLocationVillages, deleteLocationVillages, deleteLocationVillagesBatch, getAllLocationVillages, getLocationVillages } = require('./handler')
const {
  postLocationVillagesSchema, getAllLocationVillagesSchema, getLocationVillagesSchema, putLocationVillagesSchema, deleteLocationVillagesSchema, deleteLocationVillagesBatchSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postLocationVillagesSchema }, postLocationVillages)
  app.get('/', { schema: getAllLocationVillagesSchema }, getAllLocationVillages)
  app.get('/:id', { schema: getLocationVillagesSchema }, getLocationVillages)
  app.put('/:id', { schema: putLocationVillagesSchema }, putLocationVillages)
  app.delete('/:id', { schema: deleteLocationVillagesSchema }, deleteLocationVillages)
  app.post('/delete-batch', { schema: deleteLocationVillagesBatchSchema }, deleteLocationVillagesBatch)
}
