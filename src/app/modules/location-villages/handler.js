const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createLocationVillages, updateLocationVillages, destroyLocationVillages,
} = require('./service')

const postLocationVillages = async(req, reply) => {
  const res = await createLocationVillages(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllLocationVillages = async (req, reply) => {
  const candidate = await findPaging(req.query)
  return ok(reply, candidate, 'fetch data success')
}

const getLocationVillages = async(req, reply) => {
  const id = req.params.id
  const LocationVillages = await findById(id)
  if (!LocationVillages) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, LocationVillages, 'fetch data success')
}

const putLocationVillages = async(req, reply) => {
  const id = req.params.id
  const LocationVillages = await findById(id)
  if (!LocationVillages) {
    return notFound(reply, 'data not found')
  }
  const res = await updateLocationVillages(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteLocationVillages = async(req, reply) => {
  const id = req.params.id
  const LocationVillages = await findById(id)
  if (!LocationVillages) {
    return notFound(reply, 'data not found')
  }
  const res = destroyLocationVillages(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deleteLocationVillagesBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyLocationVillages(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete provincies success')
}

module.exports = {
  postLocationVillages,
  getAllLocationVillages,
  getLocationVillages,
  putLocationVillages,
  deleteLocationVillages,
  deleteLocationVillagesBatch,
}
