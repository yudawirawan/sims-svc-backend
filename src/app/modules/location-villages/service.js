const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'LocationVillages')

const findById = async(id) => {
  return await App.db.LocationVillages.findOne({ where: { id } })
}

const findPaging = async (query) => {
  query.include = [
    { association: 'district', attributes: ['name'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createLocationVillages = async(payload) => {
  try {
    const res = await App.db.LocationVillages.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateLocationVillages = async(id, payload) => {
  try {
    const res = await App.db.LocationVillages.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyLocationVillages = async(id) => {
  try {
    await App.db.LocationVillages.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createLocationVillages,
  updateLocationVillages,
  destroyLocationVillages,
}
