const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postLocationVillagesSchema = {
  description: 'Create Local Villages data',
  tags: ['local-villages'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      code: { type: 'string' },
      district_code: { type: 'number' },
      name: { type: 'string' },
      latitude: { type: 'string' },
      longitude: { type: 'string' },
    },
    required: [
      'code',
      'district_code',
      'name',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            code: { type: 'string' },
            district_code: { type: 'number' },
            name: { type: 'string' },
            latitude: { type: 'string' },
            longitude: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllLocationVillagesSchema = {
  description: 'List all Local Villages data',
  tags: ['local-villages'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              code: { type: 'string' },
              district_code: { type: 'number' },
              name: { type: 'string' },
              latitude: { type: 'string' },
              longitude: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getLocationVillagesSchema = {
  description: 'Get Local Villages data by id',
  tags: ['local-villages'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        code: { type: 'string' },
        district_code: { type: 'number' },
        name: { type: 'string' },
        latitude: { type: 'string' },
        longitude: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putLocationVillagesSchema = {
  description: 'Update Local Villages data',
  summary: 'Update data',
  tags: ['local-villages'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      code: { type: 'string' },
      district_code: { type: 'number' },
      name: { type: 'string' },
      latitude: { type: 'string' },
      longitude: { type: 'string' },
    },
    required: [
      'code',
      'district_code',
      'name',
    ],
  },
  params: idParamsSchema,
}

const deleteLocationVillagesSchema = {
  description: 'Delete Local Villages data',
  summary: 'Delete data',
  tags: ['local-villages'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

const deleteLocationVillagesBatchSchema = {
  description: 'Delete batch villages',
  summary: 'Delete villages batch',
  tags: ['local-villages'],
  body: {
    type: 'object',
    properties: {
      ids: { type: 'array' },
    },
    required: ['ids'],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        username: { type: 'string' },
      }
    },
    ...err401,
  },
}

module.exports = {
  postLocationVillagesSchema,
  getAllLocationVillagesSchema,
  getLocationVillagesSchema,
  putLocationVillagesSchema,
  deleteLocationVillagesSchema,
  deleteLocationVillagesBatchSchema,
}
