const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'Teacher')

const findById = async(id) => {
  return await App.db.Teacher.findOne({ where: { id } })
}

const findPaging = async(query) => {
  if (!query.include) {
    query.include = [
      { association: 'department', attributes: ['name'] },
      { association: 'subject', attributes: ['name'] },
      { association: 'kepegawaian', attributes: ['nama'] },
      { association: 'teacherStatus', attributes: ['name'] },
      { association: 'userCreatedBy', attributes: ['fullName'] },
      { association: 'userUpdatedBy', attributes: ['fullName'] }
    ]
  }
  return await repository.findPaging(query)
}

const createTeacher = async(payload) => {
  const transaction = await App.db.sequelize.transaction()
  try {
    const res = await App.db.Teacher.create(payload, { transaction })
    const matpel = []
    for (const mpl of payload.matpel) {
      mpl.kepegawaianId = res.kepegawaianId
      mpl.statusId = res.statusId
      const ex = await App.db.Teacher.create(mpl, { transaction })
      matpel.push(ex)
    }
    res.matpel = matpel
    await transaction.commit()
    return res
  } catch (err) {
    console.log(err)
    await transaction.rollback()
    return {
      error: err.message,
    }
  }
}

const updateTeacher = async(id, payload) => {
  try {
    const res = await App.db.Teacher.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyTeacher = async(id) => {
  try {
    await App.db.Teacher.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createTeacher,
  updateTeacher,
  destroyTeacher,
}
