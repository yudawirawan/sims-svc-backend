const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createTeacher, updateTeacher, destroyTeacher
} = require('./service')

const postTeacher = async(req, reply) => {
  const res = await createTeacher(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllTeacher = async(req, reply) => {
  const teacher = await findPaging(req.query)
  return ok(reply, teacher, 'fetch data success')
}

const getTeacher = async(req, reply) => {
  const id = req.params.id
  const teacher = await findById(id)
  if (!teacher) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, teacher, 'fetch data success')
}

const putTeacher = async(req, reply) => {
  const id = req.params.id
  const teacher = await findById(id)
  if (!teacher) {
    return notFound(reply, 'data not found')
  }
  const res = await updateTeacher(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteTeacher = async(req, reply) => {
  const id = req.params.id
  const teacher = await findById(id)
  if (!teacher) {
    return notFound(reply, 'data not found')
  }
  const res = destroyTeacher(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postTeacher,
  getAllTeacher,
  getTeacher,
  putTeacher,
  deleteTeacher,
}
