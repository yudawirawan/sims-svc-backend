const { postTeacher, putTeacher, deleteTeacher, getAllTeacher, getTeacher } = require('./handler')
const {
  postTeacherSchema, getAllTeacherSchema, getTeacherSchema, putTeacherSchema, deleteTeacherSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postTeacherSchema }, postTeacher)
  app.get('/', { schema: getAllTeacherSchema }, getAllTeacher)
  app.get('/:id', { schema: getTeacherSchema }, getTeacher)
  app.put('/:id', { schema: putTeacherSchema }, putTeacher)
  app.delete('/:id', { schema: deleteTeacherSchema }, deleteTeacher)
}
