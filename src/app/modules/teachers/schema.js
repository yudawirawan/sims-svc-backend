const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postTeacherSchema = {
  description: 'Create teacher data',
  tags: ['teacher'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      subjectId: { type: 'number' },
      matpel: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            kepegawaianId: { type: 'number' },
            statusId: { type: 'number' },
          }
        }
      },
    },
    required: [
      'departmentId',
      'subjectId',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            subjectId: { type: 'number' },
            kepegawaianId: { type: 'number' },
            statusId: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllTeacherSchema = {
  description: 'List all teacheres data',
  tags: ['teacher'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              subjectId: { type: 'number' },
              kepegawaianId: { type: 'number' },
              statusId: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getTeacherSchema = {
  description: 'Get teacher data by id',
  tags: ['teacher'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        subjectId: { type: 'number' },
        kepegawaianId: { type: 'number' },
        statusId: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putTeacherSchema = {
  description: 'Update teacher data',
  summary: 'Update data',
  tags: ['teacher'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      subjectId: { type: 'number' },
      kepegawaianId: { type: 'number' },
      statusId: { type: 'number' },
    },
    required: [
      'departmentId',
      'subjectId',
      'kepegawaianId',
      'statusId',
    ],
  },
  params: idParamsSchema,
}

const deleteTeacherSchema = {
  description: 'Delete teacher data',
  summary: 'Delete data',
  tags: ['teacher'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postTeacherSchema,
  getAllTeacherSchema,
  getTeacherSchema,
  putTeacherSchema,
  deleteTeacherSchema,
}
