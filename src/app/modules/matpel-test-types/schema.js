const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMatpelTestTypeSchema = {
  description: 'Create data type data',
  tags: ['matpel-test-types'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      subjectId: { type: 'number' },
      testType: { type: 'string' },
      abbreviation: { type: 'string' },
      status: { type: 'number' },
      description: { type: 'string' },
    },
    required: [
      'departmentId',
      'subjectId',
      'testType',
      'abbreviation',
      'status',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            subjectId: { type: 'number' },
            testType: { type: 'string' },
            abbreviation: { type: 'string' },
            status: { type: 'number' },
            description: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMatpelTestTypeSchema = {
  description: 'List all data types data',
  tags: ['matpel-test-types'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              subjectId: { type: 'number' },
              testType: { type: 'string' },
              abbreviation: { type: 'string' },
              status: { type: 'number' },
              description: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMatpelTestTypeSchema = {
  description: 'Get data type data by id',
  tags: ['matpel-test-types'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        subjectId: { type: 'number' },
        testType: { type: 'string' },
        abbreviation: { type: 'string' },
        status: { type: 'number' },
        description: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMatpelTestTypeSchema = {
  description: 'Update data type data',
  summary: 'Update data',
  tags: ['matpel-test-types'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      subjectId: { type: 'number' },
      testType: { type: 'string' },
      abbreviation: { type: 'string' },
      status: { type: 'number' },
      description: { type: 'string' },
    },
    required: [
      'departmentId',
      'subjectId',
      'testType',
      'abbreviation',
      'status',
    ],
  },
  params: idParamsSchema,
}

const deleteMatpelTestTypeSchema = {
  description: 'Delete data type data',
  summary: 'Delete data',
  tags: ['matpel-test-types'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMatpelTestTypeSchema,
  getAllMatpelTestTypeSchema,
  getMatpelTestTypeSchema,
  putMatpelTestTypeSchema,
  deleteMatpelTestTypeSchema,
}
