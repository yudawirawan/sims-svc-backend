const { postMatpelTestType, putMatpelTestType, deleteMatpelTestType, getAllMatpelTestType, getMatpelTestType } = require('./handler')
const {
  postMatpelTestTypeSchema, getAllMatpelTestTypeSchema, getMatpelTestTypeSchema, putMatpelTestTypeSchema, deleteMatpelTestTypeSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMatpelTestTypeSchema }, postMatpelTestType)
  app.get('/', { schema: getAllMatpelTestTypeSchema }, getAllMatpelTestType)
  app.get('/:id', { schema: getMatpelTestTypeSchema }, getMatpelTestType)
  app.put('/:id', { schema: putMatpelTestTypeSchema }, putMatpelTestType)
  app.delete('/:id', { schema: deleteMatpelTestTypeSchema }, deleteMatpelTestType)
}
