const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMatpelTestType, updateMatpelTestType, destroyMatpelTestType
} = require('./service')

const postMatpelTestType = async(req, reply) => {
  const res = await createMatpelTestType(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMatpelTestType = async(req, reply) => {
  const test = await findPaging(req.query)
  return ok(reply, test, 'fetch data success')
}

const getMatpelTestType = async(req, reply) => {
  const id = req.params.id
  const test = await findById(id)
  if (!test) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, test, 'fetch data success')
}

const putMatpelTestType = async(req, reply) => {
  const id = req.params.id
  const test = await findById(id)
  if (!test) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMatpelTestType(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMatpelTestType = async(req, reply) => {
  const id = req.params.id
  const test = await findById(id)
  if (!test) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMatpelTestType(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMatpelTestType,
  getAllMatpelTestType,
  getMatpelTestType,
  putMatpelTestType,
  deleteMatpelTestType,
}
