const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MatpelTestType')

const findById = async(id) => {
  return await App.db.MatpelTestType.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'subject', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMatpelTestType = async(payload) => {
  try {
    const res = await App.db.MatpelTestType.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateMatpelTestType = async(id, payload) => {
  try {
    const res = await App.db.MatpelTestType.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyMatpelTestType = async(id) => {
  try {
    await App.db.MatpelTestType.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMatpelTestType,
  updateMatpelTestType,
  destroyMatpelTestType,
}
