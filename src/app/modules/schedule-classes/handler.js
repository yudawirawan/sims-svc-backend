const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createScheduleClasses, updateScheduleClasses, destroyScheduleClasses,
} = require('./service')

const postScheduleClasses = async(req, reply) => {
  const res = await createScheduleClasses(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllScheduleClasses = async (req, reply) => {
  const schedule = await findPaging(req.query)
  return ok(reply, schedule, 'fetch data success')
}

const getScheduleClasses = async(req, reply) => {
  const id = req.params.id
  const schedule = await findById(id)
  if (!schedule) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, schedule, 'fetch data success')
}

const putScheduleClasses = async(req, reply) => {
  const id = req.params.id
  const schedule = await findById(id)
  if (!schedule) {
    return notFound(reply, 'data not found')
  }
  const res = await updateScheduleClasses(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteScheduleClasses = async(req, reply) => {
  const id = req.params.id
  const schedule = await findById(id)
  if (!schedule) {
    return notFound(reply, 'data not found')
  }
  const res = destroyScheduleClasses(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deleteScheduleClassesBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyScheduleClasses(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete users success')
}

module.exports = {
  postScheduleClasses,
  getAllScheduleClasses,
  getScheduleClasses,
  putScheduleClasses,
  deleteScheduleClasses,
  deleteScheduleClassesBatch,
}
