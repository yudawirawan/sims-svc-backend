const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'ScheduleClass')

const findById = async(id) => {
  return await App.db.ScheduleClass.findOne({ where: { id } })
}

const findPaging = async (query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'schoolYear', attributes: ['schoolYear'] },
    { association: 'level', attributes: ['level'] },
    { association: 'subject', attributes: ['name'] },
    { association: 'teacher', attributes: ['id'], 
      include: 
        { association: 'kepegawaian', attributes: ['nama'] },
    },
    { association: 'startStudyHour', attributes: ['startTime'] },
    { association: 'finishStudyHour', attributes: ['finishTime'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createScheduleClasses = async(payload) => {
  try {
    const res = await App.db.ScheduleClass.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateScheduleClasses = async(id, payload) => {
  try {
    const res = await App.db.ScheduleClass.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyScheduleClasses = async(id) => {
  try {
    await App.db.ScheduleClass.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createScheduleClasses,
  updateScheduleClasses,
  destroyScheduleClasses,
}
