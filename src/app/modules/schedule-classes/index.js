const { postScheduleClasses, putScheduleClasses, deleteScheduleClasses, getAllScheduleClasses, getScheduleClasses } = require('./handler')
const {
  postScheduleClassesSchema, getAllScheduleClassesSchema, getScheduleClassesSchema, putScheduleClassesSchema, deleteScheduleClassesSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postScheduleClassesSchema }, postScheduleClasses)
  app.get('/', { schema: getAllScheduleClassesSchema }, getAllScheduleClasses)
  app.get('/:id', { schema: getScheduleClassesSchema }, getScheduleClasses)
  app.put('/:id', { schema: putScheduleClassesSchema }, putScheduleClasses)
  app.delete('/:id', { schema: deleteScheduleClassesSchema }, deleteScheduleClasses)
}
