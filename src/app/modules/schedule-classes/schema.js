const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postScheduleClassesSchema = {
  description: 'Create Schedule Schedule Classes data',
  tags: ['schedule-classes'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      schoolYearId: { type: 'number' },
      levelId: { type: 'number' },
      subjectId: { type: 'number' },
      teacherId: { type: 'number' },
      day: { type: 'string' },
      startStudyHourId: { type: 'number' },
      finishStudyHourId: { type: 'number' },
    },
    required: [
      'departmentId',
      'schoolYearId',
      'levelId',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            schoolYearId: { type: 'number' },
            levelId: { type: 'number' },
            subjectId: { type: 'number' },
            teacherId: { type: 'number' },
            day: { type: 'string' },
            startStudyHourId: { type: 'number' },
            finishStudyHourId: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllScheduleClassesSchema = {
  description: 'List all Schedule Schedule Classes data',
  tags: ['schedule-classes'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              schoolYearId: { type: 'number' },
              levelId: { type: 'number' },
              subjectId: { type: 'number' },
              teacherId: { type: 'number' },
              day: { type: 'string' },
              startStudyHourId: { type: 'number' },
              finishStudyHourId: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getScheduleClassesSchema = {
  description: 'Get Schedule Schedule Classes data by id',
  tags: ['schedule-classes'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        schoolYearId: { type: 'number' },
        levelId: { type: 'number' },
        subjectId: { type: 'number' },
        teacherId: { type: 'number' },
        day: { type: 'string' },
        startStudyHourId: { type: 'number' },
        finishStudyHourId: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putScheduleClassesSchema = {
  description: 'Update Schedule Schedule Classes data',
  summary: 'Update data',
  tags: ['schedule-classes'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      schoolYearId: { type: 'number' },
      levelId: { type: 'number' },
      subjectId: { type: 'number' },
      teacherId: { type: 'number' },
      day: { type: 'string' },
      startStudyHourId: { type: 'number' },
      finishStudyHourId: { type: 'number' },
    },
    required: [
      'departmentId',
      'schoolYearId',
      'levelId',
    ],
  },
  params: idParamsSchema,
}

const deleteScheduleClassesSchema = {
  description: 'Delete Schedule Schedule Classes data',
  summary: 'Delete data',
  tags: ['schedule-classes'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postScheduleClassesSchema,
  getAllScheduleClassesSchema,
  getScheduleClassesSchema,
  putScheduleClassesSchema,
  deleteScheduleClassesSchema,
}
