const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMatpelWeighting, updateMatpelWeighting, destroyMatpelWeighting
} = require('./service')

const postMatpelWeighting = async(req, reply) => {
  const res = await createMatpelWeighting(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMatpelWeighting = async(req, reply) => {
  const weight = await findPaging(req.query)
  return ok(reply, weight, 'fetch data success')
}

const getMatpelWeighting = async(req, reply) => {
  const id = req.params.id
  const weight = await findById(id)
  if (!weight) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, weight, 'fetch data success')
}

const putMatpelWeighting = async(req, reply) => {
  const id = req.params.id
  const weight = await findById(id)
  if (!weight) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMatpelWeighting(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMatpelWeighting = async(req, reply) => {
  const id = req.params.id
  const weight = await findById(id)
  if (!weight) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMatpelWeighting(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMatpelWeighting,
  getAllMatpelWeighting,
  getMatpelWeighting,
  putMatpelWeighting,
  deleteMatpelWeighting,
}
