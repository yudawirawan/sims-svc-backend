const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MatpelWeighting')

const findById = async(id) => {
  return await App.db.MatpelWeighting.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'teacher', attributes: ['kepegawaian_id'], 
      include: [ 
        { association: 'kepegawaian', attributes: ['nama', 'nik'] } 
      ] 
    },
    { association: 'testType', attributes: ['testType'] },
    { association: 'department', attributes: ['name'] },
    { association: 'level', attributes: ['level'] },
    { association: 'assessmentAspect', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMatpelWeighting = async(payload) => {
  try {
    const res = await App.db.MatpelWeighting.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateMatpelWeighting = async(id, payload) => {
  try {
    const res = await App.db.MatpelWeighting.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyMatpelWeighting = async(id) => {
  try {
    await App.db.MatpelWeighting.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMatpelWeighting,
  updateMatpelWeighting,
  destroyMatpelWeighting,
}
