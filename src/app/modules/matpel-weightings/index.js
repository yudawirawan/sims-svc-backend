const { postMatpelWeighting, putMatpelWeighting, deleteMatpelWeighting, getAllMatpelWeighting, getMatpelWeighting } = require('./handler')
const {
  postMatpelWeightingSchema, getAllMatpelWeightingSchema, getMatpelWeightingSchema, putMatpelWeightingSchema, deleteMatpelWeightingSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMatpelWeightingSchema }, postMatpelWeighting)
  app.get('/', { schema: getAllMatpelWeightingSchema }, getAllMatpelWeighting)
  app.get('/:id', { schema: getMatpelWeightingSchema }, getMatpelWeighting)
  app.put('/:id', { schema: putMatpelWeightingSchema }, putMatpelWeighting)
  app.delete('/:id', { schema: deleteMatpelWeightingSchema }, deleteMatpelWeighting)
}
