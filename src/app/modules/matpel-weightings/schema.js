const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMatpelWeightingSchema = {
  description: 'Create matpel weighting data',
  tags: ['matpel-weightings'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      teacherId: { type: 'number' },
      testTypeId: { type: 'number' },
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      assessmentAspectId: { type: 'number' },
      weight: { type: 'string' },
    },
    required: [
      'teacherId',
      'testTypeId',
      'departmentId',
      'levelId',
      'weight',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            teacherId: { type: 'number' },
            testTypeId: { type: 'number' },
            departmentId: { type: 'number' },
            levelId: { type: 'number' },
            assessmentAspectId: { type: 'number' },
            weight: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMatpelWeightingSchema = {
  description: 'List all matpel weightings data',
  tags: ['matpel-weightings'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              teacherId: { type: 'number' },
              testTypeId: { type: 'number' },
              departmentId: { type: 'number' },
              levelId: { type: 'number' },
              assessmentAspectId: { type: 'number' },
              weight: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMatpelWeightingSchema = {
  description: 'Get matpel weighting data by id',
  tags: ['matpel-weightings'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        teacherId: { type: 'number' },
        testTypeId: { type: 'number' },
        departmentId: { type: 'number' },
        levelId: { type: 'number' },
        assessmentAspectId: { type: 'number' },
        weight: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMatpelWeightingSchema = {
  description: 'Update matpel weighting data',
  summary: 'Update data',
  tags: ['matpel-weightings'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      teacherId: { type: 'number' },
      testTypeId: { type: 'number' },
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      assessmentAspectId: { type: 'number' },
      weight: { type: 'string' },
    },
    required: [
      'teacherId',
      'testTypeId',
      'departmentId',
      'levelId',
      'weight',
    ],
  },
  params: idParamsSchema,
}

const deleteMatpelWeightingSchema = {
  description: 'Delete matpel weighting data',
  summary: 'Delete data',
  tags: ['matpel-weightings'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMatpelWeightingSchema,
  getAllMatpelWeightingSchema,
  getMatpelWeightingSchema,
  putMatpelWeightingSchema,
  deleteMatpelWeightingSchema,
}
