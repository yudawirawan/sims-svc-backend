const { postLocationDistricts, putLocationDistricts, deleteLocationDistricts, deleteLocationDistrictsBatch, getAllLocationDistricts, getLocationDistricts } = require('./handler')
const {
  postLocationDistrictsSchema, getAllLocationDistrictsSchema, getLocationDistrictsSchema, putLocationDistrictsSchema, deleteLocationDistrictsSchema, deleteLocationDistrictsBatchSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postLocationDistrictsSchema }, postLocationDistricts)
  app.get('/', { schema: getAllLocationDistrictsSchema }, getAllLocationDistricts)
  app.get('/:id', { schema: getLocationDistrictsSchema }, getLocationDistricts)
  app.put('/:id', { schema: putLocationDistrictsSchema }, putLocationDistricts)
  app.delete('/:id', { schema: deleteLocationDistrictsSchema }, deleteLocationDistricts)
  app.post('/delete-batch', { schema: deleteLocationDistrictsBatchSchema }, deleteLocationDistrictsBatch)
}
