const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'LocationDistricts')

const findById = async(id) => {
  return await App.db.LocationDistricts.findOne({ where: { id } })
}

const findPaging = async (query) => {
  query.include = [
    { association: 'city', attributes: ['name'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createLocationDistricts = async(payload) => {
  try {
    const res = await App.db.LocationDistricts.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateLocationDistricts = async(id, payload) => {
  try {
    const res = await App.db.LocationDistricts.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyLocationDistricts = async(id) => {
  try {
    await App.db.LocationDistricts.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createLocationDistricts,
  updateLocationDistricts,
  destroyLocationDistricts,
}
