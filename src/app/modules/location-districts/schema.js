const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postLocationDistrictsSchema = {
  description: 'Create Local Districts data',
  tags: ['local-districts'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      code: { type: 'string' },
      city_code: { type: 'number' },
      name: { type: 'string' },
      latitude: { type: 'string' },
      longitude: { type: 'string' },
    },
    required: [
      'code',
      'city_code',
      'name',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            code: { type: 'string' },
            city_code: { type: 'number' },
            name: { type: 'string' },
            latitude: { type: 'string' },
            longitude: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllLocationDistrictsSchema = {
  description: 'List all Local Districts data',
  tags: ['local-districts'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              code: { type: 'string' },
              city_code: { type: 'number' },
              name: { type: 'string' },
              latitude: { type: 'string' },
              longitude: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getLocationDistrictsSchema = {
  description: 'Get Local Districts data by id',
  tags: ['local-districts'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        code: { type: 'string' },
        city_code: { type: 'number' },
        name: { type: 'string' },
        latitude: { type: 'string' },
        longitude: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putLocationDistrictsSchema = {
  description: 'Update Local Districts data',
  summary: 'Update data',
  tags: ['local-districts'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      code: { type: 'string' },
      city_code: { type: 'number' },
      name: { type: 'string' },
      latitude: { type: 'string' },
      longitude: { type: 'string' },
    },
    required: [
      'code',
      'city_code',
      'name',
    ],
  },
  params: idParamsSchema,
}

const deleteLocationDistrictsSchema = {
  description: 'Delete Local Districts data',
  summary: 'Delete data',
  tags: ['local-districts'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

const deleteLocationDistrictsBatchSchema = {
  description: 'Delete batch districts',
  summary: 'Delete districts batch',
  tags: ['local-districts'],
  body: {
    type: 'object',
    properties: {
      ids: { type: 'array' },
    },
    required: ['ids'],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        username: { type: 'string' },
      }
    },
    ...err401,
  },
}

module.exports = {
  postLocationDistrictsSchema,
  getAllLocationDistrictsSchema,
  getLocationDistrictsSchema,
  putLocationDistrictsSchema,
  deleteLocationDistrictsSchema,
  deleteLocationDistrictsBatchSchema,
}
