const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createLocationDistricts, updateLocationDistricts, destroyLocationDistricts,
} = require('./service')

const postLocationDistricts = async(req, reply) => {
  const res = await createLocationDistricts(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllLocationDistricts = async (req, reply) => {
  const candidate = await findPaging(req.query)
  return ok(reply, candidate, 'fetch data success')
}

const getLocationDistricts = async(req, reply) => {
  const id = req.params.id
  const LocationDistricts = await findById(id)
  if (!LocationDistricts) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, LocationDistricts, 'fetch data success')
}

const putLocationDistricts = async(req, reply) => {
  const id = req.params.id
  const LocationDistricts = await findById(id)
  if (!LocationDistricts) {
    return notFound(reply, 'data not found')
  }
  const res = await updateLocationDistricts(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteLocationDistricts = async(req, reply) => {
  const id = req.params.id
  const LocationDistricts = await findById(id)
  if (!LocationDistricts) {
    return notFound(reply, 'data not found')
  }
  const res = destroyLocationDistricts(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deleteLocationDistrictsBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyLocationDistricts(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete provincies success')
}

module.exports = {
  postLocationDistricts,
  getAllLocationDistricts,
  getLocationDistricts,
  putLocationDistricts,
  deleteLocationDistricts,
  deleteLocationDistrictsBatch,
}
