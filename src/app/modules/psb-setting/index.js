const { postPsbSetting, putPsbSetting, deletePsbSetting, deletePsbSettingBatch, getAllPsbSetting, getPsbSetting } = require('./handler')
const {
  postPsbSettingSchema, getAllPsbSettingSchema, getPsbSettingSchema, putPsbSettingSchema, deletePsbSettingSchema, deletePsbSettingBatchSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postPsbSettingSchema }, postPsbSetting)
  app.get('/', { schema: getAllPsbSettingSchema }, getAllPsbSetting)
  app.get('/:id', { schema: getPsbSettingSchema }, getPsbSetting)
  app.put('/:id', { schema: putPsbSettingSchema }, putPsbSetting)
  app.delete('/:id', { schema: deletePsbSettingSchema }, deletePsbSetting)
  app.post('/delete-batch', { schema: deletePsbSettingBatchSchema }, deletePsbSettingBatch)
}
