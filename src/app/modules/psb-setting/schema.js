const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postPsbSettingSchema = {
  description: 'Create PSB Setting data',
  tags: ['psb-setting'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      processId: { type: 'number' },
      mandatoryFeeCode: { type: 'string' },
      mandatoryFeeName: { type: 'string' },
      voluntaryFeeCode: { type: 'string' },
      voluntaryFeeName: { type: 'string' },
      status: { type: 'number', default: 1 },
      exams: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            code: { type: 'string' },
            name: { type: 'string' },
            status: { type: 'number', default: 1 },
          }
        }
      },
    },
    required: [
      'departmentId',
      'processId',
      'status',
      'exams',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            processId: { type: 'number' },
            mandatoryFeeCode: { type: 'string' },
            mandatoryFeeName: { type: 'string' },
            voluntaryFeeCode: { type: 'string' },
            voluntaryFeeName: { type: 'string' },
            status: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllPsbSettingSchema = {
  description: 'List all PSB Setting data',
  tags: ['psb-setting'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              processId: { type: 'number' },
              mandatoryFeeCode: { type: 'string' },
              mandatoryFeeName: { type: 'string' },
              voluntaryFeeCode: { type: 'string' },
              voluntaryFeeName: { type: 'string' },
              status: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getPsbSettingSchema = {
  description: 'Get PSB Setting data by id',
  tags: ['psb-setting'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        department: {
          type: 'object',
          properties: {
            name: { type: 'string' },
          }
        },
        processId: { type: 'number' },
        process: {
          type: 'object',
          properties: {
            name: { type: 'string' },
          }
        },
        mandatoryFeeCode: { type: 'string' },
        mandatoryFeeName: { type: 'string' },
        voluntaryFeeCode: { type: 'string' },
        voluntaryFeeName: { type: 'string' },
        status: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putPsbSettingSchema = {
  description: 'Update PSB Setting data',
  summary: 'Update data',
  tags: ['psb-setting'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      processId: { type: 'number' },
      mandatoryFeeCode: { type: 'string' },
      mandatoryFeeName: { type: 'string' },
      voluntaryFeeCode: { type: 'string' },
      voluntaryFeeName: { type: 'string' },
      status: { type: 'number' },
      exams: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            code: { type: 'string' },
            name: { type: 'string' },
            status: { type: 'number', default: 1 },
          }
        }
      },
    },
    required: [
      'departmentId',
      'processId',
      'status',
    ],
  },
  params: idParamsSchema,
}

const deletePsbSettingSchema = {
  description: 'Delete PSB Setting data',
  summary: 'Delete data',
  tags: ['psb-setting'],
  params: idParamsSchema,
  response: {
    204: {
      type: 'null',
      description: 'Success delete'
    },
    ...err401,
  },
}

const deletePsbSettingBatchSchema = {
  description: 'Delete batch PSB Setting',
  summary: 'Delete PSB Setting batch',
  tags: ['psb-setting'],
  body: {
    type: 'object',
    properties: {
      ids: { type: 'array' },
    },
    required: ['ids'],
  },
  response: {
    204: {
      type: 'null',
      description: 'Success deletes',
    },
    ...err401,
  },
}

module.exports = {
  postPsbSettingSchema,
  getAllPsbSettingSchema,
  getPsbSettingSchema,
  putPsbSettingSchema,
  deletePsbSettingSchema,
  deletePsbSettingBatchSchema,
}
