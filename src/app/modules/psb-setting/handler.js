const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createPsbSetting, updatePsbSetting, destroyPsbSetting,
} = require('./service')

const postPsbSetting = async(req, reply) => {
  const res = await createPsbSetting(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllPsbSetting = async(req, reply) => {
  const response = await findPaging(req.query)
  return ok(reply, response, 'fetch data success')
}

const getPsbSetting = async(req, reply) => {
  const id = req.params.id
  const PsbSetting = await findById(id)
  if (!PsbSetting) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, PsbSetting, 'fetch data success')
}

const putPsbSetting = async(req, reply) => {
  const id = req.params.id
  const PsbSetting = await findById(id)
  if (!PsbSetting) {
    return notFound(reply, 'data not found')
  }
  const res = await updatePsbSetting(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deletePsbSetting = async(req, reply) => {
  const id = req.params.id
  const PsbSetting = await findById(id)
  if (!PsbSetting) {
    return notFound(reply, 'data not found')
  }
  const res = destroyPsbSetting(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deletePsbSettingBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyPsbSetting(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete Psb Setting success')
}

module.exports = {
  postPsbSetting,
  getAllPsbSetting,
  getPsbSetting,
  putPsbSetting,
  deletePsbSetting,
  deletePsbSettingBatch,
}
