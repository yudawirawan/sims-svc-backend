const App = require('./../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'PsbSetting')

const findById = async(id) => {
  return await App.db.PsbSetting.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'exams' },
    { association: 'process', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] },
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createPsbSetting = async(payload) => {
  const transaction = await App.db.sequelize.transaction()
  try {
    const res = await App.db.PsbSetting.create(payload, { transaction })
    const exams = []
    for (const exam of payload.exams) {
      exam.settingId = res.id
      const ex = await App.db.PsbSettingExams.create(exam, { transaction })
      exams.push(ex)
    }
    res.exams = exams
    await transaction.commit()
    return res
  } catch (err) {
    console.log(err)
    await transaction.rollback()
    return {
      error: err.message,
    }
  }
}

const updatePsbSetting = async(id, payload) => {
  const transaction = await App.db.sequelize.transaction()
  try {
    const res = await App.db.PsbSetting.update(
      payload,
      { where: { id } },
      {
        returning: true,
        plain: true,
        raw: true,
        transaction,
      })
    const exams = []
    console.log(res)
    for (const exam of payload.exams) {
      exam.settingId = id
      if (!exam.isDeleted) {
        const ex = await App.db.PsbSettingExams.upsert(exam, {
          transaction,
          plain: true,
          returning: true,
          raw: true,
        })
        exams.push(ex)
      } else if (exam.isDeleted && exam.id) {
        await App.db.PsbSettingExams.destroy({ where: { id: exam.id } })
      }
    }
    res.exams = exams
    await transaction.commit()
    return res
  } catch (err) {
    console.log(err)
    await transaction.rollback()
    return {
      error: err.message,
    }
  }
}

const destroyPsbSetting = async(id) => {
  try {
    await App.db.PsbSetting.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createPsbSetting,
  updatePsbSetting,
  destroyPsbSetting,
}
