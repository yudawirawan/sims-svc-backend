const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'LocationCities')

const findById = async(id) => {
  return await App.db.LocationCities.findOne({ where: { id } })
}

const findPaging = async (query) => {
  query.include = [
    { association: 'province', attributes: ['name'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createLocationCities = async(payload) => {
  try {
    const res = await App.db.LocationCities.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateLocationCities = async(id, payload) => {
  try {
    const res = await App.db.LocationCities.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyLocationCities = async(id) => {
  try {
    await App.db.LocationCities.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createLocationCities,
  updateLocationCities,
  destroyLocationCities,
}
