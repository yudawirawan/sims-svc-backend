const { postLocationCities, putLocationCities, deleteLocationCities, deleteLocationCitiesBatch, getAllLocationCities, getLocationCities } = require('./handler')
const {
  postLocationCitiesSchema, getAllLocationCitiesSchema, getLocationCitiesSchema, putLocationCitiesSchema, deleteLocationCitiesSchema, deleteLocationCitiesBatchSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postLocationCitiesSchema }, postLocationCities)
  app.get('/', { schema: getAllLocationCitiesSchema }, getAllLocationCities)
  app.get('/:id', { schema: getLocationCitiesSchema }, getLocationCities)
  app.put('/:id', { schema: putLocationCitiesSchema }, putLocationCities)
  app.delete('/:id', { schema: deleteLocationCitiesSchema }, deleteLocationCities)
  app.post('/delete-batch', { schema: deleteLocationCitiesBatchSchema }, deleteLocationCitiesBatch)
}
