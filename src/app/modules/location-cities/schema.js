const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postLocationCitiesSchema = {
  description: 'Create Local Cities data',
  tags: ['local-cities'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      code: { type: 'string' },
      province_code: { type: 'string' },
      name: { type: 'string' },
      latitude: { type: 'string' },
      longitude: { type: 'string' },
    },
    required: [
      'code',
      'province_code',
      'name',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            code: { type: 'string' },
            province_code: { type: 'number' },
            name: { type: 'string' },
            latitude: { type: 'string' },
            longitude: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllLocationCitiesSchema = {
  description: 'List all Local Cities data',
  tags: ['local-cities'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              code: { type: 'string' },
              province_code: { type: 'number' },
              name: { type: 'string' },
              latitude: { type: 'string' },
              longitude: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getLocationCitiesSchema = {
  description: 'Get Local Cities data by id',
  tags: ['local-cities'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        code: { type: 'string' },
        province_code: { type: 'number' },
        name: { type: 'string' },
        latitude: { type: 'string' },
        longitude: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putLocationCitiesSchema = {
  description: 'Update Local Cities data',
  summary: 'Update data',
  tags: ['local-cities'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      code: { type: 'string' },
      province_code: { type: 'string' },
      name: { type: 'string' },
      latitude: { type: 'string' },
      longitude: { type: 'string' },
    },
    required: [
      'code',
      'province_code',
      'name',
    ],
  },
  params: idParamsSchema,
}

const deleteLocationCitiesSchema = {
  description: 'Delete Local Cities data',
  summary: 'Delete data',
  tags: ['local-cities'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

const deleteLocationCitiesBatchSchema = {
  description: 'Delete batch cities',
  summary: 'Delete cities batch',
  tags: ['local-cities'],
  body: {
    type: 'object',
    properties: {
      ids: { type: 'array' },
    },
    required: ['ids'],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        username: { type: 'string' },
      }
    },
    ...err401,
  },
}

module.exports = {
  postLocationCitiesSchema,
  getAllLocationCitiesSchema,
  getLocationCitiesSchema,
  putLocationCitiesSchema,
  deleteLocationCitiesSchema,
  deleteLocationCitiesBatchSchema,
}
