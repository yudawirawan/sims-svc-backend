const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createLocationCities, updateLocationCities, destroyLocationCities,
} = require('./service')

const postLocationCities = async(req, reply) => {
  const res = await createLocationCities(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllLocationCities = async (req, reply) => {
  const candidate = await findPaging(req.query)
  return ok(reply, candidate, 'fetch data success')
}

const getLocationCities = async(req, reply) => {
  const id = req.params.id
  const LocationCities = await findById(id)
  if (!LocationCities) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, LocationCities, 'fetch data success')
}

const putLocationCities = async(req, reply) => {
  const id = req.params.id
  const LocationCities = await findById(id)
  if (!LocationCities) {
    return notFound(reply, 'data not found')
  }
  const res = await updateLocationCities(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteLocationCities = async(req, reply) => {
  const id = req.params.id
  const LocationCities = await findById(id)
  if (!LocationCities) {
    return notFound(reply, 'data not found')
  }
  const res = destroyLocationCities(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deleteLocationCitiesBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyLocationCities(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete provincies success')
}

module.exports = {
  postLocationCities,
  getAllLocationCities,
  getLocationCities,
  putLocationCities,
  deleteLocationCities,
  deleteLocationCitiesBatch,
}
