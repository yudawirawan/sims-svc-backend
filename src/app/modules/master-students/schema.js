const { err401, err400, idParamsSchema, queryPaging, resp200 } = require('../../utils/http_schema')

const postMasterStudentSchema = {
  description: 'Create student data',
  tags: ['master-students'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      nama: { type: 'string' },
      nipd: { type: 'string' },
      jk: { type: 'string' },
      nisn: { type: 'string' },
      tempatLahir: { type: 'string' },
      tanggalLahir: { type: 'string' },
      nik: { type: 'string' },
      agama: { type: 'string' },
      alamat: { type: 'string' },
      rt: { type: 'string' },
      rw: { type: 'string' },
      dusun: { type: 'string' },
      kelurahan: { type: 'string' },
      kecamatan: { type: 'string' },
      kodepos: { type: 'string' },
      jenisTinggal: { type: 'string' },
      alatTransportasi: { type: 'string' },
      telepon: { type: 'string' },
      hp: { type: 'string' },
      email: { type: 'string' },
      skhun: { type: 'string' },
      penerimaKps: { type: 'string' },
      noKps: { type: 'string' },
      namaAyah: { type: 'string' },
      tahunLahirAyah: { type: 'string' },
      jenjangPendidikanAyah: { type: 'string' },
      pekerjaanAyah: { type: 'string' },
      penghasilanAyah: { type: 'string' },
      nikAyah: { type: 'string' },
      namaIbu: { type: 'string' },
      tahunLahirIbu: { type: 'string' },
      jenjangPendidikanIbu: { type: 'string' },
      pekerjaanIbu: { type: 'string' },
      penghasilanIbu: { type: 'string' },
      nikIbu: { type: 'string' },
      namaWali: { type: 'string' },
      tahunLahirWali: { type: 'string' },
      jenjangPendidikanWali: { type: 'string' },
      pekerjaanWali: { type: 'string' },
      penghasilanWali: { type: 'string' },
      nikWali: { type: 'string' },
      rombelSaatIni: { type: 'string' },
      noPesertaUjianNasional: { type: 'string' },
      noSeriIjazah: { type: 'string' },
      penerimaKip: { type: 'string' },
      nomorKip: { type: 'string' },
      namaDiKip: { type: 'string' },
      nomorKks: { type: 'string' },
      noRegistrasiAktaLahir: { type: 'string' },
      bank: { type: 'string' },
      nomorRekeningBank: { type: 'string' },
      rekeningAtasNama: { type: 'string' },
      layakPip: { type: 'string' },
      alasanLayakPip: { type: 'string' },
      kebutuhanKhusus: { type: 'string' },
      sekolahAsal: { type: 'string' },
      anakKe: { type: 'string' },
      lintang: { type: 'string' },
      bujur: { type: 'string' },
      noKk: { type: 'string' },
      beratBadan: { type: 'string' },
      tinggiBadan: { type: 'string' },
      lingkarKepala: { type: 'string' },
      jumlahSaudaraKandung: { type: 'string' },
      jarakRumahSekolah: { type: 'string' },
      studentReference: {
        type: 'object',
        properties: {
          departmentId: { type: 'number' },
          levelId: { type: 'number' },
          classId: { type: 'number' },
        }
      },
    },
    required: [
      'nama',
      'jk',
      'nisn',
      'tanggalLahir',
      'nik',
      'agama',
      'kelurahan',
      'kecamatan',
      'kodepos',
      'telepon',
      'hp',
      'skhun',
      'penerimaKps',
      'noSeriIjazah',
      'penerimaKip',
      'layakPip',
      'noKk',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            nama: { type: 'string' },
            nipd: { type: 'string' },
            jk: { type: 'string' },
            nisn: { type: 'string' },
            tempatLahir: { type: 'string' },
            tanggalLahir: { type: 'string' },
            nik: { type: 'string' },
            agama: { type: 'string' },
            alamat: { type: 'string' },
            rt: { type: 'string' },
            rw: { type: 'string' },
            dusun: { type: 'string' },
            kelurahan: { type: 'string' },
            kecamatan: { type: 'string' },
            kodepos: { type: 'string' },
            jenisTinggal: { type: 'string' },
            alatTransportasi: { type: 'string' },
            telepon: { type: 'string' },
            hp: { type: 'string' },
            email: { type: 'string' },
            skhun: { type: 'string' },
            penerimaKps: { type: 'string' },
            noKps: { type: 'string' },
            namaAyah: { type: 'string' },
            tahunLahirAyah: { type: 'string' },
            jenjangPendidikanAyah: { type: 'string' },
            pekerjaanAyah: { type: 'string' },
            penghasilanAyah: { type: 'string' },
            nikAyah: { type: 'string' },
            namaIbu: { type: 'string' },
            tahunLahirIbu: { type: 'string' },
            jenjangPendidikanIbu: { type: 'string' },
            pekerjaanIbu: { type: 'string' },
            penghasilanIbu: { type: 'string' },
            nikIbu: { type: 'string' },
            namaWali: { type: 'string' },
            tahunLahirWali: { type: 'string' },
            jenjangPendidikanWali: { type: 'string' },
            pekerjaanWali: { type: 'string' },
            penghasilanWali: { type: 'string' },
            nikWali: { type: 'string' },
            rombelSaatIni: { type: 'string' },
            noPesertaUjianNasional: { type: 'string' },
            noSeriIjazah: { type: 'string' },
            penerimaKip: { type: 'string' },
            nomorKip: { type: 'string' },
            namaDiKip: { type: 'string' },
            nomorKks: { type: 'string' },
            noRegistrasiAktaLahir: { type: 'string' },
            bank: { type: 'string' },
            nomorRekeningBank: { type: 'string' },
            rekeningAtasNama: { type: 'string' },
            layakPip: { type: 'string' },
            alasanLayakPip: { type: 'string' },
            kebutuhanKhusus: { type: 'string' },
            sekolahAsal: { type: 'string' },
            anakKe: { type: 'string' },
            lintang: { type: 'string' },
            bujur: { type: 'string' },
            noKk: { type: 'string' },
            beratBadan: { type: 'string' },
            tinggiBadan: { type: 'string' },
            lingkarKepala: { type: 'string' },
            jumlahSaudaraKandung: { type: 'string' },
            jarakRumahSekolah: { type: 'string' },
            studentReference: {
              type: 'object',
              properties: {
                departmentId: { type: 'number' },
                levelId: { type: 'number' },
                classId: { type: 'number' },
              }
            },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMasterStudentSchema = {
  description: 'List all student data',
  tags: ['master-students'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              nama: { type: 'string' },
              nipd: { type: 'string' },
              jk: { type: 'string' },
              nisn: { type: 'string' },
              tempatLahir: { type: 'string' },
              tanggalLahir: { type: 'string' },
              nik: { type: 'string' },
              agama: { type: 'string' },
              alamat: { type: 'string' },
              rt: { type: 'string' },
              rw: { type: 'string' },
              dusun: { type: 'string' },
              kelurahan: { type: 'string' },
              kecamatan: { type: 'string' },
              kodepos: { type: 'string' },
              jenisTinggal: { type: 'string' },
              alatTransportasi: { type: 'string' },
              telepon: { type: 'string' },
              hp: { type: 'string' },
              email: { type: 'string' },
              skhun: { type: 'string' },
              penerimaKps: { type: 'string' },
              noKps: { type: 'string' },
              namaAyah: { type: 'string' },
              tahunLahirAyah: { type: 'string' },
              jenjangPendidikanAyah: { type: 'string' },
              pekerjaanAyah: { type: 'string' },
              penghasilanAyah: { type: 'string' },
              nikAyah: { type: 'string' },
              namaIbu: { type: 'string' },
              tahunLahirIbu: { type: 'string' },
              jenjangPendidikanIbu: { type: 'string' },
              pekerjaanIbu: { type: 'string' },
              penghasilanIbu: { type: 'string' },
              nikIbu: { type: 'string' },
              namaWali: { type: 'string' },
              tahunLahirWali: { type: 'string' },
              jenjangPendidikanWali: { type: 'string' },
              pekerjaanWali: { type: 'string' },
              penghasilanWali: { type: 'string' },
              nikWali: { type: 'string' },
              rombelSaatIni: { type: 'string' },
              noPesertaUjianNasional: { type: 'string' },
              noSeriIjazah: { type: 'string' },
              penerimaKip: { type: 'string' },
              nomorKip: { type: 'string' },
              namaDiKip: { type: 'string' },
              nomorKks: { type: 'string' },
              noRegistrasiAktaLahir: { type: 'string' },
              bank: { type: 'string' },
              nomorRekeningBank: { type: 'string' },
              rekeningAtasNama: { type: 'string' },
              layakPip: { type: 'string' },
              alasanLayakPip: { type: 'string' },
              kebutuhanKhusus: { type: 'string' },
              sekolahAsal: { type: 'string' },
              anakKe: { type: 'string' },
              lintang: { type: 'string' },
              bujur: { type: 'string' },
              noKk: { type: 'string' },
              beratBadan: { type: 'string' },
              tinggiBadan: { type: 'string' },
              lingkarKepala: { type: 'string' },
              jumlahSaudaraKandung: { type: 'string' },
              jarakRumahSekolah: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
              studentReference: {
                type: 'object',
                items: {
                  type: 'object',
                  properties: {
                    id: { type: 'number' },
                    departmentId: { type: 'number' },
                    levelId: { type: 'number' },
                    classId: { type: 'number' },
                  }
                }
              },
            },
          }
        }
      },
    },
  },
}

const getMasterStudentSchema = {
  description: 'Get student data by id',
  tags: ['master-students'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        nama: { type: 'string' },
        nipd: { type: 'string' },
        jk: { type: 'string' },
        nisn: { type: 'string' },
        tempatLahir: { type: 'string' },
        tanggalLahir: { type: 'string' },
        nik: { type: 'string' },
        agama: { type: 'string' },
        alamat: { type: 'string' },
        rt: { type: 'string' },
        rw: { type: 'string' },
        dusun: { type: 'string' },
        kelurahan: { type: 'string' },
        kecamatan: { type: 'string' },
        kodepos: { type: 'string' },
        jenisTinggal: { type: 'string' },
        alatTransportasi: { type: 'string' },
        telepon: { type: 'string' },
        hp: { type: 'string' },
        email: { type: 'string' },
        skhun: { type: 'string' },
        penerimaKps: { type: 'string' },
        noKps: { type: 'string' },
        namaAyah: { type: 'string' },
        tahunLahirAyah: { type: 'string' },
        jenjangPendidikanAyah: { type: 'string' },
        pekerjaanAyah: { type: 'string' },
        penghasilanAyah: { type: 'string' },
        nikAyah: { type: 'string' },
        namaIbu: { type: 'string' },
        tahunLahirIbu: { type: 'string' },
        jenjangPendidikanIbu: { type: 'string' },
        pekerjaanIbu: { type: 'string' },
        penghasilanIbu: { type: 'string' },
        nikIbu: { type: 'string' },
        namaWali: { type: 'string' },
        tahunLahirWali: { type: 'string' },
        jenjangPendidikanWali: { type: 'string' },
        pekerjaanWali: { type: 'string' },
        penghasilanWali: { type: 'string' },
        nikWali: { type: 'string' },
        rombelSaatIni: { type: 'string' },
        noPesertaUjianNasional: { type: 'string' },
        noSeriIjazah: { type: 'string' },
        penerimaKip: { type: 'string' },
        nomorKip: { type: 'string' },
        namaDiKip: { type: 'string' },
        nomorKks: { type: 'string' },
        noRegistrasiAktaLahir: { type: 'string' },
        bank: { type: 'string' },
        nomorRekeningBank: { type: 'string' },
        rekeningAtasNama: { type: 'string' },
        layakPip: { type: 'string' },
        alasanLayakPip: { type: 'string' },
        kebutuhanKhusus: { type: 'string' },
        sekolahAsal: { type: 'string' },
        anakKe: { type: 'string' },
        lintang: { type: 'string' },
        bujur: { type: 'string' },
        noKk: { type: 'string' },
        beratBadan: { type: 'string' },
        tinggiBadan: { type: 'string' },
        lingkarKepala: { type: 'string' },
        jumlahSaudaraKandung: { type: 'string' },
        jarakRumahSekolah: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMasterStudentSchema = {
  description: 'Update student data',
  summary: 'Update data',
  tags: ['master-students'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      nama: { type: 'string' },
      nipd: { type: 'string' },
      jk: { type: 'string' },
      nisn: { type: 'string' },
      tempatLahir: { type: 'string' },
      tanggalLahir: { type: 'string' },
      nik: { type: 'string' },
      agama: { type: 'string' },
      alamat: { type: 'string' },
      rt: { type: 'string' },
      rw: { type: 'string' },
      dusun: { type: 'string' },
      kelurahan: { type: 'string' },
      kecamatan: { type: 'string' },
      kodepos: { type: 'string' },
      jenisTinggal: { type: 'string' },
      alatTransportasi: { type: 'string' },
      telepon: { type: 'string' },
      hp: { type: 'string' },
      email: { type: 'string' },
      skhun: { type: 'string' },
      penerimaKps: { type: 'string' },
      noKps: { type: 'string' },
      namaAyah: { type: 'string' },
      tahunLahirAyah: { type: 'string' },
      jenjangPendidikanAyah: { type: 'string' },
      pekerjaanAyah: { type: 'string' },
      penghasilanAyah: { type: 'string' },
      nikAyah: { type: 'string' },
      namaIbu: { type: 'string' },
      tahunLahirIbu: { type: 'string' },
      jenjangPendidikanIbu: { type: 'string' },
      pekerjaanIbu: { type: 'string' },
      penghasilanIbu: { type: 'string' },
      nikIbu: { type: 'string' },
      namaWali: { type: 'string' },
      tahunLahirWali: { type: 'string' },
      jenjangPendidikanWali: { type: 'string' },
      pekerjaanWali: { type: 'string' },
      penghasilanWali: { type: 'string' },
      nikWali: { type: 'string' },
      rombelSaatIni: { type: 'string' },
      noPesertaUjianNasional: { type: 'string' },
      noSeriIjazah: { type: 'string' },
      penerimaKip: { type: 'string' },
      nomorKip: { type: 'string' },
      namaDiKip: { type: 'string' },
      nomorKks: { type: 'string' },
      noRegistrasiAktaLahir: { type: 'string' },
      bank: { type: 'string' },
      nomorRekeningBank: { type: 'string' },
      rekeningAtasNama: { type: 'string' },
      layakPip: { type: 'string' },
      alasanLayakPip: { type: 'string' },
      kebutuhanKhusus: { type: 'string' },
      sekolahAsal: { type: 'string' },
      anakKe: { type: 'string' },
      lintang: { type: 'string' },
      bujur: { type: 'string' },
      noKk: { type: 'string' },
      beratBadan: { type: 'string' },
      tinggiBadan: { type: 'string' },
      lingkarKepala: { type: 'string' },
      jumlahSaudaraKandung: { type: 'string' },
      jarakRumahSekolah: { type: 'string' },
      studentReference: {
        type: 'object',
        properties: {
          id: { type: 'number' },
          departmentId: { type: 'number' },
          levelId: { type: 'number' },
          classId: { type: 'number' },
        }
      },
    },
    required: [
      'nama',
      'jk',
      'nisn',
      'tanggalLahir',
      'nik',
      'agama',
      'kelurahan',
      'kecamatan',
      'kodepos',
      'telepon',
      'hp',
      'skhun',
      'penerimaKps',
      'noSeriIjazah',
      'penerimaKip',
      'layakPip',
      'noKk',
    ],
  },
  params: idParamsSchema,
}

const deleteMasterStudentSchema = {
  description: 'Delete batch psb process',
  summary: 'Delete psb process batch',
  tags: ['master-students'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}



module.exports = {
  postMasterStudentSchema,
  getAllMasterStudentSchema,
  getMasterStudentSchema,
  putMasterStudentSchema,
  deleteMasterStudentSchema,
}
