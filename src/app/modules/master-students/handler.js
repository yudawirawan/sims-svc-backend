const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMasterStudent, updateMasterStudent, destroyMasterStudent,
} = require('./service')

const postMasterStudent = async(req, reply) => {
  const res = await createMasterStudent(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMasterStudent = async(req, reply) => {
  const candidate = await findPaging(req.query)
  return ok(reply, candidate, 'fetch data success')
}

const getMasterStudent = async(req, reply) => {
  const id = req.params.id
  const MasterStudent = await findById(id)
  if (!MasterStudent) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, MasterStudent, 'fetch data success')
}

const putMasterStudent = async(req, reply) => {
  const id = req.params.id
  const MasterStudent = await findById(id)
  if (!MasterStudent) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMasterStudent(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMasterStudent = async(req, reply) => {
  const id = req.params.id
  const MasterStudent = await findById(id)
  if (!MasterStudent) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMasterStudent(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deleteMasterStudentBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyMasterStudent(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete users success')
}

module.exports = {
  postMasterStudent,
  getAllMasterStudent,
  getMasterStudent,
  putMasterStudent,
  deleteMasterStudent,
  deleteMasterStudentBatch,
}
