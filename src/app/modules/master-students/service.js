const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MasterStudent')

const findById = async(id) => {
  return await App.db.MasterStudent.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'studentReference',
      include: 
        [
          { association: 'department', attributes: ['name'] },
          { association: 'level', attributes: ['level'] },
          { association: 'class', attributes: ['name'] }, 
        ]
    },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMasterStudent = async(payload) => {
  const transaction = await App.db.sequelize.transaction()
  try {
    const res = await App.db.MasterStudent.create(payload, { transaction })
    payload.studentReference.studentId = res.id
    const reg = await genRegisterId('master_student_references')
    payload.studentReference.registerId = reg.genRegId
    const studentReference = await App.db.MasterStudentReference.create(payload.studentReference, { transaction })
    res.studentReference = studentReference
    await transaction.commit()
    return res
  } catch (err) {
    await transaction.rollback()
    return {
      error: err.message,
    }
  }
}

const updateMasterStudent = async(id, payload) => {
  const transaction = await App.db.sequelize.transaction()
  try {
    const res = await App.db.MasterStudent.update(
      payload,
      { where: { id } },
      {
        returning: true,
        plain: true,
        raw: true,
        transaction,
      })
      if (!payload.studentReference.isDeleted) {
        payload.studentReference.studentId = id
        await App.db.MasterStudentReference.upsert(payload.studentReference, {
          transaction,
          plain: true,
          returning: true,
          raw: true,
        })
      } else if (studentReference.isDeleted && studentReference.id) {
        await App.db.MasterStudentReference.destroy({ where: { id: studentReference.id } })
      }
    res.studentReference = payload.studentReference
    await transaction.commit()
    return res
  } catch (err) {
    console.log(err)
    await transaction.rollback()
    return {
      error: err.message,
    }
  }
}

const destroyMasterStudent = async(id) => {
  try {
    await App.db.MasterStudent.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const genRegisterId = async(tableName) => {
  try {
    if (tableName===undefined){
      tableName='psb_candidates'
    }
    const moment = require('moment-timezone')
    const period = moment().tz('Asia/Jakarta').format('YYYY')
    const [[result]] = await db.sequelize
      .query(`SELECT registerId FROM ${tableName} WHERE registerId LIKE '${period}-%' ORDER BY registerId DESC LIMIT 1`)
    let genRegId = ''
    if (result && result.registerId) {
      const regIdNum = result.registerId.substring(5, result.registerId.length)
      const regId = Number(regIdNum) + 1
      genRegId = period + '-' + String(regId).padStart(4, '0')
    } else {
      genRegId = moment().tz('Asia/Jakarta').format('YYYY') + '-0001'
    }
    return {
      genRegId
    }
  } catch (err) {
    console.log(err)
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMasterStudent,
  updateMasterStudent,
  destroyMasterStudent,
  genRegisterId,
}
