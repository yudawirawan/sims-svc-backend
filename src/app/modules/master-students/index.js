const { postMasterStudent, putMasterStudent, deleteMasterStudent, getAllMasterStudent, getMasterStudent } = require('./handler')
const {
  postMasterStudentSchema, getAllMasterStudentSchema, getMasterStudentSchema, putMasterStudentSchema, deleteMasterStudentSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMasterStudentSchema }, postMasterStudent)
  app.get('/', { schema: getAllMasterStudentSchema }, getAllMasterStudent)
  app.get('/:id', { schema: getMasterStudentSchema }, getMasterStudent)
  app.put('/:id', { schema: putMasterStudentSchema }, putMasterStudent)
  app.delete('/:id', { schema: deleteMasterStudentSchema }, deleteMasterStudent)
}
