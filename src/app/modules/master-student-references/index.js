const { postMasterStudentReference, putMasterStudentReference, deleteMasterStudentReference, getAllMasterStudentReference, getMasterStudentReference } = require('./handler')
const {
  postMasterStudentReferenceSchema, getAllMasterStudentReferenceSchema, getMasterStudentReferenceSchema, putMasterStudentReferenceSchema, deleteMasterStudentReferenceSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMasterStudentReferenceSchema }, postMasterStudentReference)
  app.get('/', { schema: getAllMasterStudentReferenceSchema }, getAllMasterStudentReference)
  app.get('/:id', { schema: getMasterStudentReferenceSchema }, getMasterStudentReference)
  app.put('/:id', { schema: putMasterStudentReferenceSchema }, putMasterStudentReference)
  app.delete('/:id', { schema: deleteMasterStudentReferenceSchema }, deleteMasterStudentReference)
}
