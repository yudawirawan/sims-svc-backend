const { err401, err400, idParamsSchema, queryPaging, resp200 } = require('../../utils/http_schema')

const postMasterStudentReferenceSchema = {
  description: 'Create student data',
  tags: ['master-student-references'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      registerId: { type: 'string' },
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      classId: { type: 'number' },
      admissionProcessId: { type: 'number' },
      candidateGroupId: { type: 'number' },
      studentId: { type: 'number' },
      iuranWajib: { type: 'number' },
      iuranSukarela: { type: 'number' },
      status: { type: 'number' },
      graduated: { type: 'number' },
      graduatedYear: { type: 'string' },
    },
    required: [
      'departmentId',
      'levelId',
      'classId',
      'studentId',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            registerId: { type: 'string' },
            departmentId: { type: 'number' },
            levelId: { type: 'number' },
            classId: { type: 'number' },
            admissionProcessId: { type: 'number' },
            candidateGroupId: { type: 'number' },
            studentId: { type: 'number' },
            iuranWajib: { type: 'number' },
            iuranSukarela: { type: 'number' },
            status: { type: 'number' },
            graduated: { type: 'number' },
            graduatedYear: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMasterStudentReferenceSchema = {
  description: 'List all student data',
  tags: ['master-student-references'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              registerId: { type: 'string' },
              departmentId: { type: 'number' },
              levelId: { type: 'number' },
              classId: { type: 'number' },
              admissionProcessId: { type: 'number' },
              candidateGroupId: { type: 'number' },
              studentId: { type: 'number' },
              iuranWajib: { type: 'number' },
              iuranSukarela: { type: 'number' },
              status: { type: 'number' },
              graduated: { type: 'number' },
              graduatedYear: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMasterStudentReferenceSchema = {
  description: 'Get student data by id',
  tags: ['master-student-references'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        registerId: { type: 'string' },
        departmentId: { type: 'number' },
        levelId: { type: 'number' },
        classId: { type: 'number' },
        admissionProcessId: { type: 'number' },
        candidateGroupId: { type: 'number' },
        studentId: { type: 'number' },
        iuranWajib: { type: 'number' },
        iuranSukarela: { type: 'number' },
        status: { type: 'number' },
        graduated: { type: 'number' },
        graduatedYear: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMasterStudentReferenceSchema = {
  description: 'Update student data',
  summary: 'Update data',
  tags: ['master-student-references'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      registerId: { type: 'string' },
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      classId: { type: 'number' },
      admissionProcessId: { type: 'number' },
      candidateGroupId: { type: 'number' },
      studentId: { type: 'number' },
      iuranWajib: { type: 'number' },
      iuranSukarela: { type: 'number' },
      status: { type: 'number' },
      graduated: { type: 'number' },
      graduatedYear: { type: 'string' },
    },
    required: [
      'departmentId',
      'levelId',
      'classId',
      'studentId',
    ],
  },
  params: idParamsSchema,
}

const deleteMasterStudentReferenceSchema = {
  description: 'Delete batch psb process',
  summary: 'Delete psb process batch',
  tags: ['master-student-references'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMasterStudentReferenceSchema,
  getAllMasterStudentReferenceSchema,
  getMasterStudentReferenceSchema,
  putMasterStudentReferenceSchema,
  deleteMasterStudentReferenceSchema,
}
