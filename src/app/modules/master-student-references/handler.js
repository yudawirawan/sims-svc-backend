const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMasterStudentReference, updateMasterStudentReference, destroyMasterStudentReference,
} = require('./service')

const postMasterStudentReference = async(req, reply) => {
  const result = await createMasterStudentReference(req.body)
  if (result.error) {
    return badRequest(reply, result.error)
  }
  return ok(reply, result, 'Create data success')
}

const getAllMasterStudentReference = async(req, reply) => {
  const result = await findPaging(req.query)
  return ok(reply, result, 'fetch data success')
}

const getMasterStudentReference = async(req, reply) => {
  const id = req.params.id
  const result = await findById(id)
  if (!result) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, result, 'fetch data success')
}

const putMasterStudentReference = async(req, reply) => {
  const id = req.params.id
  const result = await findById(id)
  if (!result) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMasterStudentReference(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMasterStudentReference = async(req, reply) => {
  const id = req.params.id
  const result = await findById(id)
  if (!result) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMasterStudentReference(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMasterStudentReference,
  getAllMasterStudentReference,
  getMasterStudentReference,
  putMasterStudentReference,
  deleteMasterStudentReference,
}
