const { postPsbCandidateGroup, putPsbCandidateGroup, deletePsbCandidateGroup, deletePsbCandidateGroupBatch, getAllPsbCandidateGroup, getPsbCandidateGroup } = require('./handler')
const {
  postPsbCandidateGroupSchema, getAllPsbCandidateGroupSchema, getPsbCandidateGroupSchema, putPsbCandidateGroupSchema, deletePsbCandidateGroupSchema, deletePsbCandidateGroupBatchSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postPsbCandidateGroupSchema }, postPsbCandidateGroup)
  app.get('/', { schema: getAllPsbCandidateGroupSchema }, getAllPsbCandidateGroup)
  app.get('/:id', { schema: getPsbCandidateGroupSchema }, getPsbCandidateGroup)
  app.put('/:id', { schema: putPsbCandidateGroupSchema }, putPsbCandidateGroup)
  app.delete('/:id', { schema: deletePsbCandidateGroupSchema }, deletePsbCandidateGroup)
  app.post('/delete-batch', { schema: deletePsbCandidateGroupBatchSchema }, deletePsbCandidateGroupBatch)
}
