const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createPsbCandidateGroup, updatePsbCandidateGroup, destroyPsbCandidateGroup,
} = require('./service')

const postPsbCandidateGroup = async(req, reply) => {
  const res = await createPsbCandidateGroup(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllPsbCandidateGroup = async(req, reply) => {
  const candidate = await findPaging(req.query)
  return ok(reply, candidate, 'fetch data success')
}

const getPsbCandidateGroup = async(req, reply) => {
  const id = req.params.id
  const PsbCandidateGroup = await findById(id)
  if (!PsbCandidateGroup) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, PsbCandidateGroup, 'fetch data success')
}

const putPsbCandidateGroup = async(req, reply) => {
  const id = req.params.id
  const PsbCandidateGroup = await findById(id)
  if (!PsbCandidateGroup) {
    return notFound(reply, 'data not found')
  }
  const res = await updatePsbCandidateGroup(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deletePsbCandidateGroup = async(req, reply) => {
  const id = req.params.id
  const PsbCandidateGroup = await findById(id)
  if (!PsbCandidateGroup) {
    return notFound(reply, 'data not found')
  }
  const res = destroyPsbCandidateGroup(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deletePsbCandidateGroupBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyPsbCandidateGroup(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete users success')
}

module.exports = {
  postPsbCandidateGroup,
  getAllPsbCandidateGroup,
  getPsbCandidateGroup,
  putPsbCandidateGroup,
  deletePsbCandidateGroup,
  deletePsbCandidateGroupBatch,
}
