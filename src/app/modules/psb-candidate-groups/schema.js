const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postPsbCandidateGroupSchema = {
  description: 'Create PSB Process data',
  tags: ['psb-candidate-groups'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      processId: { type: 'number' },
      groupName: { type: 'string' },
      capacity: { type: 'number' },
      description: { type: 'string' },
      status: { type: 'number' },
      startDate: { type: 'string' },
      endDate: { type: 'string' },
    },
    required: [
      'departmentId',
      'processId',
      'groupName',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            processId: { type: 'number' },
            groupName: { type: 'string' },
            capacity: { type: 'number' },
            description: { type: 'string' },
            status: { type: 'number' },
            startDate: { type: 'string' },
            endDate: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllPsbCandidateGroupSchema = {
  description: 'List all Psb Process data',
  tags: ['psb-candidate-groups'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              processId: { type: 'number' },
              groupName: { type: 'string' },
              capacity: { type: 'number' },
              description: { type: 'string' },
              status: { type: 'number' },
              startDate: { type: 'string' },
              endDate: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getPsbCandidateGroupSchema = {
  description: 'Get Psb Process data by id',
  tags: ['psb-candidate-groups'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        processId: { type: 'number' },
        groupName: { type: 'string' },
        capacity: { type: 'number' },
        description: { type: 'string' },
        status: { type: 'number' },
        startDate: { type: 'string' },
        endDate: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putPsbCandidateGroupSchema = {
  description: 'Update Psb Process data',
  summary: 'Update data',
  tags: ['psb-candidate-groups'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      processId: { type: 'number' },
      groupName: { type: 'string' },
      capacity: { type: 'number' },
      description: { type: 'string' },
      status: { type: 'number' },
      startDate: { type: 'string' },
      endDate: { type: 'string' },
    },
    required: [
      'departmentId',
      'processId',
      'groupName',
    ],
  },
  params: idParamsSchema,
}

const deletePsbCandidateGroupSchema = {
  description: 'Delete Psb Process data',
  summary: 'Delete data',
  tags: ['psb-candidate-groups'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

const deletePsbCandidateGroupBatchSchema = {
  description: 'Delete batch psb process',
  summary: 'Delete psb process batch',
  tags: ['psb-candidate-groups'],
  body: {
    type: 'object',
    properties: {
      ids: { type: 'array' },
    },
    required: ['ids'],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        username: { type: 'string' },
      }
    },
    ...err401,
  },
}

module.exports = {
  postPsbCandidateGroupSchema,
  getAllPsbCandidateGroupSchema,
  getPsbCandidateGroupSchema,
  putPsbCandidateGroupSchema,
  deletePsbCandidateGroupSchema,
  deletePsbCandidateGroupBatchSchema,
}
