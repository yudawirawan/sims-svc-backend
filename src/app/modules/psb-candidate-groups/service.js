const App = require('./../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'PsbCandidateGroup')

const findById = async(id) => {
  return await App.db.PsbCandidateGroup.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'psbProcess', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] },
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createPsbCandidateGroup = async(payload) => {
  try {
    const res = await App.db.PsbCandidateGroup.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updatePsbCandidateGroup = async(id, payload) => {
  try {
    const res = await App.db.PsbCandidateGroup.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyPsbCandidateGroup = async(id) => {
  try {
    await App.db.PsbCandidateGroup.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createPsbCandidateGroup,
  updatePsbCandidateGroup,
  destroyPsbCandidateGroup,
}
