const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MasterLevel')

const findById = async(id) => {
  return await App.db.MasterLevel.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMasterLevel = async(payload) => {
  try {
    const res = await App.db.MasterLevel.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateMasterLevel = async(id, payload) => {
  try {
    const res = await App.db.MasterLevel.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyMasterLevel = async(id) => {
  try {
    await App.db.MasterLevel.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMasterLevel,
  updateMasterLevel,
  destroyMasterLevel,
}
