const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMasterLevelSchema = {
  description: 'Create level data',
  tags: ['master-level'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      level: { type: 'string' },
      description: { type: 'string' },
      status: { type: 'number' },
    },
    required: [
      'departmentId',
      'level',
      'status',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            level: { type: 'string' },
            description: { type: 'string' },
            status: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMasterLevelSchema = {
  description: 'List all levels data',
  tags: ['master-level'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              level: { type: 'string' },
              description: { type: 'string' },
              status: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMasterLevelSchema = {
  description: 'Get level data by id',
  tags: ['master-level'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        level: { type: 'string' },
        description: { type: 'string' },
        status: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMasterLevelSchema = {
  description: 'Update level data',
  summary: 'Update data',
  tags: ['master-level'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      level: { type: 'string' },
      description: { type: 'string' },
      status: { type: 'number' },
    },
    required: [
      'departmentId',
      'level',
      'description',
      'status',
    ],
  },
  params: idParamsSchema,
}

const deleteMasterLevelSchema = {
  description: 'Delete level data',
  summary: 'Delete data',
  tags: ['master-level'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMasterLevelSchema,
  getAllMasterLevelSchema,
  getMasterLevelSchema,
  putMasterLevelSchema,
  deleteMasterLevelSchema,
}
