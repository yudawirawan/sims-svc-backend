const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMasterLevel, updateMasterLevel, destroyMasterLevel
} = require('./service')

const postMasterLevel = async(req, reply) => {
  const res = await createMasterLevel(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMasterLevel = async(req, reply) => {
  const level = await findPaging(req.query)
  return ok(reply, level, 'fetch data success')
}

const getMasterLevel = async(req, reply) => {
  const id = req.params.id
  const level = await findById(id)
  if (!level) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, level, 'fetch data success')
}

const putMasterLevel = async(req, reply) => {
  const id = req.params.id
  const level = await findById(id)
  if (!level) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMasterLevel(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMasterLevel = async(req, reply) => {
  const id = req.params.id
  const level = await findById(id)
  if (!level) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMasterLevel(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMasterLevel,
  getAllMasterLevel,
  getMasterLevel,
  putMasterLevel,
  deleteMasterLevel,
}
