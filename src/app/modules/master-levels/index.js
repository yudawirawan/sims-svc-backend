const { postMasterLevel, putMasterLevel, deleteMasterLevel, getAllMasterLevel, getMasterLevel } = require('./handler')
const {
  postMasterLevelSchema, getAllMasterLevelSchema, getMasterLevelSchema, putMasterLevelSchema, deleteMasterLevelSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMasterLevelSchema }, postMasterLevel)
  app.get('/', { schema: getAllMasterLevelSchema }, getAllMasterLevel)
  app.get('/:id', { schema: getMasterLevelSchema }, getMasterLevel)
  app.put('/:id', { schema: putMasterLevelSchema }, putMasterLevel)
  app.delete('/:id', { schema: deleteMasterLevelSchema }, deleteMasterLevel)
}
