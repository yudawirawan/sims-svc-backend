const { postMasterDepartment, putMasterDepartment, deleteMasterDepartment, getAllMasterDepartment, getMasterDepartment } = require('./handler')
const {
  postMasterDepartmentSchema, getAllMasterDepartmentSchema, getMasterDepartmentSchema, putMasterDepartmentSchema, deleteMasterDepartmentSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMasterDepartmentSchema }, postMasterDepartment)
  app.get('/', { schema: getAllMasterDepartmentSchema }, getAllMasterDepartment)
  app.get('/:id', { schema: getMasterDepartmentSchema }, getMasterDepartment)
  app.put('/:id', { schema: putMasterDepartmentSchema }, putMasterDepartment)
  app.delete('/:id', { schema: deleteMasterDepartmentSchema }, deleteMasterDepartment)
}
