const App = require('./../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MasterDepartment')

const findById = async(id) => {
  return await App.db.MasterDepartment.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'kepegawaian', attributes: ['nama', 'nik'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] },
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMasterDepartment = async(payload) => {
  try {
    const res = await App.db.MasterDepartment.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateMasterDepartment = async(id, payload) => {
  try {
    const res = await App.db.MasterDepartment.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyMasterDepartment = async(id) => {
  try {
    await App.db.MasterDepartment.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMasterDepartment,
  updateMasterDepartment,
  destroyMasterDepartment,
}
