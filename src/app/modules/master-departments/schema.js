const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMasterDepartmentSchema = {
  description: 'Create department data',
  tags: ['master-departments'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      name: { type: 'string' },
      kepegawaianId: { type: 'number' },
      description: { type: 'string' },
      status: { type: 'number' },
    },
    required: [
      'name',
      'kepegawaianId',
      'status',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            name: { type: 'string' },
            kepegawaianId: { type: 'number' },
            description: { type: 'string' },
            status: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMasterDepartmentSchema = {
  description: 'List all departments data',
  tags: ['master-departments'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              name: { type: 'string' },
              kepegawaianId: { type: 'number' },
              description: { type: 'string' },
              status: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMasterDepartmentSchema = {
  description: 'Get department data by id',
  tags: ['master-departments'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        name: { type: 'string' },
        kepegawaianId: { type: 'number' },
        description: { type: 'string' },
        status: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMasterDepartmentSchema = {
  description: 'Update department data',
  summary: 'Update data',
  tags: ['master-departments'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      name: { type: 'string' },
      kepegawaianId: { type: 'number' },
      description: { type: 'string' },
      status: { type: 'number' },
    },
    required: [
      'name',
      'kepegawaianId',
      'description',
      'status',
    ],
  },
  params: idParamsSchema,
}

const deleteMasterDepartmentSchema = {
  description: 'Delete department data',
  summary: 'Delete data',
  tags: ['master-departments'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMasterDepartmentSchema,
  getAllMasterDepartmentSchema,
  getMasterDepartmentSchema,
  putMasterDepartmentSchema,
  deleteMasterDepartmentSchema,
}
