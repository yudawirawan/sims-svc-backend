const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMasterDepartment, updateMasterDepartment, destroyMasterDepartment
} = require('./service')

const postMasterDepartment = async(req, reply) => {
  const res = await createMasterDepartment(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMasterDepartment = async(req, reply) => {
  const department = await findPaging(req.query)
  return ok(reply, department, 'fetch data success')
}

const getMasterDepartment = async(req, reply) => {
  const id = req.params.id
  const department = await findById(id)
  if (!department) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, department, 'fetch data success')
}

const putMasterDepartment = async(req, reply) => {
  const id = req.params.id
  const department = await findById(id)
  if (!department) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMasterDepartment(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMasterDepartment = async(req, reply) => {
  const id = req.params.id
  const department = await findById(id)
  if (!department) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMasterDepartment(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMasterDepartment,
  getAllMasterDepartment,
  getMasterDepartment,
  putMasterDepartment,
  deleteMasterDepartment,
}
