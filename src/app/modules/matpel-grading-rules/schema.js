const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMatpelGradingRuleSchema = {
  description: 'Create grading rule data',
  tags: ['matpel-grading-rules'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      teacherId: { type: 'number' },
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      assessmentAspectId: { type: 'number' },
      scores: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            initialScore: { type: 'number' },
            finalScore: { type: 'number' },
            grade: { type: 'string' },
          }
        }
      },
    },
    required: [
      'teacherId',
      'departmentId',
      'levelId',
      'assessmentAspectId',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            teacherId: { type: 'number' },
            departmentId: { type: 'number' },
            levelId: { type: 'number' },
            assessmentAspectId: { type: 'number' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMatpelGradingRuleSchema = {
  description: 'List all grading rules data',
  tags: ['matpel-grading-rules'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              teacherId: { type: 'number' },
              departmentId: { type: 'number' },
              levelId: { type: 'number' },
              assessmentAspectId: { type: 'number' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
              scores: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    id: { type: 'number' },
                    initialScore: { type: 'number' },
                    finalScore: { type: 'number' },
                    grade: { type: 'string' },
                  }
                }
              },
            },
          }
        }
      },
    },
  },
}

const getMatpelGradingRuleSchema = {
  description: 'Get grading rule data by id',
  tags: ['matpel-grading-rules'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        teacherId: { type: 'number' },
        departmentId: { type: 'number' },
        levelId: { type: 'number' },
        assessmentAspectId: { type: 'number' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMatpelGradingRuleSchema = {
  description: 'Update grading rule data',
  summary: 'Update data',
  tags: ['matpel-grading-rules'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      teacherId: { type: 'number' },
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      assessmentAspectId: { type: 'number' },
      scores: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            initialScore: { type: 'number' },
            finalScore: { type: 'number' },
            grade: { type: 'string' },
          }
        }
      },
    },
    required: [
      'teacherId',
      'departmentId',
      'levelId',
      'assessmentAspectId',
    ],
  },
  params: idParamsSchema,
}

const deleteMatpelGradingRuleSchema = {
  description: 'Delete grading rule data',
  summary: 'Delete data',
  tags: ['matpel-grading-rules'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMatpelGradingRuleSchema,
  getAllMatpelGradingRuleSchema,
  getMatpelGradingRuleSchema,
  putMatpelGradingRuleSchema,
  deleteMatpelGradingRuleSchema,
}
