const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MatpelGradingRule')

const findById = async(id) => {
  return await App.db.MatpelGradingRule.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'teacher', attributes: ['kepegawaianId'],
      include: [{association: 'kepegawaian', attributes: ['nama']}]
    },
    { association: 'department', attributes: ['name'] },
    { association: 'level', attributes: ['level'] },
    { association: 'assessmentAspect', attributes: ['name'] },
    { association: 'scores' },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMatpelGradingRule = async(payload) => {
  const transaction = await App.db.sequelize.transaction()
  try {
    const res = await App.db.MatpelGradingRule.create(payload, { transaction })
    const scores = []
    for (const score of payload.scores) {
      score.matpelGradingRuleId = res.id
      const ex = await App.db.MatpelGradingRuleScore.create(score, { transaction })
      scores.push(ex)
    }
    res.scores = scores
    await transaction.commit()
    return res
  } catch (err) {
    console.log(err)
    await transaction.rollback()
    return {
      error: err.message,
    }
  }
}

const updateMatpelGradingRule = async(id, payload) => {
  const transaction = await App.db.sequelize.transaction()
  try {
    const res = await App.db.MatpelGradingRule.update(
      payload,
      { where: { id } },
      {
        returning: true,
        plain: true,
        raw: true,
        transaction,
      })
    const scores = []
    console.log(res)
    for (const score of payload.scores) {
      score.matpelGradingRuleId = id
      if (!score.isDeleted) {
        const ex = await App.db.MatpelGradingRuleScore.upsert(score, {
          transaction,
          plain: true,
          returning: true,
          raw: true,
        })
        scores.push(ex)
      } else if (score.isDeleted && score.id) {
        await App.db.MatpelGradingRuleScore.destroy({ where: { id: score.id } })
      }
    }
    res.scores = scores
    await transaction.commit()
    return res
  } catch (err) {
    console.log(err)
    await transaction.rollback()
    return {
      error: err.message,
    }
  }
}

const destroyMatpelGradingRule = async(id) => {
  try {
    await App.db.MatpelGradingRule.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMatpelGradingRule,
  updateMatpelGradingRule,
  destroyMatpelGradingRule,
}
