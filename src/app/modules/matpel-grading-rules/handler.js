const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMatpelGradingRule, updateMatpelGradingRule, destroyMatpelGradingRule
} = require('./service')

const postMatpelGradingRule = async(req, reply) => {
  const res = await createMatpelGradingRule(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMatpelGradingRule = async(req, reply) => {
  const grading = await findPaging(req.query)
  return ok(reply, grading, 'fetch data success')
}

const getMatpelGradingRule = async(req, reply) => {
  const id = req.params.id
  const grading = await findById(id)
  if (!grading) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, grading, 'fetch data success')
}

const putMatpelGradingRule = async(req, reply) => {
  const id = req.params.id
  const grading = await findById(id)
  if (!grading) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMatpelGradingRule(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMatpelGradingRule = async(req, reply) => {
  const id = req.params.id
  const grading = await findById(id)
  if (!grading) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMatpelGradingRule(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMatpelGradingRule,
  getAllMatpelGradingRule,
  getMatpelGradingRule,
  putMatpelGradingRule,
  deleteMatpelGradingRule,
}
