const { postMatpelGradingRule, putMatpelGradingRule, deleteMatpelGradingRule, getAllMatpelGradingRule, getMatpelGradingRule } = require('./handler')
const {
  postMatpelGradingRuleSchema, getAllMatpelGradingRuleSchema, getMatpelGradingRuleSchema, putMatpelGradingRuleSchema, deleteMatpelGradingRuleSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMatpelGradingRuleSchema }, postMatpelGradingRule)
  app.get('/', { schema: getAllMatpelGradingRuleSchema }, getAllMatpelGradingRule)
  app.get('/:id', { schema: getMatpelGradingRuleSchema }, getMatpelGradingRule)
  app.put('/:id', { schema: putMatpelGradingRuleSchema }, putMatpelGradingRule)
  app.delete('/:id', { schema: deleteMatpelGradingRuleSchema }, deleteMatpelGradingRule)
}
