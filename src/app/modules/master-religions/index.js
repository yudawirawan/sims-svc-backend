const { postMasterReligion, putMasterReligion, deleteMasterReligion, getAllMasterReligion, getMasterReligion } = require('./handler')
const {
  postMasterReligionSchema, getAllMasterReligionSchema, getMasterReligionSchema, putMasterReligionSchema, deleteMasterReligionSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMasterReligionSchema }, postMasterReligion)
  app.get('/', { schema: getAllMasterReligionSchema }, getAllMasterReligion)
  app.get('/:id', { schema: getMasterReligionSchema }, getMasterReligion)
  app.put('/:id', { schema: putMasterReligionSchema }, putMasterReligion)
  app.delete('/:id', { schema: deleteMasterReligionSchema }, deleteMasterReligion)
}
