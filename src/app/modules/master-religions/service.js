const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MasterReligion')

const findById = async(id) => {
  return await App.db.MasterReligion.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] },
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMasterReligion = async(payload) => {
  try {
    const res = await App.db.MasterReligion.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateMasterReligion = async(id, payload) => {
  try {
    const res = await App.db.MasterReligion.update(payload, { where: { id } })
    return res
  } catch (err) {
    console.log(err)
    return {
      error: err.message,
    }
  }
}

const destroyMasterReligion = async(id) => {
  try {
    await App.db.MasterReligion.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMasterReligion,
  updateMasterReligion,
  destroyMasterReligion,
}
