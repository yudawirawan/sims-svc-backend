const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMasterReligionSchema = {
  description: 'Create religion data',
  tags: ['master-religions'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      name: { type: 'string' },
      order: { type: 'string' },
      description: { type: 'string' },
    },
    required: [
      'name',
      'order',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            name: { type: 'string' },
            order: { type: 'string' },
            description: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMasterReligionSchema = {
  description: 'List all generation master data',
  tags: ['master-religions'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              name: { type: 'string' },
              order: { type: 'string' },
              description: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMasterReligionSchema = {
  description: 'Get generation master data by id',
  tags: ['master-religions'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        name: { type: 'string' },
        order: { type: 'string' },
        description: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMasterReligionSchema = {
  description: 'Update User data',
  summary: 'Update data',
  tags: ['master-religions'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      name: { type: 'string' },
      order: { type: 'string' },
      description: { type: 'string' },
    },
    required: [
      'name',
      'order',
    ],
  },
  params: idParamsSchema,
}

const deleteMasterReligionSchema = {
  description: 'Delete generation master data',
  summary: 'Delete data',
  tags: ['master-religions'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMasterReligionSchema,
  getAllMasterReligionSchema,
  getMasterReligionSchema,
  putMasterReligionSchema,
  deleteMasterReligionSchema,
}
