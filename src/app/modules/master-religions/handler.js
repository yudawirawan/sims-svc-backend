const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMasterReligion, updateMasterReligion, destroyMasterReligion
} = require('./service')

const postMasterReligion = async(req, reply) => {
  const res = await createMasterReligion(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMasterReligion = async(req, reply) => {
  const religion = await findPaging(req.query)
  return ok(reply, religion, 'fetch data success')
}

const getMasterReligion = async(req, reply) => {
  const id = req.params.id
  const religion = await findById(id)
  if (!religion) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, religion, 'fetch data success')
}

const putMasterReligion = async(req, reply) => {
  const id = req.params.id
  const religion = await findById(id)
  if (!religion) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMasterReligion(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMasterReligion = async(req, reply) => {
  const id = req.params.id
  const religion = await findById(id)
  if (!religion) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMasterReligion(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMasterReligion,
  getAllMasterReligion,
  getMasterReligion,
  putMasterReligion,
  deleteMasterReligion,
}
