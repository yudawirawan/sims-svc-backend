const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'TeacherStatus')

const findById = async(id) => {
  return await App.db.TeacherStatus.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createTeacherStatus = async(payload) => {
  try {
    const res = await App.db.TeacherStatus.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateTeacherStatus = async(id, payload) => {
  try {
    const res = await App.db.TeacherStatus.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyTeacherStatus = async(id) => {
  try {
    await App.db.TeacherStatus.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createTeacherStatus,
  updateTeacherStatus,
  destroyTeacherStatus,
}
