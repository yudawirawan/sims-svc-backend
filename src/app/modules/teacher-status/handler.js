const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createTeacherStatus, updateTeacherStatus, destroyTeacherStatus
} = require('./service')

const postTeacherStatus = async(req, reply) => {
  const res = await createTeacherStatus(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllTeacherStatus = async(req, reply) => {
  const t_status = await findPaging(req.query)
  return ok(reply, t_status, 'fetch data success')
}

const getTeacherStatus = async(req, reply) => {
  const id = req.params.id
  const t_status = await findById(id)
  if (!t_status) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, t_status, 'fetch data success')
}

const putTeacherStatus = async(req, reply) => {
  const id = req.params.id
  const t_status = await findById(id)
  if (!t_status) {
    return notFound(reply, 'data not found')
  }
  const res = await updateTeacherStatus(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteTeacherStatus = async(req, reply) => {
  const id = req.params.id
  const t_status = await findById(id)
  if (!t_status) {
    return notFound(reply, 'data not found')
  }
  const res = destroyTeacherStatus(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postTeacherStatus,
  getAllTeacherStatus,
  getTeacherStatus,
  putTeacherStatus,
  deleteTeacherStatus,
}
