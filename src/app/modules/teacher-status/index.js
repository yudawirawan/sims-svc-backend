const { postTeacherStatus, putTeacherStatus, deleteTeacherStatus, getAllTeacherStatus, getTeacherStatus } = require('./handler')
const {
  postTeacherStatusSchema, getAllTeacherStatusSchema, getTeacherStatusSchema, putTeacherStatusSchema, deleteTeacherStatusSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postTeacherStatusSchema }, postTeacherStatus)
  app.get('/', { schema: getAllTeacherStatusSchema }, getAllTeacherStatus)
  app.get('/:id', { schema: getTeacherStatusSchema }, getTeacherStatus)
  app.put('/:id', { schema: putTeacherStatusSchema }, putTeacherStatus)
  app.delete('/:id', { schema: deleteTeacherStatusSchema }, deleteTeacherStatus)
}
