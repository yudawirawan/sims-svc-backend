const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postTeacherStatusSchema = {
  description: 'Create teacher status data',
  tags: ['teacher-status'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      name: { type: 'string' },
    },
    required: [
      'name',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            name: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllTeacherStatusSchema = {
  description: 'List all teacher statuses data',
  tags: ['teacher-status'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              name: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getTeacherStatusSchema = {
  description: 'Get teacher status data by id',
  tags: ['teacher-status'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        name: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putTeacherStatusSchema = {
  description: 'Update teacher status data',
  summary: 'Update data',
  tags: ['teacher-status'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      name: { type: 'string' },
    },
    required: [
      'name',
    ],
  },
  params: idParamsSchema,
}

const deleteTeacherStatusSchema = {
  description: 'Delete teacher status data',
  summary: 'Delete data',
  tags: ['teacher-status'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postTeacherStatusSchema,
  getAllTeacherStatusSchema,
  getTeacherStatusSchema,
  putTeacherStatusSchema,
  deleteTeacherStatusSchema,
}
