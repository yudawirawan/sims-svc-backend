const App = require('../../index')
const { queryPaging } = require('../../utils/http_schema')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MatpelAssessmentAspect')


const findById = async(id) => {
  return await App.db.MatpelAssessmentAspect.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMatpelAssessmentAspect = async(payload) => {
  try {
    const res = await App.db.MatpelAssessmentAspect.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateMatpelAssessmentAspect = async(id, payload) => {
  try {
    const res = await App.db.MatpelAssessmentAspect.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyMatpelAssessmentAspect = async(id) => {
  try {
    await App.db.MatpelAssessmentAspect.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMatpelAssessmentAspect,
  updateMatpelAssessmentAspect,
  destroyMatpelAssessmentAspect,
}
