const { postMatpelAssessmentAspect, putMatpelAssessmentAspect, deleteMatpelAssessmentAspect, getAllMatpelAssessmentAspect, getMatpelAssessmentAspect } = require('./handler')
const {
  postMatpelAssessmentAspectSchema, getAllMatpelAssessmentAspectSchema, getMatpelAssessmentAspectSchema, putMatpelAssessmentAspectSchema, deleteMatpelAssessmentAspectSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMatpelAssessmentAspectSchema }, postMatpelAssessmentAspect)
  app.get('/', { schema: getAllMatpelAssessmentAspectSchema }, getAllMatpelAssessmentAspect)
  app.get('/:id', { schema: getMatpelAssessmentAspectSchema }, getMatpelAssessmentAspect)
  app.put('/:id', { schema: putMatpelAssessmentAspectSchema }, putMatpelAssessmentAspect)
  app.delete('/:id', { schema: deleteMatpelAssessmentAspectSchema }, deleteMatpelAssessmentAspect)
}
