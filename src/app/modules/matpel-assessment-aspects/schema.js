const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postMatpelAssessmentAspectSchema = {
  description: 'Create assessment aspect data',
  tags: ['matpel-assessment-aspects'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      code: { type: 'string' },
      name: { type: 'string' },
      status: { type: 'number' },
      description: { type: 'string' },
    },
    required: [
      'code',
      'name',
      'status',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            code: { type: 'string' },
            name: { type: 'string' },
            status: { type: 'number' },
            description: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMatpelAssessmentAspectSchema = {
  description: 'List all assessment aspects data',
  tags: ['matpel-assessment-aspects'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              code: { type: 'string' },
              name: { type: 'string' },
              status: { type: 'number' },
              description: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMatpelAssessmentAspectSchema = {
  description: 'Get assessment aspect data by id',
  tags: ['matpel-assessment-aspects'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        code: { type: 'string' },
        name: { type: 'string' },
        status: { type: 'number' },
        description: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMatpelAssessmentAspectSchema = {
  description: 'Update assessment aspect data',
  summary: 'Update data',
  tags: ['matpel-assessment-aspects'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      code: { type: 'string' },
      name: { type: 'string' },
      status: { type: 'number' },
      description: { type: 'string' },
    },
    required: [
      'code',
      'name',
      'status',
    ],
  },
  params: idParamsSchema,
}

const deleteMatpelAssessmentAspectSchema = {
  description: 'Delete assessment aspect data',
  summary: 'Delete data',
  tags: ['matpel-assessment-aspects'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMatpelAssessmentAspectSchema,
  getAllMatpelAssessmentAspectSchema,
  getMatpelAssessmentAspectSchema,
  putMatpelAssessmentAspectSchema,
  deleteMatpelAssessmentAspectSchema,
}
