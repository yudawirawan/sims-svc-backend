const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMatpelAssessmentAspect, updateMatpelAssessmentAspect, destroyMatpelAssessmentAspect
} = require('./service')

const postMatpelAssessmentAspect = async(req, reply) => {
  const res = await createMatpelAssessmentAspect(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllMatpelAssessmentAspect = async(req, reply) => {
  const aspect = await findPaging(req.query)
  return ok(reply, aspect, 'fetch data success')
}

const getMatpelAssessmentAspect = async(req, reply) => {
  const id = req.params.id
  const aspect = await findById(id)
  if (!aspect) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, aspect, 'fetch data success')
}

const putMatpelAssessmentAspect = async(req, reply) => {
  const id = req.params.id
  const aspect = await findById(id)
  if (!aspect) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMatpelAssessmentAspect(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMatpelAssessmentAspect = async(req, reply) => {
  const id = req.params.id
  const aspect = await findById(id)
  if (!aspect) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMatpelAssessmentAspect(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postMatpelAssessmentAspect,
  getAllMatpelAssessmentAspect,
  getMatpelAssessmentAspect,
  putMatpelAssessmentAspect,
  deleteMatpelAssessmentAspect,
}
