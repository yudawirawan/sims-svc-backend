const { login, getMe, postUser, putUser, deleteUser, deleteUserBatch, getUsers, getUser } = require('./handler')
const {
  loginSchema, meScheme, postUserSchema, getUsersSchema, getUserSchema, putUserSchema, deleteUserSchema, deleteUsersSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/login', { schema: loginSchema }, login)
  app.post('/logout', { schema: loginSchema }, login)
  app.get('/me', { schema: meScheme }, getMe)

  app.post('/users', { schema: postUserSchema }, postUser)
  app.get('/users', { schema: getUsersSchema }, getUsers)
  app.get('/users/:id', { schema: getUserSchema }, getUser)
  app.put('/users/:id', { schema: putUserSchema }, putUser)
  app.delete('/users/:id', { schema: deleteUserSchema }, deleteUser)
  app.post('/users/delete-batch', { schema: deleteUsersSchema }, deleteUserBatch)
}
