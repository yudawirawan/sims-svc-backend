const { ok, unAuthorize, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findByUsername, findPaging, createUser, updateUser, destroyUser, validatePassword, genJwt, destroyUsers
} = require('./service')

const getMe = async(req, reply) => {
  const { id } = req.user
  const user = await findById(id)
  return ok(reply, { ...user }, 'Fetch my data success')
}

const login = async(req, reply) => {
  const { username, password } = req.body
  const checkUser = await findByUsername(username)
  if (!checkUser) {
    return unAuthorize(reply, 'Invalid username or password')
  }

  const checkPass = await validatePassword(password, checkUser.password)
  if (!checkPass) {
    return unAuthorize(reply, 'Invalid username or password')
  }

  const jwt = genJwt(checkUser)
  const user = { ...checkUser }
  delete user.password
  return ok(reply, { jwt, user }, 'Login success')
}

const postUser = async(req, reply) => {
  const res = await createUser(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create user successs')
}

const getUsers = async(req, reply) => {
  const users = await findPaging(req.query)
  return ok(reply, users, 'fetch users success')
}

const getUser = async(req, reply) => {
  const id = req.params.id
  const user = await findById(id)
  if (!user) {
    return notFound(reply, 'User not found')
  }
  return ok(reply, user, 'fetch user success')
}

const putUser = async(req, reply) => {
  const id = req.params.id
  const user = await findById(id)
  if (!user) {
    return notFound(reply, 'User not found')
  }
  const res = await updateUser(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update user success')
}

const deleteUser = async(req, reply) => {
  const id = req.params.id
  const user = await findById(id)
  if (!user) {
    return notFound(reply, 'User not found')
  }
  const res = destroyUser(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete user success')
}

const deleteUserBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyUsers(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete users success')
}

module.exports = {
  getMe,
  login,
  postUser,
  getUsers,
  getUser,
  putUser,
  deleteUser,
  deleteUserBatch,
}
