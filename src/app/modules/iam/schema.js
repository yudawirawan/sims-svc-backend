const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const loginSchema = {
  description: 'Login user',
  tags: ['iam'],
  summary: 'Authenticate user on db then response jwt',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      username: { type: 'string' },
      password: { type: 'string' },
    },
    required: [
      'username',
      'password',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            jwt: { type: 'string' }
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err401,
  },
}

const meScheme = {
  description: 'Get loggedin user info',
  tags: ['iam'],
  summary: 'Fetch loggedin user data using id on Authorization headers',
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            username: { type: 'string' },
            email: { type: 'string' },
            fullName: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err401,
  }
}

const postUserSchema = {
  description: 'Create user',
  tags: ['iam-users'],
  summary: 'Create user',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      username: { type: 'string' },
      password: { type: 'string' },
      email: { type: 'string' },
      fullName: { type: 'string' },
    },
    required: [
      'username',
      'password',
      'email',
      'fullName',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            username: { type: 'string' },
            email: { type: 'string' },
            fullName: { type: 'string' },
            updatedBy: { type: 'string' },
            createdBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getUsersSchema = {
  description: 'List user',
  tags: ['iam-users'],
  summary: 'List user',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              username: { type: 'string' },
              email: { type: 'string' },
              fullName: { type: 'string' },
              updatedBy: { type: 'number' },
              createdBy: { type: 'number' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getUserSchema = {
  description: 'Get user by id',
  tags: ['iam-users'],
  summary: 'Get user by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        username: { type: 'string' },
        email: { type: 'string' },
        fullName: { type: 'string' },
        updatedBy: { type: 'number' },
        createdBy: { type: 'number' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putUserSchema = {
  description: 'Update user',
  summary: 'Update user',
  tags: ['iam-users'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      username: { type: 'string' },
      password: { type: 'string' },
      email: { type: 'string' },
      fullName: { type: 'string' },
    },
    required: [
      'username', 'email', 'fullName'
    ],
  },
  params: idParamsSchema,
}

const deleteUserSchema = {
  description: 'Delete user',
  summary: 'Delete user',
  tags: ['iam-users'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        username: { type: 'string' },
      }
    },
    ...err401,
  },
}

const deleteUsersSchema = {
  description: 'Delete users',
  summary: 'Delete users batch',
  tags: ['iam-users'],
  body: {
    type: 'object',
    properties: {
      ids: { type: 'array' },
    },
    required: ['ids'],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        username: { type: 'string' },
      }
    },
    ...err401,
  },
}

module.exports = {
  meScheme,
  loginSchema,
  postUserSchema,
  getUsersSchema,
  getUserSchema,
  putUserSchema,
  deleteUserSchema,
  deleteUsersSchema,
}
