const App = require('./../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'User')

const findById = async(id) => {
  return await db.User.findOne({ where: { id } })
}

const findPaging = async(query) => {
  if (!query.filters) {
    query.filters = {}
  }
  query.filters.username = { ne: 'superadmin' }
  query.filters._condition = 'and'
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const findByUsername = async(username) => {
  return await db.User.scope('withPassword').findOne({ where: { username } })
}

const createUser = async(payload) => {
  try {
    const bcrypt = require('bcrypt')
    payload.password = await bcrypt.hash(payload.password, 10)
    const res = await db.User.create(payload)
    // delete res.dataValues.password
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateUser = async(id, payload) => {
  try {
    if (payload.password) {
      const bcrypt = require('bcrypt')
      payload.password = await bcrypt.hash(payload.password, 10)
    }
    return await db.User.update(payload, { where: { id } })
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyUser = async(id) => {
  try {
    await db.User.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyUsers = async(ids) => {
  try {
    await db.User.destroy({
      where: { id: ids }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const validatePassword = async(password, hashedPassword) => {
  const bcrypt = require('bcrypt')
  return await bcrypt.compare(password, hashedPassword)
}

const genJwt = (user) => {
  const payload = {
    id: user.id,
    username: user.username,
  }
  return App.jwt.sign({ payload })
}

module.exports = {
  findById,
  findByUsername,
  findPaging,
  createUser,
  updateUser,
  destroyUser,
  destroyUsers,
  validatePassword,
  genJwt,
}
