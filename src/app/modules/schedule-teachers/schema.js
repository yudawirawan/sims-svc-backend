const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postScheduleTeachersSchema = {
  description: 'Create Schedule Teachers Process data',
  tags: ['schedule-teachers'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      teacherId: { type: 'number' },
      departmentId: { type: 'number' },
      schoolYearId: { type: 'number' },
      studyHoursId: { type: 'number' },
      levelId: { type: 'number' },
      classId: { type: 'number' },
      teacherSubjectId: { type: 'number' },
      day: { type: 'number' },
      status: { type: 'number' },
      description: { type: 'string' },
    },
    required: [
      'teacherId',
      'departmentId',
      'schoolYearId',
      'studyHoursId',
      'levelId',
      'classId',
      'teacherSubjectId',
      'day',
      'status',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            teacherId: { type: 'number' },
            departmentId: { type: 'number' },
            schoolYearId: { type: 'number' },
            studyHoursId: { type: 'number' },
            levelId: { type: 'number' },
            classId: { type: 'number' },
            teacherSubjectId: { type: 'number' },
            day: { type: 'number' },
            status: { type: 'number' },
            description: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllScheduleTeachersSchema = {
  description: 'List all Schedule Teachers Process data',
  tags: ['schedule-teachers'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              teacherId: { type: 'number' },
              departmentId: { type: 'number' },
              schoolYearId: { type: 'number' },
              studyHoursId: { type: 'number' },
              levelId: { type: 'number' },
              classId: { type: 'number' },
              teacherSubjectId: { type: 'number' },
              day: { type: 'number' },
              status: { type: 'number' },
              description: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getScheduleTeachersSchema = {
  description: 'Get Schedule Teachers Process data by id',
  tags: ['schedule-teachers'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        teacherId: { type: 'number' },
        departmentId: { type: 'number' },
        schoolYearId: { type: 'number' },
        studyHoursId: { type: 'number' },
        levelId: { type: 'number' },
        classId: { type: 'number' },
        teacherSubjectId: { type: 'number' },
        day: { type: 'number' },
        status: { type: 'number' },
        description: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putScheduleTeachersSchema = {
  description: 'Update Schedule Teachers Process data',
  summary: 'Update data',
  tags: ['schedule-teachers'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      teacherId: { type: 'number' },
      departmentId: { type: 'number' },
      schoolYearId: { type: 'number' },
      studyHoursId: { type: 'number' },
      levelId: { type: 'number' },
      classId: { type: 'number' },
      teacherSubjectId: { type: 'number' },
      day: { type: 'number' },
      status: { type: 'number' },
      description: { type: 'string' },
    },
    required: [
      'teacherId',
      'departmentId',
      'schoolYearId',
      'studyHoursId',
      'levelId',
      'classId',
      'teacherSubjectId',
      'day',
      'status',
    ],
  },
  params: idParamsSchema,
}

const deleteScheduleTeachersSchema = {
  description: 'Delete Schedule Teachers Process data',
  summary: 'Delete data',
  tags: ['schedule-teachers'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postScheduleTeachersSchema,
  getAllScheduleTeachersSchema,
  getScheduleTeachersSchema,
  putScheduleTeachersSchema,
  deleteScheduleTeachersSchema,
}
