const { postScheduleTeachers, putScheduleTeachers, deleteScheduleTeachers, deleteScheduleTeachersBatch, getAllScheduleTeachers, getScheduleTeachers } = require('./handler')
const {
  postScheduleTeachersSchema, getAllScheduleTeachersSchema, getScheduleTeachersSchema, putScheduleTeachersSchema, deleteScheduleTeachersSchema, deleteScheduleTeachersBatchSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postScheduleTeachersSchema }, postScheduleTeachers)
  app.get('/', { schema: getAllScheduleTeachersSchema }, getAllScheduleTeachers)
  app.get('/:id', { schema: getScheduleTeachersSchema }, getScheduleTeachers)
  app.put('/:id', { schema: putScheduleTeachersSchema }, putScheduleTeachers)
  app.delete('/:id', { schema: deleteScheduleTeachersSchema }, deleteScheduleTeachers)
}
