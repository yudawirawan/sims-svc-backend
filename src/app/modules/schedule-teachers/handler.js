const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createScheduleTeachers, updateScheduleTeachers, destroyScheduleTeachers,
} = require('./service')

const postScheduleTeachers = async(req, reply) => {
  const res = await createScheduleTeachers(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllScheduleTeachers = async (req, reply) => {
  const teacher_schedules = await findPaging(req.query)
  return ok(reply, teacher_schedules, 'fetch data success')
}

const getScheduleTeachers = async(req, reply) => {
  const id = req.params.id
  const teacher_schedules = await findById(id)
  if (!teacher_schedules) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, teacher_schedules, 'fetch data success')
}

const putScheduleTeachers = async(req, reply) => {
  const id = req.params.id
  const teacher_schedules = await findById(id)
  if (!teacher_schedules) {
    return notFound(reply, 'data not found')
  }
  const res = await updateScheduleTeachers(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteScheduleTeachers = async(req, reply) => {
  const id = req.params.id
  const teacher_schedules = await findById(id)
  if (!teacher_schedules) {
    return notFound(reply, 'data not found')
  }
  const res = destroyScheduleTeachers(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deleteScheduleTeachersBatch = async(req, reply) => {
  const { ids } = req.body
  const res = destroyScheduleTeachers(ids)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete users success')
}

module.exports = {
  postScheduleTeachers,
  getAllScheduleTeachers,
  getScheduleTeachers,
  putScheduleTeachers,
  deleteScheduleTeachers,
  deleteScheduleTeachersBatch,
}
