const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'ScheduleTeachers')

const findById = async(id) => {
  return await App.db.ScheduleTeachers.findOne({ where: { id } })
}

const findPaging = async (query) => {
  query.include = [
    { association: 'teacher', attributes: ['id'], 
      include: 
        { association: 'kepegawaian', attributes: ['nama'] },
    },
    { association: 'teacher', attributes: ['status_id'], 
      include: 
        { association: 'teacherStatus', attributes: ['name'] },
    },
    { association: 'department', attributes: ['name'] },
    { association: 'schoolYear', attributes: ['schoolYear'] },
    { association: 'studyHour', attributes: ['hour'] },
    { association: 'level', attributes: ['level'] },
    { association: 'class', attributes: ['name'] },
    { association: 'teacherSubject', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
  return await repository.findPaging(query)
}

const createScheduleTeachers = async(payload) => {
  try {
    const res = await App.db.ScheduleTeachers.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateScheduleTeachers = async(id, payload) => {
  try {
    const res = await App.db.ScheduleTeachers.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyScheduleTeachers = async(id) => {
  try {
    await App.db.ScheduleTeachers.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createScheduleTeachers,
  updateScheduleTeachers,
  destroyScheduleTeachers,
}
