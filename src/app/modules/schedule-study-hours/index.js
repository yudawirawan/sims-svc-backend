const { postScheduleStudyHours, putScheduleStudyHours, deleteScheduleStudyHours, deleteScheduleStudyHoursBatch, getAllScheduleStudyHours, getScheduleStudyHours } = require('./handler')
const {
  postScheduleStudyHoursSchema, getAllScheduleStudyHoursSchema, getScheduleStudyHoursSchema, putScheduleStudyHoursSchema, deleteScheduleStudyHoursSchema, deleteScheduleStudyHoursBatchSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postScheduleStudyHoursSchema }, postScheduleStudyHours)
  app.get('/', { schema: getAllScheduleStudyHoursSchema }, getAllScheduleStudyHours)
  app.get('/:id', { schema: getScheduleStudyHoursSchema }, getScheduleStudyHours)
  app.put('/:id', { schema: putScheduleStudyHoursSchema }, putScheduleStudyHours)
  app.delete('/:id', { schema: deleteScheduleStudyHoursSchema }, deleteScheduleStudyHours)
}
