const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createScheduleStudyHours, updateScheduleStudyHours, destroyScheduleStudyHours,
} = require('./service')

const postScheduleStudyHours = async(req, reply) => {
  const res = await createScheduleStudyHours(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllScheduleStudyHours = async (req, reply) => {
  const study = await findPaging(req.query)
  return ok(reply, study, 'fetch data success')
}

const getScheduleStudyHours = async(req, reply) => {
  const id = req.params.id
  const study = await findById(id)
  if (!study) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, study, 'fetch data success')
}

const putScheduleStudyHours = async(req, reply) => {
  const id = req.params.id
  const study = await findById(id)
  if (!study) {
    return notFound(reply, 'data not found')
  }
  const res = await updateScheduleStudyHours(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteScheduleStudyHours = async(req, reply) => {
  const id = req.params.id
  const study = await findById(id)
  if (!study) {
    return notFound(reply, 'data not found')
  }
  const res = destroyScheduleStudyHours(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}



module.exports = {
  postScheduleStudyHours,
  getAllScheduleStudyHours,
  getScheduleStudyHours,
  putScheduleStudyHours,
  deleteScheduleStudyHours,
}
