const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postScheduleStudyHoursSchema = {
  description: 'Create Study Hours data',
  tags: ['schedule-study-hours'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      hour: { type: 'number' },
      startTime: { type: 'string' },
      finishTime: { type: 'string' },
    },
    required: [
      'departmentId',
      'hour',
      'startTime',
      'finishTime',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            departmentId: { type: 'number' },
            hour: { type: 'number' },
            startTime: { type: 'string' },
            finishTime: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllScheduleStudyHoursSchema = {
  description: 'List all Study Hours data',
  tags: ['schedule-study-hours'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              hour: { type: 'number' },
              startTime: { type: 'string' },
              finishTime: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getScheduleStudyHoursSchema = {
  description: 'Get Schedule Hours Process data by id',
  tags: ['schedule-study-hours'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        hour: { type: 'number' },
        startTime: { type: 'string' },
        finishTime: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putScheduleStudyHoursSchema = {
  description: 'Update Schedule Hours Process data',
  summary: 'Update data',
  tags: ['schedule-study-hours'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      hour: { type: 'number' },
      startTime: { type: 'string' },
      finishTime: { type: 'string' },
    },
    required: [
      'departmentId',
      'hour',
      'startTime',
      'finishTime',
    ],
  },
  params: idParamsSchema,
}

const deleteScheduleStudyHoursSchema = {
  description: 'Delete Schedule Hours Process data',
  summary: 'Delete data',
  tags: ['schedule-study-hours'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postScheduleStudyHoursSchema,
  getAllScheduleStudyHoursSchema,
  getScheduleStudyHoursSchema,
  putScheduleStudyHoursSchema,
  deleteScheduleStudyHoursSchema,
}
