const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'ScheduleStudyHours')

const findById = async(id) => {
  return await App.db.ScheduleStudyHours.findOne({ where: { id } })
}

const findPaging = async (query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] }
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createScheduleStudyHours = async(payload) => {
  try {
    const res = await App.db.ScheduleStudyHours.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateScheduleStudyHours = async(id, payload) => {
  try {
    const res = await App.db.ScheduleStudyHours.update(payload, { where: { id } })
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyScheduleStudyHours = async(id) => {
  try {
    await App.db.ScheduleStudyHours.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createScheduleStudyHours,
  updateScheduleStudyHours,
  destroyScheduleStudyHours,
}
