const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createMasterStudentCreds, updateMasterStudentCreds, destroyMasterStudentCreds,
} = require('./service')

const postMasterStudentCreds = async(req, reply) => {
  const result = await createMasterStudentCreds(req.body)
  if (result.error) {
    return badRequest(reply, result.error)
  }
  return ok(reply, result, 'Create data success')
}

const getAllMasterStudentCreds = async(req, reply) => {
  const result = await findPaging(req.query)
  return ok(reply, result, 'fetch data success')
}

const getMasterStudentCreds = async(req, reply) => {
  const id = req.params.id
  const result = await findById(id)
  if (!result) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, result, 'fetch data success')
}

const putMasterStudentCreds = async(req, reply) => {
  const id = req.params.id
  const result = await findById(id)
  if (!result) {
    return notFound(reply, 'data not found')
  }
  const res = await updateMasterStudentCreds(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteMasterStudentCreds = async(req, reply) => {
  const id = req.params.id
  const result = await findById(id)
  if (!result) {
    return notFound(reply, 'data not found')
  }
  const res = destroyMasterStudentCreds(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

const deleteMasterStudentCredsBatch = async(req, reply) => {
  const { ids } = req.body
  const result = destroyMasterStudentCreds(ids)
  if (result.error) {
    return badRequest(reply, result.error)
  }
  return ok(reply, null, 'Delete users success')
}

module.exports = {
  postMasterStudentCreds,
  getAllMasterStudentCreds,
  getMasterStudentCreds,
  putMasterStudentCreds,
  deleteMasterStudentCreds,
  deleteMasterStudentCredsBatch,
}
