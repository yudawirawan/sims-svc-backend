const { postMasterStudentCreds, putMasterStudentCreds, deleteMasterStudentCreds, getAllMasterStudentCreds, getMasterStudentCreds } = require('./handler')
const {
  postMasterStudentCredsSchema, getAllMasterStudentCredsSchema, getMasterStudentCredsSchema, putMasterStudentCredsSchema, deleteMasterStudentCredsSchema,
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postMasterStudentCredsSchema }, postMasterStudentCreds)
  app.get('/', { schema: getAllMasterStudentCredsSchema }, getAllMasterStudentCreds)
  app.get('/:id', { schema: getMasterStudentCredsSchema }, getMasterStudentCreds)
  app.put('/:id', { schema: putMasterStudentCredsSchema }, putMasterStudentCreds)
  app.delete('/:id', { schema: deleteMasterStudentCredsSchema }, deleteMasterStudentCreds)
}
