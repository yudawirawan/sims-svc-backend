const { err401, err400, idParamsSchema, queryPaging, resp200 } = require('../../utils/http_schema')

const postMasterStudentCredsSchema = {
  description: 'Create student data',
  tags: ['master-student-creds'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      classId: { type: 'number' },
      studentId: { type: 'number' },
      studentPin: { type: 'string' },
      parentPin: { type: 'string' },
    },
    required: [
      'departmentId',
      'levelId',
      'classId',
      'studentId',
      'studentPin',
      'parentPin',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            departmentId: { type: 'number' },
            levelId: { type: 'number' },
            classId: { type: 'number' },
            studentId: { type: 'number' },
            studentPin: { type: 'string' },
            parentPin: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllMasterStudentCredsSchema = {
  description: 'List all student data',
  tags: ['master-student-creds'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              departmentId: { type: 'number' },
              levelId: { type: 'number' },
              classId: { type: 'number' },
              studentId: { type: 'number' },
              studentPin: { type: 'string' },
              parentPin: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getMasterStudentCredsSchema = {
  description: 'Get student data by id',
  tags: ['master-student-creds'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        departmentId: { type: 'number' },
        levelId: { type: 'number' },
        classId: { type: 'number' },
        studentId: { type: 'number' },
        studentPin: { type: 'string' },
        parentPin: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putMasterStudentCredsSchema = {
  description: 'Update student data',
  summary: 'Update data',
  tags: ['master-student-creds'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      classId: { type: 'number' },
      studentId: { type: 'number' },
      studentPin: { type: 'string' },
      parentPin: { type: 'string' },
    },
    required: [
      'departmentId',
      'levelId',
      'classId',
      'studentId',
      'studentPin',
      'parentPin',
    ],
  },
  params: idParamsSchema,
}

const deleteMasterStudentCredsSchema = {
  description: 'Delete batch psb process',
  summary: 'Delete psb process batch',
  tags: ['master-student-creds'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postMasterStudentCredsSchema,
  getAllMasterStudentCredsSchema,
  getMasterStudentCredsSchema,
  putMasterStudentCredsSchema,
  deleteMasterStudentCredsSchema,
}
