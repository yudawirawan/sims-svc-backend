const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MasterStudentCreds')

const findById = async(id) => {
  return await App.db.MasterStudentCreds.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'level', attributes: ['level'] },
    { association: 'class', attributes: ['name'] },
    { association: 'student', attributes: ['nama'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] },
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createMasterStudentCreds = async(payload) => {
  try {
    return await App.db.MasterStudentCreds.create(payload)
  } catch (err) {
    console.log(err)
    return {
      error: err.message,
    }
  }
}

const updateMasterStudentCreds = async(id, payload) => {
  try {
    return await App.db.MasterStudentCreds.update(payload, { where: { id } })
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const destroyMasterStudentCreds = async(id) => {
  try {
    await App.db.MasterStudentCreds.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const genRegisterId = async() => {
  try {
    const moment = require('moment-timezone')
    const period = moment().tz('Asia/Jakarta').format('YYYY')
    const [[result]] = await db.sequelize
      .query(`SELECT registerId FROM psb_candidates WHERE registerId LIKE '${period}-%' ORDER BY registerId DESC LIMIT 1`)
    let genRegId = ''
    if (result && result.registerId) {
      const regIdNum = result.registerId.substring(5, result.registerId.length)
      const regId = Number(regIdNum) + 1
      genRegId = period + '-' + String(regId).padStart(4, '0')
    } else {
      genRegId = moment().tz('Asia/Jakarta').format('YYYY') + '-0001'
    }
    return {
      genRegId
    }
  } catch (err) {
    console.log(err)
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createMasterStudentCreds,
  updateMasterStudentCreds,
  destroyMasterStudentCreds,
  genRegisterId,
}
