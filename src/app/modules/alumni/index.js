const { postAlumni, putAlumni, deleteAlumni, getAllAlumni, getAlumni } = require('./handler')
const {
  postAlumniSchema, getAllAlumniSchema, getAlumniSchema, putAlumniSchema, deleteAlumniSchema
} = require('./schema')

module.exports = async(app) => {
  app.post('/', { schema: postAlumniSchema }, postAlumni)
  app.get('/', { schema: getAllAlumniSchema }, getAllAlumni)
  app.get('/:id', { schema: getAlumniSchema }, getAlumni)
  app.put('/:id', { schema: putAlumniSchema }, putAlumni)
  app.delete('/:id', { schema: deleteAlumniSchema }, deleteAlumni)
}
