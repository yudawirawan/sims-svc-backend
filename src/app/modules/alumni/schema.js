const { err401, err400, idParamsSchema, queryPaging } = require('../../utils/http_schema')

const postAlumniSchema = {
  description: 'Create alumni data',
  tags: ['alumni'],
  summary: 'Create data',
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      registerId: { type: 'string' },
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      classId: { type: 'number' },
      admissionProcessId: { type: 'number' },
      candidateGroupId: { type: 'number' },
      studentId: { type: 'number' },
      iuranWajib: { type: 'number' },
      iuranSukarela: { type: 'number' },
      status: { type: 'number' },
      graduated: { type: 'number' },
      graduatedYear: { type: 'string' },
    },
    required: [
      'departmentId',
      'levelId',
      'classId',
      'studentId',
    ],
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            registerId: { type: 'string' },
            departmentId: { type: 'number' },
            levelId: { type: 'number' },
            classId: { type: 'number' },
            admissionProcessId: { type: 'number' },
            candidateGroupId: { type: 'number' },
            studentId: { type: 'number' },
            iuranWajib: { type: 'number' },
            iuranSukarela: { type: 'number' },
            status: { type: 'number' },
            graduated: { type: 'number' },
            graduatedYear: { type: 'string' },
            createdBy: { type: 'string' },
            updatedBy: { type: 'string' },
            createdAt: { type: 'string' },
            updatedAt: { type: 'string' },
          }
        },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
    ...err400,
  },
}

const getAllAlumniSchema = {
  description: 'List all alumni data',
  tags: ['alumni'],
  summary: 'Get all data',
  query: queryPaging,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        statusCode: { type: 'number' },
        message: { type: 'string' },
        data: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              registerId: { type: 'string' },
              departmentId: { type: 'number' },
              levelId: { type: 'number' },
              classId: { type: 'number' },
              admissionProcessId: { type: 'number' },
              candidateGroupId: { type: 'number' },
              studentId: { type: 'number' },
              iuranWajib: { type: 'number' },
              iuranSukarela: { type: 'number' },
              status: { type: 'number' },
              graduated: { type: 'number' },
              graduatedYear: { type: 'string' },
              createdBy: { type: 'string' },
              updatedBy: { type: 'string' },
              createdAt: { type: 'string' },
              updatedAt: { type: 'string' },
            },
          }
        }
      },
    },
  },
}

const getAlumniSchema = {
  description: 'Get alumni data by id',
  tags: ['alumni'],
  summary: 'Get data by id',
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' },
        registerId: { type: 'string' },
        departmentId: { type: 'number' },
        levelId: { type: 'number' },
        classId: { type: 'number' },
        admissionProcessId: { type: 'number' },
        candidateGroupId: { type: 'number' },
        studentId: { type: 'number' },
        iuranWajib: { type: 'number' },
        iuranSukarela: { type: 'number' },
        status: { type: 'number' },
        graduated: { type: 'number' },
        graduatedYear: { type: 'string' },
        createdBy: { type: 'string' },
        updatedBy: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' },
      }
    },
    ...err401,
  },
}

const putAlumniSchema = {
  description: 'Update data',
  summary: 'Update data',
  tags: ['alumni'],
  body: {
    type: 'object',
    additionalProperties: false,
    properties: {
      registerId: { type: 'string' },
      departmentId: { type: 'number' },
      levelId: { type: 'number' },
      classId: { type: 'number' },
      admissionProcessId: { type: 'number' },
      candidateGroupId: { type: 'number' },
      studentId: { type: 'number' },
      iuranWajib: { type: 'number' },
      iuranSukarela: { type: 'number' },
      status: { type: 'number' },
      graduated: { type: 'number' },
      graduatedYear: { type: 'string' },
    },
    required: [
      'departmentId',
      'levelId',
      'classId',
      'studentId',
    ],
  },
  params: idParamsSchema,
}

const deleteAlumniSchema = {
  description: 'Delete alumni data',
  summary: 'Delete data',
  tags: ['alumni'],
  params: idParamsSchema,
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        id: { type: 'number' }
      }
    },
    ...err401,
  },
}

module.exports = {
  postAlumniSchema,
  getAllAlumniSchema,
  getAlumniSchema,
  putAlumniSchema,
  deleteAlumniSchema,
}
