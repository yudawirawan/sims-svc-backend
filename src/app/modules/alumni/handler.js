const { ok, notFound, badRequest } = require('../../utils/http_response')
const {
  findById, findPaging, createAlumni, updateAlumni, destroyAlumni
} = require('./service')

const postAlumni = async(req, reply) => {
  const res = await createAlumni(req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Create data success')
}

const getAllAlumni = async(req, reply) => {
  const acad = await findPaging(req.query)
  return ok(reply, acad, 'fetch data success')
}

const getAlumni = async(req, reply) => {
  const id = req.params.id
  const acad = await findById(id)
  if (!acad) {
    return notFound(reply, 'data not found')
  }
  return ok(reply, acad, 'fetch data success')
}

const putAlumni = async(req, reply) => {
  const id = req.params.id
  const acad = await findById(id)
  if (!acad) {
    return notFound(reply, 'data not found')
  }
  const res = await updateAlumni(id, req.body)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, res, 'Update data success')
}

const deleteAlumni = async(req, reply) => {
  const id = req.params.id
  const acad = await findById(id)
  if (!acad) {
    return notFound(reply, 'data not found')
  }
  const res = destroyAlumni(id)
  if (res.error) {
    return badRequest(reply, res.error)
  }
  return ok(reply, null, 'Delete data success')
}

module.exports = {
  postAlumni,
  getAllAlumni,
  getAlumni,
  putAlumni,
  deleteAlumni,
}
