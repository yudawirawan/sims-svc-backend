const App = require('../../index')
const { db } = App
const repository = require('../../plugins/database/repository')(db, 'MasterStudentReference')

const findById = async(id) => {
  return await App.db.MasterStudentReference.findOne({ where: { id } })
}

const findPaging = async(query) => {
  query.include = [
    { association: 'department', attributes: ['name'] },
    { association: 'student', attributes: ['nama', 'nisn', 'jk'] },
    { association: 'userCreatedBy', attributes: ['fullName'] },
    { association: 'userUpdatedBy', attributes: ['fullName'] },
  ]
  const { count, rows } = await repository.findPaging(query)
  return {
    count, rows,
  }
}

const createAlumni = async(payload) => {
  try {
    const res = await App.db.MasterStudentReference.create(payload)
    return res
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

const updateAlumni = async(id, payload) => {
  try {
    const res = await App.db.MasterStudentReference.update(payload, { where: { id } })
    return res
  } catch (err) {
    console.log(err)
    return {
      error: err.message,
    }
  }
}

const destroyAlumni = async(id) => {
  try {
    await App.db.MasterStudentReference.destroy({
      where: { id }
    })
    return true
  } catch (err) {
    return {
      error: err.message,
    }
  }
}

module.exports = {
  findById,
  findPaging,
  createAlumni,
  updateAlumni,
  destroyAlumni,
}
