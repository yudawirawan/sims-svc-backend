const fp = require('fastify-plugin')

module.exports = fp(async function(fastify, opts) {
  fastify.addHook('onRequest', async(request, reply) => {
    let { page, limit } = request.query
    if (page !== undefined) {
      page = parseInt(limit)
    }
    if (limit !== undefined) {
      limit = parseInt(limit)
    }
    if (page !== undefined && limit !== undefined) {
      limit = ((parseInt(request.query.page) - 1) * limit) + limit
    }
    if (request.query.page !== undefined) {
      page = parseInt(request.query.page) - 1
      if (page > 0) {
        page = page * parseInt(request.query.limit)
      }
    }
    request.query.page = page
    request.query.limit = limit
  })

  fastify.addHook('preHandler', async(request) => {
    const url = request.raw.url
    // handle filters, searches, orders, distinct, attributes
    if (url.includes('filters') || url.includes('searches') || url.includes('orders') || url.includes('distinct') ||
      url.includes('attributes') || url.includes('include')) {
      const url = request.raw.url.split('?')
      const qs = require('qs')
      const queries = qs.parse(url[1])
      if (queries.filters) { request.query.filters = queries.filters }
      if (queries.orders) { request.query.orders = queries.orders }
      if (queries.searches) { request.query.searches = queries.searches }
      if (queries.distinct) { request.query.distinct = queries.distinct }
      if (queries.attributes) { request.query.attributes = queries.attributes }
      if (queries.include) { request.query.include = queries.include }
    }
  })
})
