const fp = require('fastify-plugin')
const Swagger = require('fastify-swagger')
const Path = require('path')
const FastifyStatic = require('fastify-static')

const schemes = process.env.API_DOMAIN || 'http'

const config = {
  routePrefix: '/docs',
  swagger: {
    info: {
      title: 'SIMS API',
      description: 'API documentation of Integral SIMS System',
      version: '0.1.0',
      contact: {
        name: 'Eko Prasetyo',
        email: 'm.eko.budi.p@gmail.com',
        url: 'https://dianglobaltech.co.id/',
      }
    },
    externalDocs: {
      url: 'https://swagger.io',
      description: 'Find more info here'
    },
    // host: process.env.API_HOST,
    schemes,
    consumes: ['application/json'],
    produces: ['application/json'],
    definitions: {
      User: {
        type: 'object',
        required: [
          'username',
          'password',
          'email',
          'fullName'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          username: {
            type: 'string',
            format: 'varchar50',
            description: 'unique'
          },
          password: {
            type: 'string',
            format: 'varchar65'
          },
          email: {
            type: 'string',
            format: 'varchar100',
            description: 'unique, valid email'
          },
          fullName: {
            type: 'string',
            format: 'varchar100'
          }
        }
      },
      LocationProvincies: {
        type: 'object',
        required: [
          'code',
          'name',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          code: {
            type: 'string',
            format: 'varchar2'
          },
          name: {
            type: 'string',
            format: 'varchar150'
          },
          latitude: {
            type: 'string',
            format: 'text'
          },
          longitue: {
            type: 'string',
            format: 'text',
          }
        }
      },
      LocationCities: {
        type: 'object',
        required: [
          'code',
          'name',
          'province_code',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          code: {
            type: 'string',
            format: 'varchar4'
          },
          province_code: {
            $ref: '#/definitions/LocationProvincies'
          },
          name: {
            type: 'string',
            format: 'varchar150'
          },
          latitude: {
            type: 'string',
            format: 'text'
          },
          longitue: {
            type: 'string',
            format: 'text',
          }
        }
      },
      LocationDistricts: {
        type: 'object',
        required: [
          'code',
          'name',
          'city_code',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          code: {
            type: 'string',
            format: 'varchar7'
          },
          city_code: {
            $ref: '#/definitions/LocationCities'
          },
          name: {
            type: 'string',
            format: 'varchar200'
          },
          latitude: {
            type: 'string',
            format: 'text'
          },
          longitue: {
            type: 'string',
            format: 'text',
          }
        }
      },
      LocationVillages: {
        type: 'object',
        required: [
          'code',
          'name',
          'district_code',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          code: {
            type: 'string',
            format: 'varchar10'
          },
          district_code: {
            $ref: '#/definitions/LocationDistricts'
          },
          name: {
            type: 'string',
            format: 'varchar255'
          },
          latitude: {
            type: 'string',
            format: 'text'
          },
          longitue: {
            type: 'string',
            format: 'text',
          }
        }
      },
      ConfigSchool: {
        type: 'object',
        required: [
          'logo',
          'name',
          'address',
          'telp1',
          'hp1',
          'website',
          'email',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          logo: {
            type: 'string',
            format: 'varchar50'
          },
          name: {
            type: 'string',
            format: 'varchar100'
          },
          address: {
            type: 'string',
            format: 'text'
          },
          telp1: {
            type: 'string',
            format: 'varchar15'
          },
          telp2: {
            type: 'string',
            format: 'varchar15'
          },
          hp1: {
            type: 'string',
            format: 'varchar15'
          },
          hp2: {
            type: 'string',
            format: 'varchar15'
          },
          website: {
            type: 'string',
            format: 'varchar100'
          },
          email: {
            type: 'string',
            format: 'varchar100',
            description: 'unique, valid email'
          },
        }
      },
      MasterRole: {
        type: 'object',
        required: [
          'name',
          'status'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          name: {
            type: 'string',
            format: 'varchar15'
          },
          status: {
            type: 'boolean',
            default: true
          }
        }
      },
      MasterKepegawaian: {
        type: 'object',
        required: [
          'nama',
          'nuptk',
          'jk',
          'tempatLahir',
          'statusKepegawaian',
          'jenisPtk',
          'agama',
          'alamatJalan',
          'rt',
          'rw',
          'namaDusun',
          'desa',
          'kecamatan',
          'kodePos',
          'telepon',
          'hp',
          'email',
          'nik'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          nama: {
            type: 'string',
            format: 'varchar100',
          },
          nuptk: {
            type: 'string',
            format: 'varchar16',
          },
          jk: {
            type: 'string',
            format: 'char1',
            description: 'value eg. L or P, but no boolean'
          },
          tempatLahir: {
            type: 'string',
            format: 'text',
          },
          tanggalLahir: {
            type: 'string',
            format: 'date-only',
            description: 'value yyyy-mm-dd eg. 1966-12-11'
          },
          nip: {
            type: 'string',
            format: 'varchar20',
          },
          statusKepegawaian: {
            type: 'string',
            format: 'varchar20',
          },
          jenisPtk: {
            type: 'string',
            format: 'varchar10',
          },
          agama: {
            type: 'string',
            format: 'varchar10',
          },
          alamatJalan: {
            type: 'string',
            format: 'text',
          },
          rt: {
            type: 'string',
            format: 'char5',
          },
          rw: {
            type: 'string',
            format: 'char5',
          },
          namaDusun: {
            type: 'string',
            format: 'text',
          },
          desa: {
            type: 'string',
            format: 'text',
          },
          kecamatan: {
            type: 'string',
            format: 'text',
          },
          kodePos: {
            type: 'string',
            format: 'varchar5',
          },
          hp: {
            type: 'string',
            format: 'varchar15',
          },
          email: {
            type: 'string',
            format: 'varchar100',
            description: 'unique, valid email'
          },
          tanggalCpns: {
            type: 'string',
            format: 'date-only',
            description: 'value yyyy-mm-dd eg. 1966-12-11'
          },
          lembagaPengangkatan: {
            type: 'string',
            format: 'varchar15',
          },
          pangkatGolongan: {
            type: 'string',
            format: 'varchar10',
          },
          sumberGaji: {
            type: 'string',
            format: 'varchar15',
          },
          namaIbuKandung: {
            type: 'string',
            format: 'varchar100',
          },
          statusPerkawinan: {
            type: 'string',
            format: 'varchar15',
          },
          namaSuamiIstri: {
            type: 'string',
            format: 'varchar100',
          },
          nipSuamiIstri: {
            type: 'string',
            format: 'varchar20',
          },
          pekerjaanSuamiIstri: {
            type: 'string',
            format: 'text',
          },
          tmtPns: {
            type: 'string',
            format: 'data-only',
            description: 'value yyyy-mm-dd eg. 1966-12-11'
          },
          sudahLisensiKepalaSekolah: {
            type: 'string',
            format: 'varchar5',
          },
          pernahDiklatKepengawasan: {
            type: 'string',
            format: 'varchar5',
          },
          keahlianBraille: {
            type: 'string',
            format: 'varchar5',
          },
          keahlianBahasaIsyarat: {
            type: 'string',
            format: 'varchar15',
          },
          namaWajibPajak: {
            type: 'string',
            format: 'varchar100',
          },
          kewarganegaraan: {
            type: 'string',
            format: 'varchar3',
          },
          bank: {
            type: 'string',
            format: 'varchar50',
          },
          nomorRekeningBank: {
            type: 'string',
            format: 'varchar20',
          },
          rekeningAtasNama: {
            type: 'string',
            format: 'varchar100',
          },
          nik: {
            type: 'string',
            format: 'varchar16',
            description: 'unique'
          },
          noKk: {
            type: 'string',
            format: 'varchar16',
          },
          karpeg: {
            type: 'string',
            format: 'varchar18',
          },
          karisKarsu: {
            type: 'string',
            format: 'varchar15',
          },
          lintang: {
            type: 'string',
            format: 'text',
          },
          bujur: {
            type: 'string',
            format: 'text',
          },
          nuks: {
            type: 'string',
            format: 'varchar30',
          },
        }
      },
      MasterDepartment: {
        type: 'object',
        required: [
          'name',
          'kepegawaianId',
          'status',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          name: {
            type: 'string',
            format: 'varchar12'
          },
          kepegawaianId: {
            $ref: '#/definitions/MasterKepegawaian'
          },
          description: {
            type: 'string',
            format: 'text'
          },
          status: {
            type: 'number',
            format: 'number',
            description: 'value eg. 0 or 1 etc, but not boolean'
          },
        }
      },
      MasterGeneration: {
        type: 'object',
        required: [
          'departmentId',
          'generation',
          'status',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          description: {
            type: 'string',
            format: 'text'
          },
          status: {
            type: 'number',
            format: 'number',
            description: 'value eg. 0 or 1 etc, but not boolean'
          },
        }
      },
      MasterReligion: {
        type: 'object',
        required: [
          'name',
          'order',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          name: {
            type: 'string',
            format: 'varchar10'
          },
          order: {
            type: 'number',
            format: 'number',
            description: 'value ordering descending'
          },
          description: {
            type: 'string',
            format: 'text'
          }
        }
      },
      MasterLevel: {
        type: 'object',
        required: [
          'departmentId',
          'level',
          'status',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          level: {
            type: 'string',
            format: 'char3'
          },
          description: {
            type: 'string',
            format: 'text'
          }
        },
      },
      MasterSchoolYear: {
        type: 'object',
        required: [
          'departmentId',
          'schoolYear',
          'startDate',
          'endDate',
          'status'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          schoolYear: {
            type: 'string',
            format: 'varchar20'
          },
          startDate: {
            type: 'string',
            format: 'date-time',
            description: 'value yyyy-mm-dd hh:mm:ss eg. 2009-02-05 00:00:00'
          },
          endDate: {
            type: 'string',
            format: 'date-time',
            description: 'value yyyy-mm-dd hh:mm:ss eg. 2009-02-05 00:00:00'
          },
          description: {
            type: 'string',
            format: 'text'
          },
          status: {
            type: 'number',
            format: 'number'
          }
        }
      },
      MasterSemester: {
        type: 'object',
        required: [
          'departmentId',
          'semester',
          'status'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          semester: {
            type: 'string',
            format: 'varchar6'
          },
          description: {
            type: 'string',
            format: 'text'
          },
          status: {
            type: 'number',
            format: 'number'
          }
        }
      },
      MasterClass: {
        type: 'object',
        required: [
          'departmentId',
          'levelId',
          'schoolYearId',
          'name',
          'kepegawaianId'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          levelId: {
            $ref: '#/definitions/MasterLevel'
          },
          schoolYearId: {
            $ref: '#/definitions/MasterSchoolYear'
          },
          name: {
            type: 'string',
            format: 'varchar5'
          },
          kepegawaianId: {
            $ref: '#/definitions/MasterKepegawaian'
          },
          capacity: {
            type: 'number',
            format: 'number'
          },
          capacityFilled: {
            type: 'number',
            format: 'number'
          }
        }
      },
      MasterSubject: {
        type: 'object',
        required: [
          'departmentId',
          'code',
          'status'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          code: {
            type: 'string',
            format: 'varchar6'
          },
          name: {
            type: 'string',
            format: 'varchar30'
          },
          lessonHours: {
            type: 'integer',
            format: 'int64'
          },
          curriculumStatus: {
            type: 'string',
            format: 'varchar50'
          },
          status: {
            type: 'number',
            format: 'number'
          },
          description: {
            type: 'string',
            format: 'text'
          }
        }
      },
      MatpelLessonPlan: {
        type: 'object',
        required: [
          'departmentId',
          'levelId',
          'semesterId',
          'subjectId',
          'code',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          levelId: {
            $ref: '#/definitions/MasterLevel'
          },
          semesterId: {
            $ref: '#/definitions/MasterSemester'
          },
          subjectId: {
            $ref: '#/definitions/MasterSubject'
          },
          code: {
            type: 'string',
            format: 'varchar30'
          },
          material: {
            type: 'string',
            format: 'text'
          },
          description: {
            type: 'string',
            format: 'text'
          },
          status: {
            type: 'number',
            format: 'number'
          }
        }
      },
      MatpelAssessmentAspect: {
        type: 'object',
        required: [
          'code',
          'name',
          'status',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64',
          },
          code: {
            type: 'string',
            format: 'varchar20',
          },
          name: {
            type: 'string',
            format: 'varchar100'
          },
          status: {
            type: 'number',
            format: 'number'
          },
          description: {
            type: 'string',
            format: 'text'
          }
        }
      },
      MatpelGradingRule: {
        type: 'object',
        required: [
          'teacherId',
          'subjectId',
          'levelId',
          'assessmentAspectId',
          'minValue',
          'maxValue',
          'grade'
        ],
        properties: {
          id: {
            type: 'integer',
            firmat: 'int64'
          },
          teacherId: {
            $ref: '#/definitions/Teacher'
          },
          subjectId: {
            $ref: '#/definitions/MasterSubject'
          },
          levelId: {
            $ref: '#/definitions/MasterLevel'
          },
          assessmentAspectId: {
            $ref: '#/definitions/MatpelAssessmentAspect'
          },
          minValue: {
            type: 'string',
            varchar: 'varchar7'
          },
          maxValue: {
            type: 'string',
            varchar: 'varchar7'
          },
          grade: {
            type: 'string',
            varchar: 'varchar2',
            description: 'value eg. A, B, C'
          }
        }
      },
      MatpelTestType: {
        type: 'object',
        required: [
          'departmentId',
          'subjectId',
          'testType',
          'abbreviation',
          'status',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          subjectId: {
            $ref: '#/definitions/MasterSubject'
          },
          testType: {
            type: 'string',
            format: 'varchar50'
          },
          abbreviation: {
            type: 'string',
            format: 'varchar10'
          },
          status: {
            type: 'number',
            format: 'number'
          },
          description: {
            type: 'string',
            format: 'text'
          },
        }
      },
      MatpelWeighting: {
        type: 'object',
        required: [
          'teacherId',
          'subjectId',
          'testTypeId',
          'weight',
        ],
        properties: {
          id: {
            type: 'integer',
            firmat: 'int64'
          },
          teacherId: {
            $ref: '#/definitions/Teacher'
          },
          subjectId: {
            $ref: '#/definitions/MasterSubject'
          },
          testTypeId: {
            $ref: '#/definitions/MatpelTestType'
          },
          assessmentAspectId: {
            $ref: '#/definitions/MatpelAssessmentAspect'
          },
          weight: {
            type: 'string',
            format: 'varchar20'
          }
        }
      },
      TeacherStatus: {
        type: 'object',
        required: [
          'name'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          name: {
            type: 'string',
            format: 'varchar50'
          }
        }
      },
      Teacher: {
        type: 'object',
        required: [
          'departmentId',
          'subjectId',
          'kepegawaianId',
          'statusId'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          subjectId: {
            $ref: '#/definitions/MasterSubject'
          },
          kepegawaianId: {
            $ref: '#/definitions/MasterKepegawaian'
          },
          statusId: {
            $ref: '#/definitions/TeacherStatus',
          }
        }
      },
      SchStudyHours: {
        type: 'object',
        required: [
          'departmentId',
          'hour',
          'startTime',
          'finishTime',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          hour: {
            type: 'number',
            format: 'number'
          },
          startTime: {
            type: 'string',
            format: 'time-only',
            description: 'value eq hh::mm'
          },
          finishTime: {
            type: 'string',
            format: 'time-only',
            description: 'value eq hh::mm'
          },
        }
      },
      SchTeachers: {
        type: 'object',
        required: [
          'teacher_id',
          'department_id',
          'school_year_id',
          'study_hours_id',
          'level_id',
          'class_id',
          'teacher_subject_id',
          'day',
          'status',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          teacher_id: {
            $ref: '#/definitions/Teacher'
          },
          department_id: {
            $ref: '#/definitions/MasterDepartment'
          },
          school_year_id: {
            $ref: '#/definitions/MasterSchoolYear'
          },
          study_hours_id: {
            $ref: '#/definitions/SchStudyHours'
          },
          level_id: {
            $ref: '#/definitions/MasterLevel'
          },
          teacher_subject_id: {
            $ref: '#/definitions/MasterSubject'
          },
          day: {
            type: 'number',
            format: 'number',
          },
          status: {
            type: 'number',
            format: 'number',
          },
          description: {
            type: 'string',
            format: 'text',
          },
        }
      },
      PsbSetting: {
        type: 'object',
        required: [
          'departmentId',
          'processId',
          'status',
          'exams',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          processId: {
            $ref: '#/definitions/PsbProcess'
          },
          mandorityFreeCode: {
            type: 'string',
            format: 'varchar10'
          },
          mandorityFreeName: {
            type: 'string',
            format: 'varchar100'
          },
          voluntaryFreeCode: {
            type: 'string',
            format: 'varchar10'
          },
          voluntaryFreeName: {
            type: 'string',
            format: 'varchar100'
          },
          status: {
            type: 'number',
            format: 'number'
          },
          exams: {
            $ref: '#/definitions/PsbSettingExams'
          }
        }
      },
      PsbSettingExams: {
        type: 'object',
        required: [
          'code',
          'name',
          'status'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          code: {
            type: 'string',
            format: 'varchar8'
          },
          name: {
            type: 'string',
            format: 'varchar50'
          },
          status: {
            type: 'number',
            format: 'number'
          }
        }
      },
      PsbProcess: {
        type: 'object',
        required: [
          'departmentId',
          'name',
          'code',
          'status'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          name: {
            type: 'string',
            format: 'varchar100'
          },
          code: {
            type: 'string',
            format: 'varchar15',
            description: 'unique'
          },
          description: {
            type: 'string',
            format: 'text'
          },
          status: {
            type: 'number',
            format: 'number'
          }
        }
      },
      PsbCandidate: {
        type: 'object',
        required: [
          'registerId',
          'departmentId',
          'admission_process_id',
          'nama',
          'jk',
          'tanggalLahir',
          'nik',
          'agama',
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          registerId: {
            type: 'string',
            format: 'varchar30'
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          admissionProcessId: {
            $ref: '#/definitions/PsbProcess'
          },
          candidateGroupId: {
            $ref: '#/definitions/PsbCandidateGroup'
          },
          no: {
            type: 'integer',
            format: 'int64'
          },
          nama: {
            type: 'string',
            format: 'varchar100'
          },
          nipd: {
            type: 'string',
            format: 'varchar20'
          },
          jk: {
            type: 'string',
            format: 'char1'
          },
          nisn: {
            type: 'string',
            format: 'varchar16'
          },
          tempatLahir: {
            type: 'string',
            format: 'text'
          },
          tanggalLahir: {
            type: 'string',
            format: 'date-only',
            description: 'value yyyy-mm-dd eg. 1966-12-11'
          },
          nik: {
            type: 'string',
            format: 'varchar16'
          },
          agama: {
            type: 'string',
            format: 'varchar10'
          },
          alamat: {
            type: 'string',
            format: 'text'
          },
          rt: {
            type: 'string',
            format: 'varchar5'
          },
          rw: {
            type: 'string',
            format: 'varchar5'
          },
          dusun: {
            type: 'string',
            format: 'text'
          },
          kelurahan: {
            type: 'string',
            format: 'text'
          },
          kecamatan: {
            type: 'string',
            format: 'text'
          },
          kodepos: {
            type: 'string',
            format: 'varchar5'
          },
          jenisTinggal: {
            type: 'string',
            format: 'varchar30'
          },
          alatTransportasi: {
            type: 'string',
            format: 'varchar30'
          },
          telepon: {
            type: 'string',
            format: 'varchar50'
          },
          hp: {
            type: 'string',
            format: 'varchar15'
          },
          email: {
            type: 'string',
            format: 'varchar100'
          },
          skhun: {
            type: 'string',
            format: 'varchar25'
          },
          penerimaKps: {
            type: 'string',
            format: 'varchar5'
          },
          noKps: {
            type: 'string',
            format: 'varchar50'
          },
          namaAyah: {
            type: 'string',
            format: 'varchar100'
          },
          tahunLahirAyah: {
            type: 'string',
            format: 'varchar5'
          },
          jenjangPendidikanAyah: {
            type: 'string',
            format: 'varchar20'
          },
          pekerjaanAyah: {
            type: 'string',
            format: 'varchar20'
          },
          penghasilanAyah: {
            type: 'string',
            format: 'varchar20'
          },
          namaIbu: {
            type: 'string',
            format: 'varchar100'
          },
          tahunLahirIbu: {
            type: 'string',
            format: 'varchar5'
          },
          jenjangPendidikanIbu: {
            type: 'string',
            format: 'varchar20'
          },
          pekerjaanIbu: {
            type: 'string',
            format: 'varchar20'
          },
          penghasilanIbu: {
            type: 'string',
            format: 'varchar20'
          },
          nikIbu: {
            type: 'string',
            format: 'varchar16'
          },
          namaWali: {
            type: 'string',
            format: 'varchar100'
          },
          tahunLahirWali: {
            type: 'string',
            format: 'varchar5'
          },
          jenjangPendidikanWali: {
            type: 'string',
            format: 'varchar20'
          },
          pekerjaanWali: {
            type: 'string',
            format: 'varchar20'
          },
          penghasilanWali: {
            type: 'string',
            format: 'varchar20'
          },
          nikWali: {
            type: 'string',
            format: 'varchar16'
          },
          rombelSaatIni: {
            type: 'string',
            format: 'varchar20'
          },
          noPesertaUjianNasional: {
            type: 'string',
            format: 'varchar30'
          },
          noSeriIjazah: {
            type: 'string',
            format: 'varchar30'
          },
          penerimaKip: {
            type: 'string',
            format: 'varchar5'
          },
          nomorKip: {
            type: 'string',
            format: 'varchar20'
          },
          namaDiKip: {
            type: 'string',
            format: 'varchar100'
          },
          nomorKks: {
            type: 'string',
            format: 'varchar20'
          },
          noRegistrasiAktaLahir: {
            type: 'string',
            format: 'varchar30'
          },
          bank: {
            type: 'string',
            format: 'varchar50'
          },
          nomorRekeningBank: {
            type: 'string',
            format: 'varchar20'
          },
          layakPip: {
            type: 'string',
            format: 'varchar5'
          },
          alasanLayakPip: {
            type: 'string',
            format: 'varchar50'
          },
          kebutuhanKhusus: {
            type: 'string',
            format: 'varchar10'
          },
          sekolahAsal: {
            type: 'string',
            format: 'varchar100'
          },
          anakKe: {
            type: 'string',
            format: 'varchar5'
          },
          lintang: {
            type: 'string',
            format: 'text'
          },
          bujur: {
            type: 'string',
            format: 'text'
          },
          noKk: {
            type: 'string',
            format: 'varchar16'
          },
          beratBadan: {
            type: 'string',
            format: 'varchar3'
          },
          tinggiBadan: {
            type: 'string',
            format: 'varchar3'
          },
          lingkarKepala: {
            type: 'string',
            format: 'varchar3'
          },
          jumlahSaudaraKandung: {
            type: 'string',
            format: 'varchar3'
          },
          jarakRumahSekolah: {
            type: 'string',
            format: 'varchar9'
          },
        }
      },
      PsbCandidateGroup: {
        type: 'object',
        required: [
          'departmentId',
          'processId',
          'groupName',
          'status'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          processId: {
            $ref: '#/definitions/PsbProcess'
          },
          groupName: {
            type: 'string',
            format: 'varchar20'
          },
          capacity: {
            type: 'integer',
            format: 'int64'
          },
          description: {
            type: 'string',
            format: 'text'
          },
          status: {
            type: 'number',
            format: 'number'
          },
          startDate: {
            type: 'string',
            format: 'date-time',
            description: 'value yyyy-mm-dd hh:mm:ss eg. 2009-02-05 00:00:00'
          },
          endDate: {
            type: 'string',
            format: 'date-time',
            description: 'value yyyy-mm-dd hh:mm:ss eg. 2009-02-05 00:00:00'
          },
        }
      },
      PsbPlace: {
        type: 'object',
        required: [
          'departmentId',
          'processId',
          'groupId',
          'generationId',
          'levelId',
          'classId'
        ],
        properties: {
          id: {
            type: 'integer',
            format: 'int64'
          },
          departmentId: {
            $ref: '#/definitions/MasterDepartment'
          },
          processId: {
            $ref: '#/definitions/PsbProcess'
          },
          groupId: {
            $ref: '#/definitions/PsbCandidateGroup'
          },
          generationId: {
            $ref: '#/definitions/MasterGeneration'
          },
          levelId: {
            $ref: '#/definitions/MasterLevel'
          },
          classId: {
            $ref: '#/definitions/MasterClass'
          },
        }
      },
    },
    securityDefinitions: {
      Bearer: {
        type: 'apiKey',
        name: 'Authorization',
        in: 'header',
      }
    }
  },
  uiConfig: {
    docExpansion: 'list',
    deepLinking: true,
  },
  uiHooks: {
    onRequest: function(request, reply, next) { next() },
    preHandler: function(request, reply, next) { next() }
  },
  staticCSP: true,
  transformStaticCSP: (header) => header,
  exposeRoute: true,
}

module.exports = fp(async(fastify) => {
  fastify.register(Swagger, config)

  // Register /docs/statics to swagger/static/ folder
  fastify.register(FastifyStatic, {
    root: Path.join(__dirname, 'static'),
    prefix: '/docs/statics',
    decorateReply: false,
  })

  // Force redirect to our custom swagger ui
  fastify.addHook('onRequest', (request, reply, done) => {
    if (request.raw.url.includes('docs/static/index.html')) {
      reply.redirect('/docs/statics/index.html')
    }
    done()
  })
})
