const SearchBuilder = require('./sequilize-search-builder')
const Sequelize = require('sequelize')

const builder = (sequelize, query) => {
  return new SearchBuilder(sequelize, query)
    .setConfig({
      logging: process.env.NODE_ENV === 'development',
      fields: {
        filter: 'filters',
        order: 'orders',
        limit: 'limit',
        offset: 'page',
      }
    })
}

const repository = (db, modelName) => ({
  builder,
  findPaging: async(query) => {
    const search = builder(db.Sequilize, query)
    let whereQuery = search.getWhereQuery()

    if (query.searches && query.searches.value) {
      if (!whereQuery) {
        whereQuery = {}
      }
      if (!whereQuery[Sequelize.Op.and]) {
        whereQuery[Sequelize.Op.and] = {}
      }
      if (!whereQuery[Sequelize.Op.and][Sequelize.Op.or]) {
        whereQuery[Sequelize.Op.and] = {
          [Sequelize.Op.or]: []
        }
      }

      for (const field of query.searches.fields) {
        whereQuery[Sequelize.Op.and][Sequelize.Op.or].push(
          Sequelize.where(
            Sequelize.fn('lower', Sequelize.col(field)),
            'like', `%${query.searches.value.toLowerCase()}%`
          )
        )
      }
    }

    const orderQuery = search.getOrderQuery()
    const { page, limit } = query

    const params = {
      include: [{ all: true, nested: true, duplicating: false }],
      where: whereQuery,
      order: orderQuery,
      offset: page,
      logging: process.env.NODE_ENV === 'development' ? console.log : false,
      raw: false,
    }
    if (limit !== undefined) {
      params.limit = limit
    }
    if (query.include) {
      params.include = query.include
    }
    if (query.attributes) {
      params.attributes = query.attributes
    }
    if (query.distinct) {
      // params.distinct = true
      // params.col = query.distinct
      params.group = query.distinct
    }

    if (modelName === 'Teacher') {
      console.log(modelName)
      console.log('params')
      console.log(params)
    }

    try {
      return await db[modelName].findAndCountAll(params)
    } catch (e) {
      console.log(e)
      return {
        error: e.message,
        count: 0,
        rows: [],
      }
    }
  }
})

module.exports = repository
