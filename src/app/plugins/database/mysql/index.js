const fp = require('fastify-plugin')
const { requestContext } = require('fastify-request-context')
const Models = require('../../../../db/models')

module.exports = fp(async(fastify, opts, done) => {
  Models.sequelize.addHook('beforeCreate', (instance) => {
    const user = requestContext.get('user')
    instance.dataValues.createdBy = user.id
    instance.dataValues.updatedBy = user.id
  })

  Models.sequelize.addHook('beforeUpdate', (instance) => {
    const user = requestContext.get('user')
    instance.dataValues.updatedBy = user.id
  })

  fastify.decorate(opts.dbInstance, Models)

  // close Models database connection before shutdown
  // 'onClose' is triggered when fastify.close() is invoked to stop the server
  fastify.addHook(
    'onClose',
    (instance, done) => Models.close().then(() => done())
  )

  done()
})
