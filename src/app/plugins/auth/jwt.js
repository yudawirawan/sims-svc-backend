const fp = require('fastify-plugin')

module.exports = fp(async function(fastify, opts) {
  const messages = {
    badRequestErrorMessage: 'Format is Authorization: Bearer [token]',
    badCookieRequestErrorMessage: 'Cookie could not be parsed in request',
    noAuthorizationInHeaderMessage: 'No authorization token, please login.',
    noAuthorizationInCookieMessage: 'No authorization token, please login.',
    authorizationTokenExpiredMessage: 'Authorization token expired',
    authorizationTokenInvalid: (err) => `Authorization token is invalid: ${err.message}`,
    authorizationTokenUntrusted: 'Untrusted authorization token'
  }

  const formatUser = (user) => {
    return {
      id: user.payload.id,
      username: user.payload.username,
    }
  }

  const sign = {
    expiresIn: '1d'
  }

  const config = Object.assign({ whitelist: [] }, { messages, formatUser, sign }, opts)

  fastify.register(require('fastify-jwt'), config)

  fastify.decorate('authenticate', async function(request, reply) {
    try {
      await request.jwtVerify()
    } catch (err) {
      reply.send(err)
    }
  })

  fastify.addHook('onRequest', async(request, reply) => {
    try {
      // Only validate request that not in whitelist
      if (request.method !== 'OPTIONS' &&
        request.raw.url !== '/' &&
        !config.whitelist.some(v => request.raw.url.toUpperCase().includes(v.toUpperCase())) &&
        (request.context.schema && !request.context.schema.public)
      ) {
        await request.jwtVerify()
      }
    } catch (err) {
      reply.send(err)
    }
  })
})
