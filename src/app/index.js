const Fastify = require('fastify')
const Cors = require('fastify-cors')
const Jwt = require('./plugins/auth/jwt')
const Swagger = require('./plugins/swagger')
const Path = require('path')
const Autoload = require('fastify-autoload')
const Database = require('./plugins/database/mysql')
const FastifyConfig = require('./config/fastify')
const Pagination = require('./plugins/pagination')
const { fastifyRequestContextPlugin } = require('fastify-request-context')

const app = Fastify(FastifyConfig)

app.register(fastifyRequestContextPlugin, {
  hook: 'onRequest',
  defaultStoreValues: {
    user: { username: 'system', id: null }
  }
})
app.register(Jwt, { secret: process.env.SECRET, whitelist: ['login', 'docs', 'static', 'favico'] })
app.register(Cors, { origin: '*' })
app.register(Database, { dbInstance: 'db' })
app.register(Pagination)

app.setErrorHandler(function(error, request, reply) {
  console.log(error)
  if (error.validation) {
    reply.status(422).send(new Error(error.message))
  }
})

app.addHook('preHandler', (req, reply, done) => {
  if (req.user && req.user.id) {
    req.requestContext.set('user', { username: req.user.username, id: req.user.id })
  }
  done()
})

if (process.env.NODE_ENV === 'development') {
  app.register(Swagger)
}

app.register(Autoload, {
  dir: Path.join(__dirname, 'modules'),
  maxDepth: 1,
  options: { prefix: '/api' },
})

app.ready(async err => {
  if (err) throw err
  if (process.env.NODE_ENV === 'development') {
    app.swagger()
  }
  try {
    // first connection
    await app.db.sequelize.authenticate()
    console.log(`Database connection is successfully established. ${process.env.DB_HOST}:${process.env.DB_PORT}`)
  } catch (err) {
    console.log(`Connection could not established: ${err}`)
  }
})

app.get('/', async(request, reply) => {
  return { hello: 'Sims API is running...' }
})

module.exports = app
