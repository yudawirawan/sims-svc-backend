const ok = (reply, data, message) => {
  return reply.code(201).send({
    data,
    message,
    statusCode: 201,
  })
}

const badRequest = (reply, error) => {
  return reply.code(400).send({
    error,
    statusCode: 400,
  })
}

const unAuthorize = (reply, error) => {
  return reply.code(401).send({
    error,
    statusCode: 401,
  })
}

const forbidden = (reply, error) => {
  return reply.code(403).send({
    error,
    statusCode: 403,
  })
}

const notFound = (reply, error) => {
  return reply.code(404).send({
    error,
    statusCode: 404,
  })
}

module.exports = {
  ok,
  badRequest,
  unAuthorize,
  forbidden,
  notFound,
}
