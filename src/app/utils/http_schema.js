const queryPaging = {
  type: 'object',
  additionalProperties: false,
  properties: {
    page: { type: 'number', default: 1 },
    limit: { type: 'number', default: 100 },
    filters: { type: 'string' },
    orders: { type: 'string' },
    searches: { type: 'string' },
    distinct: { type: 'string' },
    attributes: { type: 'string' },
    include: { type: 'string' },
  },
  required: [
    'page',
  ],
}

const errSchema = {
  description: 'Error 400 response',
  type: 'object',
  properties: {
    error: { type: 'string' },
    statusCode: { type: 'number' },
  }
}

const err400 = {
  400: {
    ...errSchema,
  }
}

const err401 = {
  401: {
    ...errSchema,
  }
}

const err403 = {
  403: {
    ...errSchema,
  }
}

const idParamsSchema = {
  type: 'object',
  additionalProperties: false,
  required: ['id'],
  properties: { id: { type: 'number' } },
}

const resp200 = (data, description = 'Successful response') => {
  return {
    200: {
      description,
      type: 'object',
      properties: {
        data: { ...data },
        message: { type: 'string' },
        statusCode: { type: 'number' },
      }
    },
  }
}

module.exports = {
  queryPaging,
  err400,
  err401,
  err403,
  idParamsSchema,
  resp200,
}
