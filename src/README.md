SIMS API Service
================

# Tech Stack
- NodeJs v16 LTS
- MariaDB 10
- Micro Framework: [Fastify Framework](https://www.fastify.io/)
- ORM: [Sequelize](https://sequelize.org/master) 

# Running on Docker
- Copy past .env.example to .env
- Run docker compose: `docker-compose up --build -d`

Akan meng-generate nodejs & mariadb service (detail di [docker-compose.yml](../docker-compose.yml)):

- API run on http://localhost:9112
- API Docs on http://localhost:9112/docs
- MySQL exposed to localhost:3315
  - username: sims_user
  - password: test12345
  - db: sims
- ssh ke container sims-mgt-svc_nodejs, run db seeder: `npm run seeder`

# Init Project Manually
1. Setup npm: `npm install`
2. Setup Database:
    * create database
      ```
      mysql -u root -p
      CREATE USER 'sims_user'@'localhost' IDENTIFIED BY 'test12345';
      create database sims;
      GRANT ALL PRIVILEGES ON sims.* TO 'sims_user'@'localhost';
      FLUSH PRIVILEGES;
      \q
      ```
    * Database migrations & ORM [Migration using Sequelize](https://sequelize.org/master/manual/migrations.html)
      * Init migration on mariadb `npm run migrate`
      * Init seeders `npm run seeder`
3. Init env copy paste env.example to env
   - change DB_HOST to localhost.
   - Sesuaikan field lain sesuai kebutuhan.

# Running app manually
`npm run dev`

# API Documentation (Swagger)
- url: http://localhost:9112/docs 
- Klik tombol **try it out** untuk testing api.

# Application folder
- db: database related, [Sequilize Docs](https://sequelize.org/master/)
- app: folder sistem api
  - modules: module service yang diexpose di api
    - handler.js (logic controller)
    - schema.js : definisi & validasi endpoint [Schema Tutorial](https://ruanmartinelli.com/posts/using-schemas-fastify-fun-and-profit)
    - index.js: routing REST API (harus index.js agar bisa di autoload.)
  - plugins: reusable middleware 

# ORM Sequelize
[Sequelize-cli](https://github.com/sequelize/cli)

[Model Basic](https://sequelize.org/master/manual/model-basics.html)

[Migration Docs](https://sequelize.org/master/manual/migrations.html)

## Create migration & Model
```
npx sequelize-cli model:generate --name User --attributes field1:string,field2:string,field3:string
```

## Create only migration
```
npx sequelize-cli migration:generate --name table_change_operation
```

## create seeder
```
npx sequelize-cli seed:generate --name seeder_name
```

## 1 file seeding
```
npx sequelize-cli db:seed --seed file_seeder.js
```

## Undoing Migration All
```
npx sequelize-cli db:migrate:undo:all
```

## Undoin Seeder All
```
npx sequelize-cli db:seed:undo:all   
```