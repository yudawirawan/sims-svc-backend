'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.addColumn('master_student_references', 'graduated', {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      after: 'status',
    })
    await queryInterface.addColumn('master_student_references', 'graduatedYear', {
      type: Sequelize.STRING(10),
      after: 'graduated',
    })
  },

  down: async(queryInterface) => {
    await queryInterface.removeColumn('master_student_references', 'graduated')
    await queryInterface.removeColumn('master_student_references', 'graduatedYear')
  }
}
