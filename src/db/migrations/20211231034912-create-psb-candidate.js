'use strict'
module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.createTable('psb_candidates', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      registerId: {
        allowNull: false,
        type: Sequelize.STRING(10),
      },
      department_id: {
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_departments'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER,
      },
      admission_process_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'psb_processes'
          }
        }
      },
      candidate_group_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'psb_candidate_groups'
          }
        }
      },
      nama: {
        allowNull: false,
        type: Sequelize.STRING(100),
      },
      nipd: {
        type: Sequelize.STRING(20),
      },
      jk: {
        allowNull: false,
        type: Sequelize.CHAR(1),
      },
      nisn: {
        allowNull: false,
        type: Sequelize.STRING(16),
      },
      tempatLahir: {
        type: Sequelize.TEXT,
      },
      tanggalLahir: {
        allowNull: false,
        type: Sequelize.DATEONLY,
      },
      nik: {
        type: Sequelize.STRING(16),
      },
      agama: {
        allowNull: false,
        type: Sequelize.STRING(10),
      },
      alamat: {
        type: Sequelize.TEXT,
      },
      rt: {
        type: Sequelize.STRING(5),
      },
      rw: {
        type: Sequelize.STRING(5),
      },
      dusun: {
        type: Sequelize.TEXT,
      },
      kelurahan: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      kecamatan: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      kodepos: {
        allowNull: false,
        type: Sequelize.STRING(5),
      },
      jenisTinggal: {
        type: Sequelize.STRING(30),
      },
      alatTransportasi: {
        type: Sequelize.STRING(50),
      },
      telepon: {
        allowNull: false,
        type: Sequelize.STRING(15),
      },
      hp: {
        allowNull: false,
        type: Sequelize.STRING(15),
      },
      email: {
        type: Sequelize.STRING(100),
      },
      skhun: {
        allowNull: false,
        type: Sequelize.STRING(25),
      },
      penerimaKps: {
        allowNull: false,
        type: Sequelize.STRING(5),
      },
      noKps: {
        type: Sequelize.STRING(20),
      },
      namaAyah: {
        type: Sequelize.STRING(100),
      },
      tahunLahirAyah: {
        type: Sequelize.STRING(5),
      },
      jenjangPendidikanAyah: {
        type: Sequelize.STRING(20),
      },
      pekerjaanAyah: {
        type: Sequelize.STRING(20),
      },
      penghasilanAyah: {
        type: Sequelize.STRING(20),
      },
      nikAyah: {
        type: Sequelize.STRING(16),
      },
      namaIbu: {
        type: Sequelize.STRING(100),
      },
      tahunLahirIbu: {
        type: Sequelize.STRING(5),
      },
      jenjangPendidikanIbu: {
        type: Sequelize.STRING(20),
      },
      pekerjaanIbu: {
        type: Sequelize.STRING(20),
      },
      penghasilanIbu: {
        type: Sequelize.STRING(20),
      },
      nikIbu: {
        type: Sequelize.STRING(16),
      },
      namaWali: {
        type: Sequelize.STRING(100),
      },
      tahunLahirWali: {
        type: Sequelize.STRING(5),
      },
      jenjangPendidikanWali: {
        type: Sequelize.STRING(20),
      },
      pekerjaanWali: {
        type: Sequelize.STRING(20),
      },
      penghasilanWali: {
        type: Sequelize.STRING(20),
      },
      nikWali: {
        type: Sequelize.STRING(16),
      },
      rombelSaatIni: {
        type: Sequelize.STRING(20),
      },
      noPesertaUjianNasional: {
        type: Sequelize.STRING(30),
      },
      noSeriIjazah: {
        allowNull: false,
        type: Sequelize.STRING(30),
      },
      penerimaKip: {
        allowNull: false,
        type: Sequelize.STRING(5),
      },
      nomorKip: {
        type: Sequelize.STRING(20),
      },
      namaDiKip: {
        type: Sequelize.STRING(100),
      },
      nomorKks: {
        type: Sequelize.STRING(20),
      },
      noRegistrasiAktaLahir: {
        type: Sequelize.STRING(30),
      },
      bank: {
        type: Sequelize.STRING(50),
      },
      nomorRekeningBank: {
        type: Sequelize.STRING(20),
      },
      rekeningAtasNama: {
        type: Sequelize.STRING(100),
      },
      layakPip: {
        allowNull: false,
        type: Sequelize.STRING(5),
      },
      alasanLayakPip: {
        type: Sequelize.STRING(50),
      },
      kebutuhanKhusus: {
        type: Sequelize.STRING(25),
      },
      sekolahAsal: {
        type: Sequelize.STRING(100),
      },
      anakKe: {
        type: Sequelize.STRING(5),
      },
      lintang: {
        type: Sequelize.TEXT,
      },
      bujur: {
        type: Sequelize.TEXT,
      },
      noKk: {
        allowNull: false,
        type: Sequelize.STRING(16),
      },
      beratBadan: {
        type: Sequelize.STRING(3),
      },
      tinggiBadan: {
        type: Sequelize.STRING(3),
      },
      lingkarKepala: {
        type: Sequelize.STRING(3),
      },
      jumlahSaudaraKandung: {
        type: Sequelize.STRING(3),
      },
      jarakRumahSekolah: {
        type: Sequelize.STRING(9),
      },
      iuranWajib: {
        type: Sequelize.FLOAT,
      },
      iuranSukarela: {
        type: Sequelize.FLOAT,
      },
      status: {
        defaultValue: 0,
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      createdBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      updatedBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: async(queryInterface, Sequelize) => {
    await queryInterface.dropTable('psb_candidates')
  }
}
