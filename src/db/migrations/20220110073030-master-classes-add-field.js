'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.addColumn('master_classes', 'capacityFilled', {
      type: Sequelize.INTEGER,
      after: 'capacity'
    })
    await queryInterface.changeColumn('master_classes', 'capacity', {
      type: Sequelize.INTEGER,
    })
  },

  down: async(queryInterface) => {
    queryInterface.removeColumn('master_classes', 'capacityFilled')
  }
}
