'use strict'
module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.createTable('psb_processes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      department_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_departments'
          },
          key: 'id'
        },
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING(100)
      },
      code: {
        unique: true,
        type: Sequelize.STRING(15)
      },
      description: {
        type: Sequelize.TEXT
      },
      status: {
        type: Sequelize.INTEGER
      },
      createdBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      updatedBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: async(queryInterface, Sequelize) => {
    await queryInterface.dropTable('psb_processes')
  }
}
