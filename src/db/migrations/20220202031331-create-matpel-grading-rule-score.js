'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('matpel_grading_rule_scores', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      matpel_grading_rule_id: {
        allowNull : false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'matpel_grading_rules'
          },
          key: 'id'
        },
      },
      initialScore: {
        allowNull : false,
        type: Sequelize.INTEGER
      },
      finalScore: {
        allowNull : false,
        type: Sequelize.INTEGER
      },
      grade: {
        allowNull : false,
        type: Sequelize.STRING
      },
      createdBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      updatedBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('matpel_grading_rule_scores');
  }
};