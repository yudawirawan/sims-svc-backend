'use strict'
module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.createTable('master_kepegawaian', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nama: {
        allowNull: false,
        type: Sequelize.STRING(100)
      },
      nuptk: {
        allowNull: true,
        type: Sequelize.STRING(16)
      },
      jk: {
        allowNull: false,
        type: Sequelize.CHAR(1)
      },
      tempatLahir: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      tanggalLahir: {
        allowNull: false,
        type: Sequelize.DATEONLY
      },
      nip: {
        type: Sequelize.STRING(20)
      },
      statusKepegawaian: {
        allowNull: false,
        type: Sequelize.STRING(20)
      },
      jenisPtk: {
        allowNull: false,
        type: Sequelize.STRING(10)
      },
      agama: {
        allowNull: false,
        type: Sequelize.STRING(10)
      },
      alamatJalan: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      rt: {
        allowNull: true,
        type: Sequelize.CHAR(5)
      },
      rw: {
        allowNull: true,
        type: Sequelize.CHAR(5)
      },
      namaDusun: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      desa: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      kecamatan: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      kodePos: {
        allowNull: true,
        type: Sequelize.STRING(5)
      },
      telepon: {
        allowNull: true,
        type: Sequelize.STRING(15)
      },
      hp: {
        allowNull: true,
        type: Sequelize.STRING(15)
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING(100),
        unique: true
      },
      tugasTambahan: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      skCpns: {
        type: Sequelize.STRING(30)
      },
      tanggalCpns: {
        type: Sequelize.DATEONLY
      },
      skPengangkatan: {
        type: Sequelize.STRING(50)
      },
      tmtPengangkatan: {
        type: Sequelize.DATEONLY
      },
      lembagaPengangkatan: {
        type: Sequelize.STRING(50)
      },
      pangkatGolongan: {
        type: Sequelize.STRING(10)
      },
      sumberGaji: {
        type: Sequelize.STRING(15)
      },
      namaIbuKandung: {
        type: Sequelize.STRING(100)
      },
      statusPerkawinan: {
        type: Sequelize.STRING(15)
      },
      namaSuamiIstri: {
        type: Sequelize.STRING(100)
      },
      nipSuamiIstri: {
        type: Sequelize.STRING(20)
      },
      pekerjaanSuamiIstri: {
        type: Sequelize.TEXT
      },
      tmtPns: {
        type: Sequelize.DATEONLY
      },
      sudahLisensiKepalaSekolah: {
        type: Sequelize.STRING(5)
      },
      pernahDiklatKepengawasan: {
        type: Sequelize.STRING(5)
      },
      keahlianBraille: {
        type: Sequelize.STRING(5)
      },
      keahlianBahasaIsyarat: {
        type: Sequelize.STRING(5)
      },
      npwp: {
        type: Sequelize.STRING(15)
      },
      namaWajibPajak: {
        type: Sequelize.STRING(100)
      },
      kewarganegaraan: {
        type: Sequelize.STRING(3)
      },
      bank: {
        type: Sequelize.STRING(50)
      },
      nomorRekeningBank: {
        type: Sequelize.STRING(20)
      },
      rekeningAtasNama: {
        type: Sequelize.STRING(100)
      },
      nik: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING(16)
      },
      noKk: {
        type: Sequelize.STRING(16)
      },
      karpeg: {
        type: Sequelize.STRING(18)
      },
      karisKarsu: {
        type: Sequelize.STRING(15)
      },
      lintang: {
        type: Sequelize.TEXT
      },
      bujur: {
        type: Sequelize.TEXT
      },
      nuks: {
        type: Sequelize.STRING(30)
      },
      createdBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      updatedBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: async(queryInterface, Sequelize) => {
    await queryInterface.dropTable('master_kepegawaian')
  }
}
