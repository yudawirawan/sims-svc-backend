'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('psb_candidate_scores', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      candidate_id: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'psb_candidates'
          },
          key: 'id'
        },
        allowNull: false,
        type: Sequelize.INTEGER
      },
      exam_id: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'psb_setting_exams'
          },
          key: 'id'
        },
        allowNull: false,
        type: Sequelize.INTEGER
      },
      score: {
        allowNull: false,
        type: Sequelize.FLOAT
      },
      createdBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      updatedBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('psb_candidate_scores');
  }
};