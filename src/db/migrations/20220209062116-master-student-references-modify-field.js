'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.changeColumn('master_student_references', 'registerId', {
      type: Sequelize.STRING(10),
      allowNull: true,
    })
    await queryInterface.changeColumn('master_student_references', 'department_id', {
      type: Sequelize.INTEGER,
      allowNull: true,
    })
    await queryInterface.changeColumn('master_student_references', 'admission_process_id', {
      type: Sequelize.INTEGER,
      allowNull: true,
    })
    await queryInterface.changeColumn('master_student_references', 'candidate_group_id', {
      type: Sequelize.INTEGER,
      allowNull: true,
    })
  },

  down: async(queryInterface) => {
    await queryInterface.removeColumn('master_student_references', 'registerId')
    await queryInterface.removeColumn('master_student_references', 'department_id')
    await queryInterface.removeColumn('master_student_references', 'admission_process_id')
    await queryInterface.removeColumn('master_student_references', 'candidate_group_id')
  }
}
