'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.changeColumn('psb_processes', 'status', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 1,
    })
  },

  down: async(queryInterface) => {
    await queryInterface.removeColumn('psb_processes', 'status')
  }
}
