'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('student_extracurriculars', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      department_id: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_departments'
          },
          key: 'id'
        },
        allowNull: false,
        type: Sequelize.INTEGER
      },
      level_id: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_levels'
          },
          key: 'id'
        },
        allowNull: false,
        type: Sequelize.INTEGER
      },
      class_id: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_classes'
          },
          key: 'id'
        },
        allowNull: false,
        type: Sequelize.INTEGER
      },
      student_id: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_students'
          },
          key: 'id'
        },
        allowNull: false,
        type: Sequelize.INTEGER
      },
      subject_id_1: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_subjects'
          },
          key: 'id'
        },
        allowNull: false,
        type: Sequelize.INTEGER
      },
      subject_id_2: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_subjects'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      subject_id_3: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_subjects'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      createdBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      updatedBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('student_extracurriculars');
  }
};