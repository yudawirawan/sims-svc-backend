'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     await queryInterface.addColumn('master_kepegawaian', 'status', {
      type: Sequelize.INTEGER,
      defaultValue: 1,
      AllowNull: false,
      after: 'nuks'
    })
  },

  down: async (queryInterface) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     queryInterface.removeColumn('master_kepegawaian', 'status')
  }
};
