'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    queryInterface.addColumn('config_school', 'navigations', { type: Sequelize.DataTypes.JSON })
  },

  down: async(queryInterface) => {
    queryInterface.removeColumn('config_school', 'navigations')
  }
}
