'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('master_student_references', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      registerId: {
        allowNull: false,
        type: Sequelize.STRING(10),
      },
      department_id: {
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_departments'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      admission_process_id: {
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'psb_processes'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      candidate_group_id: {
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'psb_candidate_groups'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      student_id: {
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_students'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      iuranWajib: {
        type: Sequelize.INTEGER
      },
      iuranSukarela: {
        type: Sequelize.INTEGER
      },
      status: {
        defaultValue: 1,
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      createdBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      updatedBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('master_student_references');
  }
};