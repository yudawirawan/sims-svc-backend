'use strict';

module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.addColumn('master_student_references', 'level_id', 
    { 
      type: Sequelize.INTEGER,
      after: 'department_id',
      onDelete: 'cascade',
      onUpdate: 'cascade',
      references: {
        model: {
          tableName: 'master_levels'
        },
        key: 'id'
      },
    })
    await queryInterface.addColumn('master_student_references', 'class_id', 
    { 
      type: Sequelize.INTEGER,
      after: 'level_id',
      onDelete: 'cascade',
      onUpdate: 'cascade',
      references: {
        model: {
          tableName: 'master_classes'
        },
        key: 'id'
      },
    })
  },

  down: async(queryInterface) => {
    await queryInterface.removeColumn('master_student_references', 'level_id')
    await queryInterface.removeColumn('master_student_references', 'class_id')
  }
};
