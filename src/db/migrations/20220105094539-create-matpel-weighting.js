'use strict'
module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.createTable('matpel_weightings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      teacher_id: {
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'teachers'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      test_type_id: {
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'matpel_test_types'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      department_id: {
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_departments'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      level_id: {
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_levels'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      assessment_aspect_id: {
        allowNull: false,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'matpel_assessment_aspects'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      weight: {
        allowNull: false,
        type: Sequelize.STRING(20)
      },
      createdBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      updatedBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: async(queryInterface, Sequelize) => {
    await queryInterface.dropTable('matpel_weightings')
  }
}
