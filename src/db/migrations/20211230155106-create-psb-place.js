'use strict'
module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.createTable('psb_places', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      department_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_departments'
          },
          key: 'id'
        },
      },
      process_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'psb_processes'
          },
          key: 'id'
        },
      },
      namegroup_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'psb_candidate_groups'
          },
          key: 'id'
        },
      },
      generation_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_generations'
          },
          key: 'id'
        },
      },
      level_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_levels'
          },
          key: 'id'
        },
      },
      class_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'master_classes'
          },
          key: 'id'
        },
      },
      createdBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      updatedBy: {
        onDelete: 'cascade',
        onUpdate: 'cascade',
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        },
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: async(queryInterface, Sequelize) => {
    await queryInterface.dropTable('psb_places')
  }
}
