'use strict';

module.exports = {
  up: async(queryInterface, Sequelize) => {
    queryInterface.addColumn('master_classes', 'mentor_id', 
    { 
      type: Sequelize.INTEGER,
      after: 'kepegawaian_id',
      onDelete: 'cascade',
      onUpdate: 'cascade',
      references: {
        model: {
          tableName: 'master_kepegawaian'
        },
        key: 'id'
      },
    })
  },

  down: async(queryInterface) => {
    queryInterface.removeColumn('master_classes', 'mentor_id')
  }
}

