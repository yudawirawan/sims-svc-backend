require('dotenv-safe').config()

module.exports = {
  development: {
    username: process.env.DB_USER || 'root',
    password: process.env.DB_PASSWORD || 'root',
    database: process.env.DB_NAME || 'sims_svc',
    host: process.env.DB_HOST || 'mariadb',
    port: process.env.DB_PORT || '3306',
    dialect: 'mariadb',
    seederStorage: 'sequelize',
    seederStorageTableName: 'sequelizeData',
  },
  staging: {
    username: process.env.DB_USER || 'root',
    password: process.env.DB_PASSWORD || 'root',
    database: process.env.DB_NAME || 'sims_svc',
    host: process.env.DB_HOST || 'mariadb',
    port: process.env.DB_PORT || '3306',
    dialect: 'mariadb',
    seederStorage: 'sequelize',
    seederStorageTableName: 'sequelizeData',
  },
  production: {
    username: process.env.DB_USER || 'root',
    password: process.env.DB_PASSWORD || 'root',
    database: process.env.DB_NAME || 'sims_svc',
    host: process.env.DB_HOST || 'mariadb',
    port: process.env.DB_PORT || '3306',
    dialect: 'mariadb'
  }
}
