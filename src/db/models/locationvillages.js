'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class LocationVillages extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.LocationDistricts, {
        foreignKey: 'district_code',
        targetKey: 'code',
        as: 'district',
      })
    }
  };
  LocationVillages.init({
    code: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING(10),
    },
    district_code: {
      allowNull: false,
      type: DataTypes.STRING(7)
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING(255)
    },
    latitude: {
      type: DataTypes.TEXT
    },
    longitude: {
      type: DataTypes.TEXT
    }
  }, {
    sequelize,
    modelName: 'LocationVillages',
    tableName: 'location_villages',
  })
  return LocationVillages
}
