'use strict'
const {
  Model
} = require('sequelize')
const User = require('./user')
const Role = require('./masterrole')

module.exports = (sequelize, DataTypes) => {
  class UserRole extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }

  UserRole.init({
    userId: {
      type: DataTypes.INTEGER,
      field: 'user_id',
      allowNull: false,
      references: {
        model: User, // 'Movies' would also work
        key: 'id'
      }
    },
    roleId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'role_id',
      references: {
        model: Role, // 'Movies' would also work
        key: 'id'
      }
    },
    createdBy: {
      type: DataTypes.INTEGER,
    },
    updatedBy: {
      type: DataTypes.INTEGER,
    },
  }, {
    sequelize,
    modelName: 'UserRole',
    tableName: 'user_roles',
  })
  return UserRole
}
