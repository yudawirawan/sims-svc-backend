'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StudentExtracurricular extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.MasterLevel, {
        foreignKey: 'levelId',
        as: 'level',
      })
      this.belongsTo(models.MasterClass, {
        foreignKey: 'classId',
        as: 'class',
      })
      this.belongsTo(models.MasterStudent, {
        foreignKey: 'studentId',
        as: 'student',
      })
      this.belongsTo(models.MasterSubject, {
        foreignKey: 'subjectId1',
        as: 'subject1',
      })
      this.belongsTo(models.MasterSubject, {
        foreignKey: 'subjectId2',
        as: 'subject2',
      })
      this.belongsTo(models.MasterSubject, {
        foreignKey: 'subjectId3',
        as: 'subject3',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  StudentExtracurricular.init({
    departmentId: {
      field: 'department_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    levelId: {
      field: 'level_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    classId: {
      field: 'class_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    studentId: {
      field: 'student_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    subjectId1: {
      field: 'subject_id_1',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    subjectId2: {
      field: 'subject_id_2',
      type: DataTypes.INTEGER,
    },
    subjectId3: {
      field: 'subject_id_3',
      type: DataTypes.INTEGER
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    }
  }, {
    sequelize,
    modelName: 'StudentExtracurricular',
    tableName: 'student_extracurriculars',
  });
  return StudentExtracurricular;
};