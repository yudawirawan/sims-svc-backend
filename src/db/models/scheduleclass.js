'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ScheduleClass extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.MasterSchoolYear, {
        foreignKey: 'schoolYearId',
        as: 'schoolYear',
      })
      this.belongsTo(models.MasterLevel, {
        foreignKey: 'levelId',
        as: 'level',
      })
      this.belongsTo(models.MasterSubject, {
        foreignKey: 'subjectId',
        as: 'subject',
      })
      this.belongsTo(models.Teacher, {
        foreignKey: 'teacherId',
        as: 'teacher',
      })
      this.belongsTo(models.ScheduleStudyHours, {
        foreignKey: 'startStudyHourId',
        as: 'startStudyHour',
      })
      this.belongsTo(models.ScheduleStudyHours, {
        foreignKey: 'finishStudyHourId',
        as: 'finishStudyHour',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  }
  ScheduleClass.init({
    departmentId: {
      field: 'department_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    schoolYearId: {
      field: 'school_year_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    levelId: {
      field: 'level_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    subjectId: {
      field: 'subject_id',
      type: DataTypes.INTEGER,
    },
    teacherId: {
      field: 'teacher_id',
      type: DataTypes.INTEGER,
    },
    day: {
      type: DataTypes.STRING(10)
    },
    startStudyHourId: {
      field: 'start_study_hour_id',
      type: DataTypes.INTEGER,
    },
    finishStudyHourId: {
      field: 'finish_study_hour_id',
      type: DataTypes.INTEGER,
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
  }, {
    sequelize,
    tableName: 'schedule_classes',
    modelName: 'ScheduleClass',
  });
  return ScheduleClass;
};