'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class LocationCities extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.LocationProvincies, {
        foreignKey: 'province_code',
        targetKey: 'code',
        as: 'province',
      })
    }
  };
  LocationCities.init({
    code: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING(4),
    },
    province_code: {
      allowNull: false,
      type: DataTypes.STRING(2)
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING(255)
    },
    latitude: {
      type: DataTypes.TEXT
    },
    longitude: {
      type: DataTypes.TEXT
    }
  }, {
    sequelize,
    modelName: 'LocationCities',
    tableName: 'location_cities',
  })
  return LocationCities
}
