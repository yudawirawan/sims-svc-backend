'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ScheduleTeacherRecap extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.MasterSchoolYear, {
        foreignKey: 'schoolYearId',
        as: 'schoolYear',
      })
      this.belongsTo(models.Teacher, {
        foreignKey: 'teacherId',
        as: 'teacher',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  }
  ScheduleTeacherRecap.init({
    departmentId: {
      field: 'department_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    schoolYearId: {
      field: 'school_year_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    teacherId: {
      field: 'teacher_id',
      type: DataTypes.INTEGER,
    },
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
  }, {
    sequelize,
    tableName: 'schedule_teacher_recaps',
    modelName: 'ScheduleTeacherRecap',
  });
  return ScheduleTeacherRecap;
};