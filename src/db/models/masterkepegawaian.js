'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class MasterKepegawaian extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  MasterKepegawaian.init({
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    nama: {
      allowNull: false,
      type: DataTypes.STRING(200)
    },
    nuptk: {
      allowNull: true,
      type: DataTypes.STRING(16)
    },
    jk: {
      allowNull: false,
      type: DataTypes.CHAR(1)
    },
    tempatLahir: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    tanggalLahir: {
      allowNull: false,
      type: DataTypes.DATEONLY,
    },
    nip: {
      type: DataTypes.STRING(20),
    },
    statusKepegawaian: {
      allowNull: false,
      type: DataTypes.STRING(20)
    },
    jenisPtk: {
      allowNull: false,
      type: DataTypes.STRING(10)
    },
    agama: {
      allowNull: false,
      type: DataTypes.STRING(10)
    },
    alamatJalan: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    rt: {
      allowNull: true,
      type: DataTypes.CHAR(5)
    },
    rw: {
      allowNull: true,
      type: DataTypes.CHAR(5)
    },
    namaDusun: {
      allowNull: true,
      type: DataTypes.TEXT
    },
    desa: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    kecamatan: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    kodePos: {
      allowNull: true,
      type: DataTypes.STRING(5)
    },
    telepon: {
      allowNull: true,
      type: DataTypes.STRING(15)
    },
    hp: {
      allowNull: true,
      type: DataTypes.STRING(15)
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING(10),
      unique: true
    },
    tugasTambahan: {
      allowNull: true,
      type: DataTypes.TEXT,
    },
    skCpns: {
      type: DataTypes.STRING(30)
    },
    tanggalCpns: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      defaultValue: null,
    },
    skPengangkatan: {
      type: DataTypes.STRING(50)
    },
    tmtPengangkatan: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      defaultValue: null,
    },
    lembagaPengangkatan: {
      type: DataTypes.STRING(50)
    },
    pangkatGolongan: {
      type: DataTypes.STRING(10)
    },
    sumberGaji: {
      type: DataTypes.STRING(15)
    },
    namaIbuKandung: {
      type: DataTypes.STRING(100)
    },
    statusPerkawinan: {
      type: DataTypes.STRING(15)
    },
    namaSuamiIstri: {
      type: DataTypes.STRING(100)
    },
    nipSuamiIstri: {
      type: DataTypes.STRING(20)
    },
    pekerjaanSuamiIstri: {
      type: DataTypes.TEXT
    },
    tmtPns: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      defaultValue: null,
    },
    sudahLisensiKepalaSekolah: {
      type: DataTypes.STRING(5)
    },
    pernahDiklatKepengawasan: {
      type: DataTypes.STRING(5)
    },
    keahlianBraille: {
      type: DataTypes.STRING(5)
    },
    keahlianBahasaIsyarat: {
      type: DataTypes.STRING(5)
    },
    npwp: {
      type: DataTypes.STRING(15)
    },
    namaWajibPajak: {
      type: DataTypes.STRING(100)
    },
    kewarganegaraan: {
      type: DataTypes.STRING(3)
    },
    bank: {
      type: DataTypes.STRING(50)
    },
    nomorRekeningBank: {
      type: DataTypes.STRING(20)
    },
    rekeningAtasNama: {
      type: DataTypes.STRING(100)
    },
    nik: {
      unique: true,
      allowNull: false,
      type: DataTypes.STRING(16)
    },
    noKk: {
      type: DataTypes.STRING(16)
    },
    karpeg: {
      type: DataTypes.STRING(18)
    },
    karisKarsu: {
      type: DataTypes.STRING(15)
    },
    lintang: {
      type: DataTypes.TEXT
    },
    bujur: {
      type: DataTypes.TEXT
    },
    nuks: {
      type: DataTypes.STRING(30)
    },
    status: {
      type: DataTypes.INTEGER
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    modelName: 'MasterKepegawaian',
    tableName: 'master_kepegawaian'
  })
  return MasterKepegawaian
}
