'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class LocationDistricts extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.LocationCities, {
        foreignKey: 'city_code',
        targetKey: 'code',
        as: 'city',
      })
    }
  };
  LocationDistricts.init({
    code: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING(7),
    },
    city_code: {
      allowNull: false,
      type: DataTypes.STRING(4)
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING(200)
    },
    latitude: {
      type: DataTypes.TEXT
    },
    longitude: {
      type: DataTypes.TEXT
    }
  }, {
    sequelize,
    modelName: 'LocationDistricts',
    tableName: 'location_districts',
  })
  return LocationDistricts
}
