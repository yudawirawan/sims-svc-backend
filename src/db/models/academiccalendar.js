'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AcademicCalendar extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  }
  AcademicCalendar.init({
    color: {
      allowNull: false,
      type: DataTypes.STRING(8),
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING(100),
    },
    startDate: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    endDate: {
      allowNull: false,
      type: DataTypes.DATE
    },
  }, {
    sequelize,
    tableName: 'academic_calendars',
    modelName: 'AcademicCalendar',
  });
  return AcademicCalendar;
};