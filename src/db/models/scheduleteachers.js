'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class ScheduleTeachers extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Teacher, {
        foreignKey: 'teacherId',
        as: 'teacher',
      })
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.MasterSchoolYear, {
        foreignKey: 'schoolYearId',
        as: 'schoolYear',
      })
      this.belongsTo(models.ScheduleStudyHours, {
        foreignKey: 'studyHoursId',
        as: 'studyHour',
      })
      this.belongsTo(models.MasterLevel, {
        foreignKey: 'levelId',
        as: 'level',
      })
      this.belongsTo(models.MasterClass, {
        foreignKey: 'classId',
        as: 'class',
      })
      this.belongsTo(models.MasterSubject, {
        foreignKey: 'teacherSubjectId',
        as: 'teacherSubject',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  ScheduleTeachers.init({
    teacherId: {
      field: 'teacher_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    departmentId: {
      field: 'department_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    schoolYearId: {
      field: 'school_year_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    studyHoursId: {
      field: 'study_hours_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    levelId: {
      field: 'level_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    classId: {
      field: 'class_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    teacherSubjectId: {
      field: 'teacher_subject_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    day: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    status: {
      allowNull: false,
      type: DataTypes.NUMBER
    },
    description: {
      type: DataTypes.TEXT
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    }
  }, {
    sequelize,
    modelName: 'ScheduleTeachers',
    tableName: 'schedule_teachers',
  })
  return ScheduleTeachers
}
