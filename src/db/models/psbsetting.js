'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class PsbSetting extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
      this.belongsTo(models.PsbProcess, {
        foreignKey: 'processId',
        as: 'process',
      })
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.hasMany(models.PsbSettingExams, {
        as: 'exams',
        foreignKey: 'setting_id',
      })
    }
  }
  PsbSetting.init({
    departmentId: {
      field: 'department_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    processId: {
      field: 'process_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    mandatoryFeeCode: DataTypes.STRING(10),
    mandatoryFeeName: DataTypes.STRING(100),
    voluntaryFeeCode: DataTypes.STRING(10),
    voluntaryFeeName: DataTypes.STRING(100),
    status: {
      allowNull: false,
      type: DataTypes.NUMBER
    },
    createdBy: DataTypes.NUMBER,
    updatedBy: DataTypes.NUMBER
  }, {
    sequelize,
    tableName: 'psb_setting',
    modelName: 'PsbSetting',
  })
  return PsbSetting
}
