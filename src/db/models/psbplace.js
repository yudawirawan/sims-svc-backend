'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class PsbPlace extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.PsbProcess, {
        foreignKey: 'processId',
        as: 'psb_process',
      })
      this.belongsTo(models.PsbCandidateGroup, {
        foreignKey: 'namegroup_id',
        as: 'psb_group',
      })
      this.belongsTo(models.MasterGeneration, {
        foreignKey: 'generationId',
        as: 'generation',
      })
      /* Waiting merge issue #1 #2 #3 #5
      this.belongsTo(models.MasterLevel,{
        foreignKey: 'processId',
        as: 'level',
      })
      */
      /* Waiting merge issue #1 #2 #3 #5
      this.belongsTo(models.MasterClass,{
        foreignKey: 'processId',
        as: 'class',
      })
      */
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  PsbPlace.init({
    departmentId: {
      field: 'department_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    processId: {
      field: 'process_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    groupId: {
      field: 'namegroup_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    generationId: {
      field: 'generation_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    levelId: {
      field: 'level_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    classId: {
      field: 'class_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    modelName: 'PsbPlace',
    tableName: 'psb_places'
  })
  return PsbPlace
}
