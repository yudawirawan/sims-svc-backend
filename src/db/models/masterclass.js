'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class MasterClass extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.MasterLevel, {
        foreignKey: 'levelId',
        as: 'level',
      })
      this.belongsTo(models.MasterSchoolYear, {
        foreignKey: 'schoolYearId',
        as: 'schoolYear',
      })
      this.belongsTo(models.MasterKepegawaian, {
        foreignKey: 'kepegawaianId',
        as: 'kepegawaian',
      })
      this.belongsTo(models.MasterKepegawaian, {
        foreignKey: 'mentorId',
        as: 'mentor',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  }
  MasterClass.init({
    departmentId: {
      field: 'department_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    levelId: {
      field: 'level_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    schoolYearId: {
      field: 'school_year_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING(10),
    },
    kepegawaianId: {
      field: 'kepegawaian_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    mentorId: {
      field: 'mentor_id',
      type: DataTypes.INTEGER,
    },
    capacity: {
      type: DataTypes.INTEGER,
    },
    capacityFilled: {
      type: DataTypes.INTEGER,
    },
    description: {
      type: DataTypes.TEXT,
    },
    status: {
      allowNull: false,
      defaultValue: 1,
      type: DataTypes.NUMBER,
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    tableName: 'master_classes',
    modelName: 'MasterClass',
  })
  return MasterClass
}
