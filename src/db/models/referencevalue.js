'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class ReferenceValue extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }

  ReferenceValue.init({
    code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    value: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    label: DataTypes.STRING,
    createdBy: {
      type: DataTypes.INTEGER
    },
    updatedBy: {
      type: DataTypes.INTEGER
    },
  }, {
    sequelize,
    modelName: 'ReferenceValue',
    tableName: 'reference_value',
    indexes: [
      {
        unique: true,
        fields: ['code', 'value']
      }
    ]
  })
  return ReferenceValue
}
