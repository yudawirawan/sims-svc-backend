'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class MatpelGradingRule extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Teacher, {
        foreignKey: 'teacherId',
        as: 'teacher',
      })
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.MasterLevel, {
        foreignKey: 'levelId',
        as: 'level',
      })
      this.belongsTo(models.MatpelAssessmentAspect, {
        foreignKey: 'assessmentAspectId',
        as: 'assessmentAspect',
      })
      this.hasMany(models.MatpelGradingRuleScore, {
        as: 'scores',
        foreignKey: 'matpel_grading_rule_id',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  MatpelGradingRule.init({
    teacherId: {
      field: 'teacher_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    departmentId: {
      field: 'department_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    levelId: {
      field: 'level_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    assessmentAspectId: {
      field: 'assessment_aspect_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    tableName: 'matpel_grading_rules',
    modelName: 'MatpelGradingRule',
  })
  return MatpelGradingRule
}
