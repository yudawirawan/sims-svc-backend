'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class MasterGeneration extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  MasterGeneration.init({
    departmentId: {
      field: 'department_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    generation: {
      allowNull: false,
      type: DataTypes.STRING(10),
    },
    description: {
      type: DataTypes.TEXT,
    },
    status: {
      allowNull: false,
      type: DataTypes.NUMBER,
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    tableName: 'master_generations',
    modelName: 'MasterGeneration',
  })
  return MasterGeneration
}
