'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ClassesPromote extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterLevel, {
        foreignKey: 'oldLevelId',
        as: 'oldLevel',
      })
      this.belongsTo(models.MasterClass, {
        foreignKey: 'oldClassId',
        as: 'oldClass',
      })
      this.belongsTo(models.MasterStudent, {
        foreignKey: 'studentId',
        as: 'student',
      })
      this.belongsTo(models.MasterStudentReference, {
        foreignKey: 'studentId',
        as: 'studentReference',
      })
      this.belongsTo(models.MasterLevel, {
        foreignKey: 'newLevelId',
        as: 'newLevel',
      })
      this.belongsTo(models.MasterClass, {
        foreignKey: 'newClassId',
        as: 'newClass',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  }
  ClassesPromote.init({
    oldLevelId: {
      field: 'old_level_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    oldClassId: {
      field: 'old_class_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    studentId: {
      field: 'student_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    newLevelId: {
      field: 'new_level_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    newClassId: {
      field: 'new_class_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    createdBy: DataTypes.NUMBER,
    updatedBy: DataTypes.NUMBER
  }, {
    sequelize,
    tableName: 'classes_promote',
    modelName: 'ClassesPromote',
  });
  return ClassesPromote;
};