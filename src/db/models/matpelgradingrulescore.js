'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MatpelGradingRuleScore extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MatpelGradingRule, {
        foreignKey: 'matpelGradingRuleId',
        as: 'gradingRule',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  }
  MatpelGradingRuleScore.init({
    matpelGradingRuleId : {
      field : 'matpel_grading_rule_id',
      allowNull : false,
      type : DataTypes.INTEGER,
    },
    initialScore: {
      allowNull : false,
      type: DataTypes.INTEGER,
    },
    finalScore: {
      allowNull : false,
      type : DataTypes.INTEGER,
    },
    grade: {
      type : DataTypes.STRING(3)
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    tableName : 'matpel_grading_rule_scores',
    modelName: 'MatpelGradingRuleScore',
  });
  return MatpelGradingRuleScore;
};