'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MasterStudentReference extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.MasterLevel, {
        foreignKey: 'levelId',
        as: 'level',
      })
      this.belongsTo(models.MasterClass, {
        foreignKey: 'classId',
        as: 'class',
      })
      this.belongsTo(models.PsbProcess, {
        foreignKey: 'admissionProcessId',
        as: 'admissionProcess',
      })
      this.belongsTo(models.PsbCandidateGroup, {
        foreignKey: 'candidateGroupId',
        as: 'candidateGroup',
      })
      this.belongsTo(models.MasterStudent, {
        foreignKey: 'studentId',
        as: 'student',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  }
  MasterStudentReference.init({
    registerId: {
      type: DataTypes.STRING(10),
    },
    departmentId: {
      field: 'department_id',
      type : DataTypes.INTEGER,
    },
    levelId: {
      field: 'level_id',
      type : DataTypes.INTEGER,
    },
    classId: {
      field: 'class_id',
      type : DataTypes.INTEGER,
    },
    admissionProcessId: {
      field: 'admission_process_id',
      type: DataTypes.INTEGER,
    },
    candidateGroupId: {
      field: 'candidate_group_id',
      type : DataTypes.INTEGER,
    },
    studentId: {
      field: 'student_id',
      type: DataTypes.INTEGER,
    },
    status: {
      allowNull : false,
      type : DataTypes.INTEGER,
      defaultValue: 1,
    },
    iuranWajib: {
      type : DataTypes.INTEGER,
    },
    iuranSukarela: {
      type : DataTypes.INTEGER,
    },
    graduated: {
      type : DataTypes.BOOLEAN,
    },
    graduatedYear: {
      type : DataTypes.STRING(10),
    },
  }, {
    sequelize,
    tableName: 'master_student_references',
    modelName: 'MasterStudentReference',
  });
  return MasterStudentReference;
};