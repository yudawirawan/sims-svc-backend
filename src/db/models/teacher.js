'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Teacher extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'department_id',
        as: 'department',
      })
      this.belongsTo(models.MasterSubject, {
        foreignKey: 'subject_id',
        as: 'subject',
      })
      this.belongsTo(models.MasterKepegawaian, {
        foreignKey: 'kepegawaian_id',
        as: 'kepegawaian',
      })
      this.belongsTo(models.TeacherStatus, {
        foreignKey: 'status_id',
        as: 'teacherStatus',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  Teacher.init({
    departmentId: {
      field: 'department_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    subjectId: {
      field: 'subject_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    kepegawaianId: {
      field: 'kepegawaian_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    statusId: {
      field: 'status_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    tableName: 'teachers',
    modelName: 'Teacher',
  })
  return Teacher
}
