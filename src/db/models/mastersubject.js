'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class MasterSubject extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  MasterSubject.init({
    departmentId: {
      allowNull: false,
      field: 'department_id',
      type: DataTypes.INTEGER,
    },
    code: {
      allowNull: false,
      type: DataTypes.STRING(6),
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING(30),
    },
    lessonHours: {
      type: DataTypes.INTEGER,
    },
    curriculumStatus: {
      type: DataTypes.STRING(50),
    },
    status: {
      allowNull: false,
      type: DataTypes.NUMBER,
    },
    description: {
      type: DataTypes.TEXT
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    tableName: 'master_subjects',
    modelName: 'MasterSubject',
  })
  return MasterSubject
}
