'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class PsbCandidateScore extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.PsbCandidate, {
        foreignKey: 'candidateId',
        as: 'candidate',
      })
      this.belongsTo(models.PsbSettingExams, {
        foreignKey: 'examId',
        as: 'candidateExam',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  PsbCandidateScore.init({
    candidateId: {
      type: DataTypes.INTEGER,
      field: 'candidate_id',
      allowNull: false,
    },
    examId: {
      type: DataTypes.INTEGER,
      field: 'exam_id',
      allowNull: false,
    },
    score: {
      allowNull: false,
      type: DataTypes.FLOAT
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    tableName: 'psb_candidate_scores',
    modelName: 'PsbCandidateScore',
  })
  return PsbCandidateScore
}
