'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class MasterStudent extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.MasterStudentReference, {
        foreignKey: 'student_id',
        as: 'studentReference',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  MasterStudent.init({
    nama: {
      allowNull: false,
      type: DataTypes.STRING(100),
    },
    nipd: {
      type: DataTypes.STRING(20),
    },
    jk: {
      allowNull: false,
      type: DataTypes.CHAR(1),
    },
    nisn: {
      allowNull: false,
      type: DataTypes.STRING(16),
    },
    tempatLahir: {
      type: DataTypes.TEXT,
    },
    tanggalLahir: {
      allowNull: false,
      type: DataTypes.DATEONLY,
    },
    nik: {
      allowNull: false,
      type: DataTypes.STRING(16),
    },
    agama: {
      allowNull: false,
      type: DataTypes.STRING(10),
    },
    alamat: {
      type: DataTypes.TEXT,
    },
    rt: {
      type: DataTypes.STRING(5),
    },
    rw: {
      type: DataTypes.STRING(5),
    },
    dusun: {
      type: DataTypes.TEXT,
    },
    kelurahan: {
      allowNull: false,
      type: DataTypes.TEXT,
    },
    kecamatan: {
      allowNull: false,
      type: DataTypes.TEXT,
    },
    kodepos: {
      allowNull: false,
      type: DataTypes.STRING(5),
    },
    jenisTinggal: {
      type: DataTypes.STRING(30),
    },
    alatTransportasi: {
      type: DataTypes.STRING(30),
    },
    telepon: {
      allowNull: false,
      type: DataTypes.STRING(50),
    },
    hp: {
      allowNull: false,
      type: DataTypes.STRING(15),
    },
    email: {
      type: DataTypes.STRING(100),
    },
    skhun: {
      allowNull: false,
      type: DataTypes.STRING(25),
    },
    penerimaKps: {
      allowNull: false,
      type: DataTypes.STRING(5),
    },
    noKps: {
      type: DataTypes.STRING(20),
    },
    // --data ayah--
    namaAyah: {
      type: DataTypes.STRING(100),
    },
    tahunLahirAyah: {
      type: DataTypes.STRING(5),
    },
    jenjangPendidikanAyah: {
      type: DataTypes.STRING(20),
    },
    pekerjaanAyah: {
      type: DataTypes.STRING(20),
    },
    penghasilanAyah: {
      type: DataTypes.STRING(20),
    },
    nikAyah: {
      type: DataTypes.STRING(16),
    },
    // --end data ayah--
    // --data ibu--
    namaIbu: {
      type: DataTypes.STRING(100),
    },
    tahunLahirIbu: {
      type: DataTypes.STRING(5),
    },
    jenjangPendidikanIbu: {
      type: DataTypes.STRING(20),
    },
    pekerjaanIbu: {
      type: DataTypes.STRING(20),
    },
    penghasilanIbu: {
      type: DataTypes.STRING(20),
    },
    nikIbu: {
      type: DataTypes.STRING(16),
    },
    // --end data ibu--
    // --data wali--
    namaWali: {
      type: DataTypes.STRING(100),
    },
    tahunLahirWali: {
      type: DataTypes.STRING(5),
    },
    jenjangPendidikanWali: {
      type: DataTypes.STRING(20),
    },
    pekerjaanWali: {
      type: DataTypes.STRING(20),
    },
    penghasilanWali: {
      type: DataTypes.STRING(20),
    },
    nikWali: {
      type: DataTypes.STRING(16),
    },
    // --end data wali--
    rombelSaatIni: {
      type: DataTypes.STRING(20),
    },
    noPesertaUjianNasional: {
      type: DataTypes.STRING(30),
    },
    noSeriIjazah: {
      allowNull: false,
      type: DataTypes.STRING(30),
    },
    penerimaKip: {
      allowNull: false,
      type: DataTypes.STRING(5),
    },
    nomorKip: {
      type: DataTypes.STRING(20),
    },
    namaDiKip: {
      type: DataTypes.STRING(100),
    },
    nomorKks: {
      type: DataTypes.STRING(20),
    },
    noRegistrasiAktaLahir: {
      type: DataTypes.STRING(30),
    },
    bank: {
      type: DataTypes.STRING(50),
    },
    nomorRekeningBank: {
      type: DataTypes.STRING(20),
    },
    rekeningAtasNama: {
      type: DataTypes.STRING(100),
    },
    layakPip: {
      allowNull: false,
      type: DataTypes.STRING(5),
    },
    alasanLayakPip: {
      type: DataTypes.STRING(50),
    },
    kebutuhanKhusus: {
      type: DataTypes.STRING(25),
    },
    sekolahAsal: {
      type: DataTypes.STRING(100),
    },
    anakKe: {
      type: DataTypes.STRING(5),
    },
    lintang: {
      type: DataTypes.TEXT,
    },
    bujur: {
      type: DataTypes.TEXT,
    },
    noKk: {
      allowNull: false,
      type: DataTypes.STRING(16),
    },
    beratBadan: {
      type: DataTypes.STRING(3),
    },
    tinggiBadan: {
      type: DataTypes.STRING(3),
    },
    lingkarKepala: {
      type: DataTypes.STRING(3),
    },
    jumlahSaudaraKandung: {
      type: DataTypes.STRING(3),
    },
    jarakRumahSekolah: {
      type: DataTypes.STRING(9),
    },
    createdBy: {
      type: DataTypes.NUMBER
    },
    updatedBy: {
      type: DataTypes.NUMBER
    },
  }, {
    sequelize,
    modelName: 'MasterStudent',
    tableName: 'master_students'
  })
  return MasterStudent
}
