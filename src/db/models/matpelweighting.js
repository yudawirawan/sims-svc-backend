'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class MatpelWeighting extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Teacher, {
        foreignKey: 'teacherId',
        as: 'teacher',
      })
      this.belongsTo(models.MatpelTestType, {
        foreignKey: 'testTypeId',
        as: 'testType',
      })
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.MasterLevel, {
        foreignKey: 'levelId',
        as: 'level',
      })
      this.belongsTo(models.MatpelAssessmentAspect, {
        foreignKey: 'assessmentAspectId',
        as: 'assessmentAspect',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  MatpelWeighting.init({
    teacherId: {
      allowNull: false,
      field: 'teacher_id',
      type: DataTypes.INTEGER,
    },
    testTypeId: {
      allowNull: false,
      field: 'test_type_id',
      type: DataTypes.INTEGER,
    },
    departmentId: {
      allowNull: false,
      field: 'department_id',
      type: DataTypes.INTEGER,
    },
    levelId: {
      allowNull: false,
      field: 'level_id',
      type: DataTypes.INTEGER,
    },
    assessmentAspectId: {
      allowNull: false,
      field: 'assessment_aspect_id',
      type: DataTypes.INTEGER,
    },
    weight: {
      allowNull: false,
      type: DataTypes.STRING(20),
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    tableName: 'matpel_weightings',
    modelName: 'MatpelWeighting',
  })
  return MatpelWeighting
}
