'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class PsbSettingExams extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.PsbSetting, {
        foreignKey: 'settingId',
        as: 'settings',
      })
    }
  }
  PsbSettingExams.init({
    settingId: {
      field: 'setting_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    code: DataTypes.STRING(8),
    name: DataTypes.STRING(50),
    status: {
      allowNull: false,
      type: DataTypes.NUMBER
    },
    createdBy: DataTypes.NUMBER,
    updatedBy: DataTypes.NUMBER
  }, {
    sequelize,
    tableName: 'psb_setting_exams',
    modelName: 'PsbSettingExams',
  })
  return PsbSettingExams
}
