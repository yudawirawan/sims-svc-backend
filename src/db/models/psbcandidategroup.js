'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class PsbCandidateGroup extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.PsbProcess, {
        foreignKey: 'processId',
        as: 'psbProcess',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  PsbCandidateGroup.init({
    departmentId: {
      field: 'department_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    processId: {
      field: 'process_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    groupName: {
      allowNull: false,
      type: DataTypes.STRING(20)
    },
    capacity: {
      type: DataTypes.INTEGER,
    },
    description: {
      type: DataTypes.TEXT,
    },
    status: {
      allowNull: false,
      type: DataTypes.NUMBER,
    },
    startDate: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.DATE,
    },
    endDate: {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.DATE,
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    modelName: 'PsbCandidateGroup',
    tableName: 'psb_candidate_groups',
  })
  return PsbCandidateGroup
}
