'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class MatpelLessonPlan extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.MasterLevel, {
        foreignKey: 'levelId',
        as: 'level',
      })
      this.belongsTo(models.MasterSemester, {
        foreignKey: 'semesterId',
        as: 'semester',
      })
      this.belongsTo(models.MasterSubject, {
        foreignKey: 'subjectId',
        as: 'subject',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  MatpelLessonPlan.init({
    departmentId: {
      allowNull: false,
      field: 'department_id',
      type: DataTypes.INTEGER,
    },
    levelId: {
      allowNull: false,
      field: 'level_id',
      type: DataTypes.INTEGER,
    },
    semesterId: {
      allowNull: false,
      field: 'semester_id',
      type: DataTypes.INTEGER,
    },
    subjectId: {
      allowNull: false,
      field: 'subject_id',
      type: DataTypes.INTEGER,
    },
    code: {
      allowNull: false,
      type: DataTypes.STRING(30),
    },
    material: {
      type: DataTypes.TEXT,
    },
    description: {
      type: DataTypes.TEXT,
    },
    status: {
      type: DataTypes.NUMBER
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    tableName: 'matpel_lesson_plans',
    modelName: 'MatpelLessonPlan',
  })
  return MatpelLessonPlan
}
