'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class ConfigSchool extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  ConfigSchool.init({
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    logo: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    address: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    telp1: {
      type: DataTypes.STRING(15),
      allowNull: false,
    },
    telp2: {
      type: DataTypes.STRING(15),
    },
    hp1: {
      type: DataTypes.STRING(15),
      allowNull: false,
    },
    hp2: {
      type: DataTypes.STRING(15),
    },
    website: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    createdBy: DataTypes.NUMBER,
    updatedBy: DataTypes.NUMBER
  }, {
    sequelize,
    tableName: 'config_school',
    modelName: 'ConfigSchool',
  })
  return ConfigSchool
}
