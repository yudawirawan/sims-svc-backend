'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class MatpelTestType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.MasterSubject, {
        foreignKey: 'subjectId',
        as: 'subject',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  MatpelTestType.init({
    departmentId: {
      field: 'department_id',
      allowNull: false,
      type: DataTypes.INTEGER
    },
    subjectId: {
      field: 'subject_id',
      allowNull: false,
      type: DataTypes.INTEGER
    },
    testType: {
      allowNull: false,
      type: DataTypes.STRING(50)
    },
    abbreviation: {
      allowNull: false,
      type: DataTypes.STRING(10)
    },
    status: {
      allowNull: false,
      type: DataTypes.NUMBER
    },
    description: {
      type: DataTypes.TEXT
    },
    createdBy: {
      type: DataTypes.NUMBER
    },
    updatedBy: {
      type: DataTypes.NUMBER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    tableName: 'matpel_test_types',
    modelName: 'MatpelTestType',
  })
  return MatpelTestType
}
