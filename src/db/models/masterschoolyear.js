'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class MasterSchoolYear extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  MasterSchoolYear.init({
    departmentId: {
      field: 'department_id',
      type: DataTypes.INTEGER,
    },
    schoolYear: {
      allowNull: false,
      type: DataTypes.STRING(20),
    },
    startDate: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    endDate: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    description: {
      type: DataTypes.TEXT,
    },
    status: {
      allowNull: false,
      type: DataTypes.NUMBER
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    tableName: 'master_school_years',
    modelName: 'MasterSchoolYear',
  })
  return MasterSchoolYear
}
