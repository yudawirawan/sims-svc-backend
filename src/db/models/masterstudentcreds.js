'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MasterStudentCreds extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.MasterDepartment, {
        foreignKey: 'departmentId',
        as: 'department',
      })
      this.belongsTo(models.MasterLevel, {
        foreignKey: 'levelId',
        as: 'level',
      })
      this.belongsTo(models.MasterClass, {
        foreignKey: 'classId',
        as: 'class',
      })
      this.belongsTo(models.MasterStudent, {
        foreignKey: 'studentId',
        as: 'student',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  }
  MasterStudentCreds.init({
    departmentId: {
      field: 'department_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    levelId: {
      field: 'level_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    classId: {
      field: 'class_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    studentId: {
      field: 'student_id',
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    studentPin: {
      allowNull: false,
      type: DataTypes.CHAR(10),
    },
    parentPin: {
      allowNull: false,
      type: DataTypes.CHAR(10)
    },
  }, {
    sequelize,
    tableName: 'master_student_creds',
    modelName: 'MasterStudentCreds',
  });
  return MasterStudentCreds;
};