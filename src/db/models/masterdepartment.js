'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class MasterDepartment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.MasterKepegawaian, {
        foreignKey: 'kepegawaianId',
        as: 'kepegawaian',
      })
      this.belongsTo(models.User, {
        foreignKey: 'createdBy',
        as: 'userCreatedBy',
      })
      this.belongsTo(models.User, {
        foreignKey: 'updatedBy',
        as: 'userUpdatedBy',
      })
    }
  };
  MasterDepartment.init({
    name: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING(12),
    },
    kepegawaianId: {
      field: 'kepegawaian_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
    },
    status: {
      type: DataTypes.NUMBER,
      allowNull: false,
    },
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    tableName: 'master_departments',
    modelName: 'MasterDepartment',
  })

  return MasterDepartment
}
