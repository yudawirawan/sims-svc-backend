'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class MasterRole extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsToMany(models.User, {
        through: models.UserRole,
        as: 'users',
        foreignKey: 'role_id',
      })
    }
  }

  MasterRole.init({
    name: {
      type: DataTypes.STRING(15),
      allowNull: false,
      unique: true,
    },
    status: DataTypes.BOOLEAN,
    createdBy: {
      type: DataTypes.NUMBER,
    },
    updatedBy: {
      type: DataTypes.NUMBER,
    },
  }, {
    sequelize,
    tableName: 'master_roles',
    modelName: 'MasterRole',
  })
  return MasterRole
}
