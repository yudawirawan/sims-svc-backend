'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class LocationProvincies extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  LocationProvincies.init({
    code: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING(2),
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING(150),
    },
    latitude: {
      type: DataTypes.TEXT
    },
    longitude: {
      type: DataTypes.TEXT
    }
  }, {
    sequelize,
    modelName: 'LocationProvincies',
    tableName: 'location_provincies',
  })
  return LocationProvincies
}
