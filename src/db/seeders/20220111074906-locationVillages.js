'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const fs = require('fs')
    const path = require('path')

    const csvFolder = path.join(__dirname, 'csv')
    const csvVillages = path.join(__dirname, 'csv/villages/')

    let data = []
    const files = fs.readdirSync(csvFolder + '/villages/')

    for (const file of files) {
      const temp = fs.readFileSync(csvVillages + file)
        .toString()
        .split('\n')
        .map(e => e.trim())
        .map(e => e.split(',').map(e => e.trim()))
      data = data.concat(temp)
    }

    const objectData = []
    const length = data.length
    const headers = ['code', 'district_code', 'name', 'latitude', 'longitude']

    for (let i = 0; i < length - 1; i++) {
      const tmp = {}
      for (const [x, header] of headers.entries()) {
        tmp[header] = data[i][x]
      }
      if (tmp.district_code) {
        objectData.push(tmp)
      }
    }

    for (const tempData of objectData) {
      tempData.createdAt = Sequelize.literal('CURRENT_TIMESTAMP')
      tempData.updatedAt = Sequelize.literal('CURRENT_TIMESTAMP')
    }
    await queryInterface.bulkInsert('location_villages', objectData)
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('location_villages')
  }
}
