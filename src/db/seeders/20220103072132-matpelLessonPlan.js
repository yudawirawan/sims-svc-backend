'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('matpel_lesson_plans', [
      {
        department_id: 1,
        level_id: 1,
        semester_id: 1,
        subject_id: 1,
        code: 'TIK1001',
        material: 'Tren Teknologi Informasi Sekarang',
        description: '1. Standar kompetensi, 2. Kompetensi Dasar, 3. Indikator Pencapaian Hasil Belajar',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        level_id: 1,
        semester_id: 1,
        subject_id: 1,
        code: 'AGM02',
        material: 'Perilaku Terpuji',
        description: '1. Standar kompetensi, 2. Kompetensi Dasar, 3. Indikator Pencapaian Hasil Belajar',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 2,
        level_id: 2,
        semester_id: 2,
        subject_id: 2,
        code: 'BIO1233',
        material: 'Anatomi Tubuh',
        description: '1. Standar kompetensi, 2. Kompetensi Dasar, 3. Indikator Pencapaian Hasil Belajar',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 3,
        level_id: 3,
        semester_id: 2,
        subject_id: 3,
        code: 'RPP 21',
        material: null,
        description: null,
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('matpel_lesson_plans')
  }
}
