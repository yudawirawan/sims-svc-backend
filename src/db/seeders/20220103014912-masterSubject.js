'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('master_subjects', [
      {
        department_id: 1,
        code: 'BIO',
        name: 'Biologi',
        lessonHours: 2,
        status: 1,
        curriculumStatus: 'Matpel bidang studi wajib',
        description: 'Pelajaran wajib',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 2,
        code: 'GEO',
        name: 'Geografi',
        lessonHours: 2,
        status: 1,
        curriculumStatus: 'Matpel Peminatan',
        description: 'Pelajaran wajib',
        createdBy: 2,
        updatedBy: 2,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 2,
        code: 'ENG',
        name: 'Bahasa Inggris',
        lessonHours: 2,
        status: 1,
        curriculumStatus: 'Matpel bidang studi wajib',
        description: 'Pelajaran wajib',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 3,
        code: 'MAT',
        name: 'Matematika',
        lessonHours: null,
        status: 1,
        curriculumStatus: 'Matpel bidang studi wajib (Tambahan)',
        description: '',
        createdBy: 2,
        updatedBy: 2,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 3,
        code: 'FIS',
        name: 'Fisika',
        lessonHours: null,
        status: 1,
        curriculumStatus: 'Matpel kelompok A (Umum)',
        description: '',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        code: 'FUT',
        name: 'Futsal',
        lessonHours: 1,
        status: 1,
        curriculumStatus: 'Ekstrakulikuler',
        description: null,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        code: 'RM',
        name: 'Remaja Masjid',
        lessonHours: 1,
        status: 1,
        curriculumStatus: 'Ekstrakulikuler',
        description: 'Pengurus masjid',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 3,
        code: 'KRIS',
        name: 'Kekristenan',
        lessonHours: 1,
        status: 1,
        curriculumStatus: 'Ekstrakulikuler',
        description: 'Perkumpulan siswa kristen',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('master_subjects')
  }
}
