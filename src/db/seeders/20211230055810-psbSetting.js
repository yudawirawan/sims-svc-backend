'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('psb_setting', [
      {
        department_id: 1,
        process_id: 1,
        mandatoryFeeCode: 'SPP',
        mandatoryFeeName: 'Sumbangan Wajib',
        voluntaryFeeCode: 'SBS',
        voluntaryFeeName: 'Sumbangan Sukarela',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 2,
        process_id: 1,
        mandatoryFeeCode: 'SPX',
        mandatoryFeeName: 'Sumbangan Sunnah',
        voluntaryFeeCode: 'SBF',
        voluntaryFeeName: 'Sumbangan Sedekah',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('psb_setting')
  }
}
