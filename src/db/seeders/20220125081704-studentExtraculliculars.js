'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('student_extracurriculars', [
      {
        department_id: 1,
        level_id: 1,
        class_id: 1,
        student_id: 1,
        subject_id_1: 6,
        subject_id_2: null,
        subject_id_3: null,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        level_id: 1,
        class_id: 1,
        student_id: 2,
        subject_id_1: 7,
        subject_id_2: 8,
        subject_id_3: null,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 3,
        level_id: 1,
        class_id: 1,
        student_id: 3,
        subject_id_1: 6,
        subject_id_2: 7,
        subject_id_3: 8,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.dropTable('student_extracurriculars')
  }
};
