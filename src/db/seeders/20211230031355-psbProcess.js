'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('psb_processes', [
      {
        department_id: 1,
        name: 'Prosess Penerimaan Siswa Baru SMA',
        code: 'PPSB-SMA',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        name: 'Prosess Penerimaan Siswa Baru SD',
        code: 'PPSB-SD',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        name: 'Prosess Penerimaan Siswa Baru SMP',
        code: 'PPSB-SMP',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('psb_processes')
  }
}
