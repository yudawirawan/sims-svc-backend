'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('master_classes', [
      {
        department_id: 1,
        level_id: 1,
        school_year_id: 1,
        name: 'X IPA 1',
        kepegawaian_id: 1,
        mentor_id: 2,
        capacity: 30,
        capacityFilled: 25,
        description: 'Kelas IPA',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        level_id: 1,
        school_year_id: 2,
        name: 'X IPS 1',
        kepegawaian_id: 2,
        mentor_id: null,
        capacity: null,
        capacityFilled: null,
        description: 'Kelas IPS',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        level_id: 3,
        school_year_id: 2,
        name: 'XII IPS 4',
        kepegawaian_id: 1,
        mentor_id: 2,
        description: 'Kelas 12 IPS',
        capacity: 40,
        capacityFilled: 37,
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 2,
        level_id: 4,
        school_year_id: 1,
        name: 'VII A',
        kepegawaian_id: 1,
        mentor_id: 2,
        capacity: 50,
        capacityFilled: 25,
        description: 'Kelas 7 SMPN 30 Bandung',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 2,
        level_id: 6,
        school_year_id: 1,
        name: 'IX C',
        kepegawaian_id: 1,
        mentor_id: 2,
        capacity: 50,
        capacityFilled: 25,
        description: '',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 3,
        level_id: 7,
        school_year_id: 1,
        name: '1 F',
        kepegawaian_id: 1,
        mentor_id: 2,
        capacity: 50,
        capacityFilled: 25,
        description: 'SD Negeri Margahayu',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 3,
        level_id: 7,
        school_year_id: 1,
        name: '1 Y',
        kepegawaian_id: 1,
        mentor_id: null,
        capacity: 0,
        capacityFilled: 0,
        description: 'SD Negeri Margahayu',
        status: 0,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('master_classes')
  }
}
