'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('master_student_references', [
      {
        registerId: '2022-0001',
        department_id: 1,
        level_id: 1,
        class_id: 1,
        admission_process_id: 1,
        candidate_group_id: 1,
        student_id: 1,
        iuranWajib: 1000000,
        iuranSukarela: 200000,
        status: 1,
        graduated: 0,
        graduatedYear: null,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        registerId: '2022-0002',
        department_id: 1,
        level_id: 1,
        class_id: 1,
        admission_process_id: 1,
        candidate_group_id: 1,
        student_id: 2,
        iuranWajib: 1000000,
        iuranSukarela: 150000,
        status: 1,
        graduated: 1,
        graduatedYear: '2022',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        registerId: '2022-0003',
        department_id: 1,
        level_id: 2,
        class_id: 2,
        admission_process_id: 1,
        candidate_group_id: 1,
        student_id: 3,
        iuranWajib: 1000000,
        iuranSukarela: 100000,
        status: 1,
        graduated: 0,
        graduatedYear: null,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  await queryInterface.dropTable('master_student_references')
  }
};
