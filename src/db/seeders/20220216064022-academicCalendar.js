'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('academic_calendars', [
      {
        color: 'FFFF00',
        name: 'Cuti Bersama',
        startDate: '2022-02-11 00:00:00',
        endDate: '2022-02-13 12:59:59',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        color: 'FF0000',
        name: 'Libur Tahun Baru',
        startDate: '2021-12-24 00:00:00',
        endDate: '2022-01-3 23:59:59',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        color: '0000FF',
        name: 'Libur Nasional',
        startDate: '2022-02-02 00:00:00',
        endDate: '2022-02-03 12:59:59',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.dropTable('academic_calendars')
  }
};
