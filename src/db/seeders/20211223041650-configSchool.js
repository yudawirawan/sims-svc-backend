'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('config_school', [
      {
        logo: null,
        name: 'Dian Digital School',
        address: 'Jl.Neptunus Timur IV A 27 No 10, Margayu Raya, Bandung',
        telp1: '+62 22-875-13118',
        telp2: '+62 22-751-3012',
        hp1: '+628-1721-5496',
        hp2: '+628-1220-8717-67',
        website: 'dianglobaltech.co.id',
        email: 'info@dianglobaltech.co.id',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('config_school')
  }
}
