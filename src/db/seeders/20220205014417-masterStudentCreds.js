'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('master_student_creds', [
      {
        department_id: 1,
        level_id: 1,
        class_id: 1,
        student_id: 1,
        studentPin: 'siswa111',
        parentPin: 'ortu111',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        level_id: 1,
        class_id: 1,
        student_id: 2,
        studentPin: 'siswa222',
        parentPin: 'ortu222',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        level_id: 1,
        class_id: 1,
        student_id: 3,
        studentPin: 'siswa333',
        parentPin: 'ortu333',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.dropTable('master_student_creds')
  }
};
