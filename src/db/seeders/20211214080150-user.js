'use strict'
const bcrypt = require('bcrypt')

module.exports = {
  up: async(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('users', [
      {
        id: 1,
        username: 'superadmin',
        password: await bcrypt.hash('superadmin', 10),
        email: 'superadmin-sims@dianglobaltect.co.id',
        createdBy: 'system',
        updatedBy: 'system',
        fullName: 'Admin Suradmin',
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 2,
        username: 'admin',
        password: await bcrypt.hash('admin', 10),
        email: 'admin@sekolah.com',
        createdBy: 'system',
        updatedBy: 'system',
        fullName: 'Admin Suradmin',
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 3,
        username: 'guru',
        password: await bcrypt.hash('guru', 10),
        email: 'guru@sekolah.com',
        fullName: 'Guru',
        createdBy: 'system',
        updatedBy: 'system',
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 4,
        username: 'pegawai',
        password: await bcrypt.hash('pegawai', 10),
        email: 'pegawai@sekolah.com',
        fullName: 'Pegawai',
        createdBy: 'system',
        updatedBy: 'system',
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 5,
        username: 'siswa',
        email: 'siswa@sekolah.com',
        fullName: 'Alex Ibrahim',
        createdBy: 'system',
        updatedBy: 'system',
        password: await bcrypt.hash('siswa', 10),
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 6,
        username: 'siswa1',
        email: 'siswa1@sekolah.com',
        fullName: 'Januar Sibolga',
        createdBy: 'system',
        updatedBy: 'system',
        password: await bcrypt.hash('siswa', 10),
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 7,
        username: 'siswa2',
        email: 'siswa2@sekolah.com',
        fullName: 'Teresa William',
        createdBy: 'system',
        updatedBy: 'system',
        password: await bcrypt.hash('siswa', 10),
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 8,
        username: 'siswa3',
        email: 'siswa3@sekolah.com',
        fullName: 'Stepanie Simanjuntak',
        createdBy: 'system',
        updatedBy: 'system',
        password: await bcrypt.hash('siswa', 10),
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 9,
        username: 'siswa4',
        email: 'siswa4@sekolah.com',
        fullName: 'Beny Almanik',
        createdBy: 'system',
        updatedBy: 'system',
        password: await bcrypt.hash('siswa', 10),
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 10,
        username: 'siswa5',
        email: 'siswa5@sekolah.com',
        fullName: 'Beny Almanik',
        createdBy: 'system',
        updatedBy: 'system',
        password: await bcrypt.hash('siswa', 10),
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 11,
        username: 'siswa6',
        email: 'siswa6@sekolah.com',
        fullName: 'Alejandro Sundoro',
        createdBy: 'system',
        updatedBy: 'system',
        password: await bcrypt.hash('siswa', 10),
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 12,
        username: 'siswa7',
        email: 'siswa7@sekolah.com',
        fullName: 'Risa Sulistra',
        createdBy: 'system',
        updatedBy: 'system',
        password: await bcrypt.hash('siswa', 10),
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 13,
        username: 'siswa8',
        email: 'siswa8@sekolah.com',
        fullName: 'Ben Aflect',
        createdBy: 'system',
        updatedBy: 'system',
        password: await bcrypt.hash('siswa', 10),
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 14,
        username: 'siswa9',
        email: 'siswa9@sekolah.com',
        fullName: 'Lina Inverse',
        createdBy: 'system',
        updatedBy: 'system',
        password: await bcrypt.hash('siswa', 10),
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        id: 15,
        username: 'siswa10',
        email: 'siswa10@sekolah.com',
        fullName: 'Alfonso Alberque',
        createdBy: 'system',
        updatedBy: 'system',
        password: await bcrypt.hash('siswa', 10),
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      }
    ], {})
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('users')
  }
}
