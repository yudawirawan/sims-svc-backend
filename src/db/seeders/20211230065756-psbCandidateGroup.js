'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('psb_candidate_groups', [
      {
        department_id: 1,
        process_id: 1,
        groupName: 'Kelompok 1',
        capacity: 10,
        description: null,
        status: 1,
        startDate: '2022-01-01',
        endDate: '2022-12-12',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 2,
        process_id: 2,
        groupName: 'Kelompok 2',
        capacity: 20,
        description: 'Kelompok MPLS',
        status: 0,
        startDate: '2022-01-01',
        endDate: '2022-12-12',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        process_id: 1,
        groupName: 'Kelompok 3',
        capacity: 5,
        description: null,
        status: 1,
        startDate: '2022-01-01',
        endDate: '2022-12-12',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('psb_candidate_groups')
  }
}
