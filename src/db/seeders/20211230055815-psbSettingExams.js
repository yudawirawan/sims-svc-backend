'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('psb_setting_exams', [
      {
        setting_id: 1,
        name: 'Pendidikan Agama',
        code: 'AGAMA',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        setting_id: 1,
        name: 'Pendidikan Kewarganegaraan',
        code: 'KWN',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        setting_id: 1,
        name: 'Bahasa Indonesia',
        code: 'BI',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        setting_id: 1,
        name: 'Matematika',
        code: 'MAT',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        setting_id: 1,
        name: 'Ilmu Pengetahuan Alam',
        code: 'IPA',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        setting_id: 1,
        name: 'Ilmu Pengetahuan Sosial',
        code: 'IPS',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        setting_id: 1,
        name: 'Bahasa Inggris',
        code: 'BING',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        setting_id: 1,
        name: 'Seni Budaya',
        code: 'SB',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        setting_id: 1,
        name: 'Pendidikan Jasmani, Olah Raga, dan Kesehatan',
        code: 'PENJAS',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        setting_id: 1,
        name: 'Prakarya',
        code: 'PRK',
        status: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('psb_setting_exams')
  }
}
