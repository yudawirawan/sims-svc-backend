'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('schedule_study_hours', [
      {
        department_id: 1,
        hour: '1',
        startTime: '07:00:00',
        finishTime: '09:00:00',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        hour: '2',
        startTime: '10:00:00',
        finishTime: '11:00:00',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        hour: '3',
        startTime: '12:00:00',
        finishTime: '13:00:00',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        hour: '4',
        startTime: '14:00:00',
        finishTime: '15:00:00',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 2,
        hour: '1',
        startTime: '07:00:00',
        finishTime: '09:00:00',
        createdBy: 2,
        updatedBy: 2,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 2,
        hour: '2',
        startTime: '10:00:00',
        finishTime: '11:00:00',
        createdBy: 2,
        updatedBy: 2,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 2,
        hour: '3',
        startTime: '12:00:00',
        finishTime: '13:00:00',
        createdBy: 2,
        updatedBy: 2,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 2,
        hour: '4',
        startTime: '14:00:00',
        finishTime: '15:00:00',
        createdBy: 2,
        updatedBy: 2,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 3,
        hour: '1',
        startTime: '07:00:00',
        finishTime: '09:00:00',
        createdBy: 3,
        updatedBy: 3,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 3,
        hour: '2',
        startTime: '10:00:00',
        finishTime: '11:00:00',
        createdBy: 3,
        updatedBy: 3,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 3,
        hour: '3',
        startTime: '12:00:00',
        finishTime: '13:00:00',
        createdBy: 3,
        updatedBy: 3,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 3,
        hour: '4',
        startTime: '14:00:00',
        finishTime: '15:00:00',
        createdBy: 3,
        updatedBy: 3,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      }
    ])
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('schedule_study_hours')
  }
}
