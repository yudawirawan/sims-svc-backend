'use strict'

const fs = require('fs')
const path = require('path')

const csvFolder = path.join(__dirname, 'csv')

const data = fs.readFileSync(csvFolder + '/districts.csv').toString().split('\n')
  .map(e => e.trim())
  .map(e => e.split(',').map(e => e.trim()))

const objectData = []
const length = data.length
const headers = ['code', 'city_code', 'name', 'latitude', 'longitude']

module.exports = {
  up: async(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    for (let i = 0; i < length - 1; i++) {
      const tmp = {}
      for (const [x, header] of headers.entries()) {
        tmp[header] = data[i][x]
      }
      objectData.push(tmp)
    }

    for (const tempData of objectData) {
      tempData.createdAt = Sequelize.literal('CURRENT_TIMESTAMP')
      tempData.updatedAt = Sequelize.literal('CURRENT_TIMESTAMP')
    }
    await queryInterface.bulkInsert('location_districts', objectData)
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('location_districts')
  }
}
