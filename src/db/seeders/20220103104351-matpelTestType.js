'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('matpel_test_types', [
      {
        department_id: 1,
        subject_id: 1,
        testType: 'Ulangan Akhir Semester',
        abbreviation: 'UAS',
        status: 1,
        description: 'Ulangan yang dilaksanakan di akhir semester',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        subject_id: 1,
        testType: 'Ulangan Tengah Semester',
        abbreviation: 'UTS',
        status: 1,
        description: 'Ulangan yang dilaksanakan di pertengahan semester',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        subject_id: 1,
        testType: 'Kuis',
        abbreviation: 'KS',
        status: 1,
        description: 'Ulangan yang dilaksanakan untuk mengetahui batas kemampuan siswa',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        subject_id: 1,
        testType: 'Ulangan Harian',
        abbreviation: 'UH',
        status: 1,
        description: 'Ulangan yang dilaksanakan untuk mengetahui batas kemampuan siswa',
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('matpel_test_types')
  }
}
