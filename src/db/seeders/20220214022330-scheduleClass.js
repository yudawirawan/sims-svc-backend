'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('schedule_classes', [
      {
        department_id: 1,
        school_year_id: 1,
        level_id: 1,
        subject_id: 1,
        teacher_id: 2,
        day: '1',
        start_study_hour_id: 1,
        finish_study_hour_id: 2,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        school_year_id: 1,
        level_id: 2,
        subject_id: 1,
        teacher_id: 2,
        day: '2',
        start_study_hour_id: 1,
        finish_study_hour_id: 2,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        school_year_id: 1,
        level_id: 1,
        subject_id: 1,
        teacher_id: 1,
        day: '3',
        start_study_hour_id: 1,
        finish_study_hour_id: 2,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.dropTable('schedule_classes')
  }
};
