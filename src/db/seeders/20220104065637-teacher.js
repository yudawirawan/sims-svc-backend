'use strict'

module.exports = {
  up: async(queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('teachers', [
      {
        department_id: 1,
        subject_id: 1,
        kepegawaian_id: 1,
        status_id: 1,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 2,
        subject_id: 2,
        kepegawaian_id: 2,
        status_id: 2,
        createdBy: 2,
        updatedBy: 2,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      {
        department_id: 1,
        subject_id: 1,
        kepegawaian_id: 3,
        status_id: 3,
        createdBy: 1,
        updatedBy: 1,
        createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
        updatedAt: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    ], {})
  },

  down: async(queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('teachers')
  }
}
