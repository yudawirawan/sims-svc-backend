module.exports = {
  apps: [
    {
      name: 'api.sims.alter.codes',
      exec_mode: 'fork_mode',
      instances: 1,
      script: 'npm',
      args: 'start'
    }
  ]
}
