const App = require('./app')

const startServer = async() => {
  try {
    await App.listen(process.env.PORT || 9101, '0.0.0.0')
    console.log('----------------------------------------------------------------')
    console.log(`SIM SVC API port on port ${process.env.PORT} Running`)
    console.log('----------------------------------------------------------------')
    if (process.env.NODE_ENV === 'development') {
      console.log('Swagger doc on: /docs')
    }
  } catch (err) {
    App.log.error(err)
    process.exit(1)
  }
}

startServer()
