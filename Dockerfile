FROM node:16.13.1-alpine3.14

WORKDIR /src

RUN apk update && apk add g++ make python3

COPY ./docker/bin/wait /wait
RUN chmod +x /wait

CMD /wait && npm install && npm run migrate && npm run seeder && npm run dev
